var utils;
var Templates = {};
var JsonNotif = {}

utils = {
    RenderPage: function (id, data, todiv) {
        var _data = data || {};
        this.getTemplate(id, function (template) {
            var result = template(_data);
            $(todiv).append(result);
        })
    },
    getTemplate: function (id, callback) {
        if (Templates[id]) {
            callback(Templates[id]);
        } else {
            $.get(baseurl + "/assets/mastersis3/templates/" + id + ".html", function (template) {
                Templates[id] = Handlebars.compile(template);
                callback(Templates[id]);
            })
        }
    },
    // converte data do mysql para o nacional
    MostraData: function (data) {
        d = moment(data).format('DD/MM/YYYY');
        if (d !== "Invalid date") {
            return d;
        }
    },
    // convert data e hora do mysql para nacional
    DataHora: function (data) {
        d = moment(data).format('DD/MM/YYYY HH:mm:ss');
        if (d !== "Invalid date") {
            return d;
        }
    },
    // Converte Float em Moeda real
    FloatMoeda: function (num) {

        valor = parseFloat(num).toFixed(2);
        result = valor.replace(".", ",")
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");

        return result;
    },
    // converte moeda em float
    MoedaFloat: function (num) {
        return parseFloat(num.replace(".", "").replace(",", "."));
    },
    // Renderisa o float sem o NULL
    ParseFloat: function (num) {
        if (num) {
            return parseFloat(num);
        }
    },
    // Compara o json
    CompareJson: function (json1, json2) {
        return JSON.stringify(json1) === JSON.stringify(json2);
    },
    // gerador de codigo de barras
    GeraCodbarra: function (x) {
        if (!x) {
            x = 16;
        }
        chars = "1234567890";
        no = "";
        for (var i = 0; i < x; i++) {
            var rnum = Math.floor(Math.random() * chars.length);
            no += chars.substring(rnum, rnum + 1);
        }
        return no;
    },
    // gerenciador de janelas
    Window: function (conteudo, titulo, modal) {
        $('<p>')
            .append(conteudo)
            .dialog({
                resizable: false,
                title: titulo,
                width: "600",
                maxWidth: $(window).width(),
                maxHeight: $(window).height(),
                appendTo: ".container",
                modal: modal,
                open: function () {
                    $(this).css("overflow-x", "hidden")
                },
                close: function () {
                    $(this).remove()
                },
            });
    },
    notifiction: function () {
        // atualiza as notificações
        $.get(baseurl + "/notificacao/alerta", function (data) {
            if (data !== "" & typeof data == 'object' & !utils.CompareJson(JsonNotif, data)) {
                JsonNotif = data;
                $('.dropdown-alerta').removeClass('hide');
                $('.dropdown-alerta > .dropdown-menu').empty();
                $(".badge-alerta").html(data.length);
                $.each(data, function (key, value) {
                    $('.dropdown-alerta > .dropdown-menu').append(
                        $('<li>').append(
                            $('<a>', {href: value.url}).html(value.msg).append(
                                $('<span>', {class: "badge pull-right"}).append(value.qnt)
                            )
                        ));
                });
            }
        })
    }

};