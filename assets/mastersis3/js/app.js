$(function () {

    var InEdite = false;

    // Funções que devem esperar para ser disparadas
    $(document).ready(function () {
        utils.notifiction();

    });

    $(document).on("ajaxStart", function () {
        $("body").append(
            $('<div>', {class: 'spinner'}).html(
                ' <div class="rect1"></div>' +
                ' <div class="rect2"></div>' +
                ' <div class="rect3"></div>' +
                ' <div class="rect4"></div>' +
                ' <div class="rect5"></div>'
            )
        );
    });

    $(document).on("ajaxStop", function () {
        $('.spinner').remove();
    });

    $(document).on("focus", ".moeda", function () {
        $(this).mask('00.000.000,00', {reverse: true});
    });

    $(document).on("focus", ".data", function () {
        $(this).mask("00/00/0000");
        $(this).datepicker({
            changeMonth: true,
            changeYear: true,
            showOn: "focus",
            dateFormat: "dd/mm/yy",
            dayNames: ["Domingo", "Segunda", "Terça", "Quarte", "Quinta", "Sexta", "Sábado"],
            dayNamesMin: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
            monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"]
        });
    });

    $(document).on("click", ".InWindow", function (e) {
        e.preventDefault();

        var href = $(this).attr('href');
        var origem = window.location.origin + "/";
        var titulo = href.replace(origem, '').replace(/[/]/g, " » ");

        var modal = $(this).data("modal");

        $.ajax(href).done(function (data) {
            if (data.msg) {
                utils.Window(data.msg, titulo, true);
            } else {
                utils.Window(data, titulo, modal);
            }
        });
    });

    $(document).on("click", ".ConfirmDialog", function (e) {
        e.preventDefault();

        var nome = $(this).data('nome');
        var href = $(this).attr('href');
        utils.getTemplate("DialogConfirm", function (template) {
            retorno = template({"nome": nome});
            $('<div>')
                .append(retorno)
                .dialog({
                    resizable: false,
                    height: "auto",
                    width: 450,
                    modal: true,
                    appendTo: ".container",
                    title: "Atenção!",
                    close: function () {
                        $(this).remove()
                    },
                    buttons: {
                        "Continuar": function () {
                            thet = $(this);
                            $.post(href, function (data) {
                                if (data.ok) {
                                    thet.remove();
                                    $('#exibindo').val(0);
                                    $('#corpotabela').empty();
                                    LoadTabela();
                                } else if (data.msg) {
                                    utils.Window(data.msg);
                                }
                            }).fail(function (response) {
                                utils.Window(response.responseText, response.statusText);
                            });
                        },
                        "Cancel": function () {
                            $(this).remove();
                        }
                    }

                });
        })

    });

    $(document).on("click", "nav ul li ul li a", function (e) {
        if (InEdite) {
            e.preventDefault();

            var href = $(this).attr('href');

            $('<div>')
                .append("<div>", "Deseja sair da pagina sem salvar?")
                .dialog({
                    resizable: false,
                    height: "auto",
                    width: 450,
                    modal: true,
                    title: "Aviso",
                    appendTo: ".container",
                    close: function () {
                        $(this).remove()
                    },
                    buttons: {
                        "Confirma": function () {
                            window.location = href;
                        },
                        "Cancel": function () {
                            $(this).dialog("close");
                        }
                    }

                });
        }

    });

    // comportamento do menu
    $('ul.nav li').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(300);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(300);
    });
    var url = location.href;
    $('ul.nav a').filter(function () {
        return url === this.href;
    }).parents('li').addClass('active');
    //

    setInterval(function(){
        utils.notifiction();
    },5000)


});