Handlebars.registerHelper({
    Estatus: function (valor) {
        return Estatus[valor];
    },
    Natureza: function (valor) {
        return new Handlebars.SafeString(Natureza[valor]);
    },
    ProdutoTipo: function (valor) {
        return ProdutoTipo[valor];
    },
    PessoaTipo: function (valor) {
        return PessoaTipo[valor];
    },
    OrdemEstatus: function (valor) {
        return new Handlebars.SafeString(OrdemEstatus[valor]);
    },
    OsEstatus: function (valor) {
        return new Handlebars.SafeString(OsEstatus[valor]);
    },
    FinancEstatus: function (valor) {
        return new Handlebars.SafeString(FinancEstatus[valor]);
    },
    Data: function (data) {
        d = moment(data).format('DD/MM/YYYY');
        if (d !== "Invalid date") {
            return d;
        }
    },
    DataHora: function (data) {
        d = moment(data).format('DD/MM/YYYY HH:mm:ss');
        if (d !== "Invalid date") {
            return d;
        }
    },
    FloatMoeda: function (num) {
        valor = parseFloat(num).toFixed(2);
        result = valor.replace(".", ",")
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
        return result;
    },
    parseFloat: function (valor) {
        if (valor) {
            return parseFloat(valor);
        }
    },
    MoedaFloat: function (num) {
        return parseFloat(num.replace(".", "").replace(",", "."));
    }
});
