const Estatus = {
    a: "Ativo",
    d: "Desativo"
};

const Natureza = {
    R: '<span class="alerta verde">Receita',
    D: '<span class="alerta vermelho">Despesa'
};

const PessoaTipo = {
    f: "Fisica",
    j: "Juridica"
};

const ProdutoTipo = {
    k: "kit",
    s: "Serviço",
    p: "Produto"
};

const OrdemEstatus = {
    EA: '<span class="alerta vermelho">ABERTA',
    PA: '<span class="alerta laranja">PAGAMENTO EM ANALISE',
    AP: '<span class="alerta laranja">AGUARDANDO PAGAMENTO',
    EE: '<span class="alerta laranja">ENVIADO/ESPERANDO',
    RP: '<span class="alerta azul">REC/ENT PARCIAL',
    RE: '<span class="alerta verde">RECEBIDO/ENTREGUE',
    CC: '<span class="alerta">CONCLUIDO',
    CA: '<span class="alerta vermelho">CANCELADA'
};

const FinancEstatus = {
    "ab": '<span class="alerta vermelho">ABERTO',
    "pg": '<span class="alerta verde">PAGO',
    "pp": '<span class="alerta laranja">PARCIAL',
    "cn": '<span class="alerta">CANSELADO',
    "dv": '<span class="alerta azul">DEVOLVIDO'
};