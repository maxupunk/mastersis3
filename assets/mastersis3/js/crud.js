// Carregada as tabelas
LoadTabela();

function LoadTabela() {
    var dados = {
        buscar: $('#buscar').val(),
        mostrar: $('#exibindo').val(),
        estatus: $('#estatus').val()
    };

    $.get(urlbusca, dados, function (data) {
        if (typeof EscreveTabela === 'function') {
            EscreveTabela(data);
        } else {
            utils.RenderPage(current_tmpl, data, '#corpotabela');
            $('#exibindo').val(parseInt($('#exibindo').val()) + data.length);
        }
    });
}

$(document).tooltip();

$(function () {

    $(document).on('change', '#buscar, #estatus', function () {
        $('#exibindo').val(0);
        $('#corpotabela').empty();
        LoadTabela();
    });

// comportamento dos formularios
    $(document).on("submit", '#FormCRUD', function () {
        var window_current = $(this).parents(".ui-dialog-content");
        $.post($(this).attr('action'), $(this).serialize(), function (data) {
            if (data.ok) {
                window_current.dialog("close");
                $('#exibindo').val(0);
                $('#corpotabela').empty();
                LoadTabela();
            } else if (data.msg) {
                utils.Window(data.msg);
            } else {
                $("#FormCRUD :input").removeAttr("title").removeClass("alert-danger");
                $.each(data, function (index, value) {
                    input = $('[name=' + index + ']');
                    input.attr("title", value);
                    input.addClass("alert-danger");
                    input.focus();
                });
            }
        });
        return false;
    });

    $(document).on("keydown", "#FormCRUD input:text, select", function (event) {
        if (event.keyCode == 13) {
            var inputs = $(this).parents("form").eq(0).find(":input");
            var idx = inputs.index(this);

            if (idx == inputs.length - 1) {
                inputs[0].select();
            } else {
                inputs[idx + 1].focus();
            }
            return false;
        }
    });

    $(document).on("change", "#ESTOQ_MIN", function (e) {
        e.preventDefault();
        var id = $(this).parents('tr').attr('id');
        var dados = {PRO_ID: id, ESTOQ_MIN: $(this).val()};
        $.post("estoque/set", dados, function (data) {
            if (data.ok) {
                LoadTabela();
            } else if (data.msg) {
                utils.Window(data.msg);
            }
        });
    });

    $(document).on("change", "#ESTOQ_CUSTO", function (e) {
        e.preventDefault();
        var id = $(this).parents('tr').attr('id');
        var dados = {PRO_ID: id, ESTOQ_CUSTO: $(this).val()};
        $.post("financeiro/setpreco", dados, function (data) {
            if (data.ok) {
                LoadTabela();
            } else if (data.msg) {
                utils.Window(data.msg);
            }
        });
    });

    $(document).on("change", "#ESTOQ_VENDA", function (e) {
        e.preventDefault();
        var id = $(this).parents('tr').attr('id');
        var dados = {PRO_ID: id, ESTOQ_VENDA: $(this).val()};
        $.post("financeiro/setpreco", dados, function (data) {
            if (data.ok) {
                LoadTabela();
            } else if (data.msg) {
                utils.Window(data.msg);
            }
        });
    });

    // Botão editar em compra e venda
    $(document).on("click", ".BtnEditar", function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        $.post(href, function (data) {
            if (data.msg) {
                utils.Window(data.msg, "Aviso");
            }
            if (data.url) {
                window.location = data.url;
            }
        });
    });
    // auto complete de pessoas
    $(document).on("focus", "#PESSOA", function () {
        $(this).autocomplete({
            source: function (request, response) {
                $.get(baseurl + "pessoa/busca", request, response)
            },
            focus: function (event, ui) {
                event.preventDefault();
                $('#PES_ID').val(ui.item.PES_ID);
                $(this).val(ui.item.PES_NOME);
            },
            select: function (event, ui) {
                $('#PES_ID').val(ui.item.PES_ID);
                $(this).prop('readonly', true);
                return false;
            },
            minLength: 3
        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $('<li>').append(
                $('<div>').html(item.PES_NOME)
            ).appendTo(ul);
        };

    });

    $(document).on("click", ".editar-pessoa", function () {
        $('#PESSOA').prop('readonly', false);
        $('#PESSOA').val('');
        $('#PES_ID').val('');
        $('#PESSOA').select();
    });

});

$(window).scroll(function () {
    scrolltocurrent = Math.ceil($(window).scrollTop());
    heightwin = $(document).height() - $(window).height();
    if (scrolltocurrent === heightwin) {
        LoadTabela();
    }
});