<?php echo form_open(uri_string(), array("class" => "form-horizontal", "id" => "FormCRUD")); ?>

    <div class="form-group">
        <div class="col-md-8">
            <label for="ESTA_NOME" class="control-label">Estado</label>
            <input type="text" name="ESTA_NOME" value="<?= isset($end) ? $end->ESTA_NOME : null ?>" class="form-control"
                   id="ESTA_NOME" autocomplete="off"/>
            <input type="hidden" name="ESTA_ID" value="<?= isset($end) ? $end->ESTA_ID : null ?>" id="ESTA_ID"/>
        </div>
        <div class="col-md-4">
            <label for="ESTA_UF" class="control-label">UF</label>
            <input type="text" name="ESTA_UF" value="<?= isset($end) ? $end->ESTA_UF : null ?>" class="form-control"
                   id="ESTA_UF" autocomplete="off"/>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-6">
            <label for="CIDA_NOME" class="control-label">Cidade</label>
            <input type="text" name="CIDA_NOME" value="<?= isset($end) ? $end->CIDA_NOME : null ?>" class="form-control"
                   id="CIDA_NOME" autocomplete="off"/>
            <input type="hidden" name="CIDA_ID" value="<?= isset($end) ? $end->CIDA_ID : null ?>" id="CIDA_ID"/>
        </div>
        <div class="col-md-6">
            <label for="BAIRRO_NOME" class="control-label">Bairro</label>
            <input type="text" name="BAIRRO_NOME" value="<?= isset($end) ? $end->BAIRRO_NOME : null ?>"
                   class="form-control" id="BAIRRO_NOME" autocomplete="off"/>
            <input type="hidden" name="BAIRRO_ID" value="<?= isset($end) ? $end->BAIRRO_ID : null ?>" id="BAIRRO_ID"/>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12">
            <label for="RUA_NOME" class="control-label">Rua</label>
            <input type="text" name="RUA_NOME" value="<?= isset($end) ? $end->RUA_NOME : null ?>" class="form-control"
                   id="RUA_NOME" autocomplete="off"/>
            <input type="hidden" name="RUA_ID" value="<?= isset($end) ? $end->RUA_ID : null ?>" id="RUA_ID"/>
        </div>
    </div>

    <button type="submit" class="btn btn-success">Grava</button>

    <script>


        // Autocomplete do estado
        $("#ESTA_NOME").autocomplete({
            source: function (request, response) {
                $.get(baseurl + "/endereco/buscaestado", request, response)
            },
            focus: function (event, ui) {
                $(this).val(ui.item.ESTA_NOME);
                return false;
            },
            select: function (event, ui) {
                $('#ESTA_ID').val(ui.item.ESTA_ID);
                $('#ESTA_UF').val(ui.item.ESTA_UF).addClass("btn-success");
                $(this).val(ui.item.ESTA_NOME).addClass("btn-success");
                return false;
            },
            minLength: 3
        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $('<li>').append(
                $('<div>').html(item.ESTA_NOME)
            ).appendTo(ul);
        };

        $("#BAIRRO_NOME").autocomplete({
            source: function (request, response) {
                $.get(baseurl + "endereco/buscabairro/" + $('#CIDA_ID').val(), request, response)
            },
            focus: function (event, ui) {
                $(this).val(ui.item.BAIRRO_NOME);
                return false;
            },
            select: function (event, ui) {
                $('#BAIRRO_ID').val(ui.item.CIDA_ID);
                $(this).addClass("btn-success");
                return false;
            },
            minLength: 3
        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $('<li>').append(
                $('<div>').html(item.BAIRRO_NOME)
            ).appendTo(ul);
        };

        $("#CIDA_NOME").autocomplete({
            source: function (request, response) {
                $.get(baseurl + "endereco/buscacidade/" + $('#ESTA_ID').val(), request, response)
            },
            focus: function (event, ui) {
                $(this).val(ui.item.CIDA_NOME);
                return false;
            },
            select: function (event, ui) {
                $('#CIDA_ID').val(ui.item.CIDA_ID);
                $(this).addClass("btn-success");
                return false;
            },
            minLength: 3
        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $('<li>').append(
                $('<div>').html(item.CIDA_NOME)
            ).appendTo(ul);
        };

        $("#CIDA_NOME").autocomplete({
            source: function (request, response) {
                $.get(baseurl + "endereco/buscacidade/" + $('#ESTA_ID').val(), request, response)
            },
            focus: function (event, ui) {
                $(this).val(ui.item.CIDA_NOME);
                return false;
            },
            select: function (event, ui) {
                $('#CIDA_ID').val(ui.item.CIDA_ID);
                $(this).addClass("btn-success");
                return false;
            },
            minLength: 3
        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $('<li>').append(
                $('<div>').html(item.CIDA_NOME)
            ).appendTo(ul);
        };

    </script>

<?php
echo form_close();
