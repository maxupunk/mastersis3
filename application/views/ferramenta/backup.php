<div class="panel panel-default">
    <div class="panel-heading">
        Backups
        <div class="pull-right">
            <a href="" id="fazerbkp">Fazer backup</a>
        </div>
    </div>

    <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-hover table-condensed table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="60%">NOME</th>
                        <th>Data de criação</th>
                        <th>AÇÃO</th>
                    </tr>
                </thead>

                <tbody id="lista-backup">
                </tbody>

            </table>
        </div>
    </div>
</div>

<div class="modal fade modal_confirmation" id="modal_restourar">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmação restouração</h4>
            </div>
            <div class="modal-body">
                <p>Deseja realmente restaurar a base de dados: <strong id="restore_arquivo"></strong>?</p>
                <p class="alert alert-danger">
                    Todos o conteudo feito apos o backup será excluido,
                    por questão de seguraça será feito um backup  da base de dados
                    antes de restaurar!
                </p>

                <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemax="100">
                        <span class="sr-only"></span>
                    </div>
                </div>
                <strong id="mensagem"></strong>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
                <button type="button" class="btn btn-danger" id="btn_restourar">Sim, agora mesmo</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    var urlbusca = '<?= base_url('ferramenta/backupjson') ?>';

    LoadTabela();

    $(document).on("click", "#fazerbkp", function (e) {
        e.preventDefault();
        $.ajax({
            url: '<?= site_url('ferramenta/backupcriar') ?>',
            type: 'GET',
            dataType: 'json',
            beforeSend: function () {
                $('body').append(
                        $('<div>', {class: 'overlay'}).html(
                        $('<div>', {class: 'carregando'})
                        )
                        );
            },
            success: function (data) {
                $('.overlay').remove();
                $('#lista-backup').empty();
                EscreveTabela(data);
            }
        });
    });

    $(document).on("click", "#excluir", function (e) {
        e.preventDefault();
        data = {arquivo: $(this).data('nome')}
        $.post("<?= site_url('ferramenta/backupdel') ?>", data, function (data) {
            $('#lista-backup').empty();
            EscreveTabela(data);
        });
    });

    $(document).on("click", "#restore", function (e) {
        e.preventDefault();

        var nome = $(this).data('nome');

        $('#restore_arquivo').text(nome);
        $('#modal_restourar').data('arquivo', nome);
        $('#modal_restourar').modal('show');
    });

    $(document).on("click", "#btn_restourar", function (e) {
        e.preventDefault();
        
        var arquivo = $('#modal_restourar').data('arquivo');
        $('.progress-bar').css('width', 0 + '%');
        $('#mensagem').html("Fazendo um backup antes da restauração da base de dados!")
        
        $.ajax({
            type: 'GET',
            url: "<?= site_url('ferramenta/backuprestore') ?>",
            data: {arquivo: arquivo},
            xhrFields: {
                onprogress: function (e) {
                    $('#mensagem').html("Aguade, fazendo a restauração da base de dados!")
                    if (e.lengthComputable) {
                        var porcento = Math.ceil((e.loaded * 100) / e.total);
                        $('.progress-bar').width(porcento + '%').text(porcento + '%');
                    }
                }
            },
            success: function () {
                LoadTabela();
                $('#mensagem').html("Restouração feita com sucesso!");
                setTimeout(function () {
                    window.location = '<?= site_url() ?>'
                }, 3000);
            },
            error: function (data) {
                utils.Window("Erro:",data.responseText);
            }
        });
    });

    function EscreveTabela(data) {
        if (data !== "") {
            $.each(data, function (key, value) {
                $('#lista-backup').append(
                        $('<tr>').append(
                        $('<td>').html(
                        $('<a>', {href: value.URL}).html(value.ARQUIVO)
                        ),
                        $('<td>').html(value.DATA),
                        $('<td>').append(
                        // links
                        $('<button>',
                                {id: 'restore', class: 'btn btn-warning'}).html('Restaura').data("nome", value.ARQUIVO),
                        $('<button>',
                                {id: 'excluir', class: 'btn btn-danger'}).html('Excluir').data("nome", value.ARQUIVO)
                        // link fim
                        )
                        ));
            });
            $('#exibindo').val(parseInt($('#exibindo').val()) + data.length)
        }
    }

    function LoadTabela() {
        $.get(urlbusca, function (data) {
            $('#lista-backup').empty();
            EscreveTabela(data);
        });
    }

</script>