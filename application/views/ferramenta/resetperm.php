<?php echo form_open(uri_string(), array("class" => "form-horizontal", "id" => "FormReset")); ?>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="alert alert-danger" role="alert">
            Atenção todos os usuario perderão o acesso aos menus e etc, somente você
            terá permisão total e poderá dá permisão novamente a todos eles.<br>
            obs.: nem uma informação da base de dados será apagado.
        </div>

        <input type="checkbox" name="aceitatermo" value="1"> Tudo bem, resete as permissões<br><br>
        
        <div class="console"></div>
        
        <hr>

        <button type="submit" class="btn btn-success">Reseta</button>

    </div>
</div>

<?php echo form_close(); ?>
<script>
    // comportamento dos formularios
    $(document).on("submit", '#FormReset', function () {
        //$.post($(this).attr('action'), $(this).serialize(), function (data) {
        //    utils.Window(data);
        //});

        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            xhrFields: {
                onprogress: function (e) {
                    $('.console').html(e.currentTarget.responseText);
                }
            },
            success: function (data) {
                $('.console').html(data)
            }
        });

        return false;
    });
</script>