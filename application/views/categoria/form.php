<?php echo form_open(uri_string(), array("class" => "form-horizontal", "id" => "FormCRUD")); ?>
    <div class="form-group">
        <div class="col-md-12">
            <label for="CATE_NOME" class="control-label">Nome</label>
            <input type="text" name="CATE_NOME" value="<?= isset($categoria) ? $categoria->CATE_NOME : null ?>"
                   class="form-control" id="CATE_NOME"/>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            <label for="CATE_DESCRIC" class="control-label">Categoria</label>
            <textarea name="CATE_DESCRIC"
                      class="form-control"><?= isset($categoria) ? $categoria->CATE_DESCRIC : null ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            <label for="CATE_IMG" class="control-label">Imagem</label>
            <input type="text" name="CATE_IMG" value="<?= isset($categoria) ? $categoria->CATE_IMG : null ?>"
                   class="form-control" id="CATE_IMG"/>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            <label for="CATE_ESTATUS" class="control-label">Estatus</label>
            <?php
            $opt_estatus['a'] = "Ativo";
            $opt_estatus['d'] = "Desativo";

            echo form_dropdown('CATE_ESTATUS', $opt_estatus, isset($categoria) ? $categoria->CATE_ESTATUS : null, 'class="form-control"');
            ?>
        </div>
    </div>

    <button type="submit" class="btn btn-success">Grava</button>

<?php
echo form_close();
