<?php echo form_open(uri_string(), array("class" => "form-horizontal", "id" => "FormCRUD")); ?>
<div class="form-group">
    <div class="col-md-8">
        <div class="input-group">
            <label for="PES_ID" class="control-label">Pessoa</label>
            <input type="text" id="PESSOA" class="form-control">
            <input type="hidden" name="PES_ID" value="" id="PES_ID">
            <span class="input-group-btn">
                    <a class="btn btn-default editar-pessoa" style="margin-top: 27px"><i
                                class="fa fa-pencil-square-o"></i></a>
                </span>
        </div>
    </div>
    <div class="col-md-4">
        <label for="FINANC_NATUREZA" class="control-label">Natureza</label>
        <select name="FINANC_NATUREZA" class="form-control">
            <option value="D">DESPESA</option>
            <option value="R">RECEITA</option>
        </select>
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <label for="FINANC_DESCR" class="control-label">Descrição</label>
        <input type="text" name="FINANC_DESCR" class="form-control" id="FINANC_DESCR"/>
    </div>
</div>

<div class="form-group">
    <div class="col-md-3">
        <label for="FINANC_VALOR" class="control-label">Valor</label>
        <input type="text" name="FINANC_VALOR" class="form-control moeda" id="FINANC_VALOR"/>
    </div>
    <div class="col-md-3">
        <label for="FINANC_PORCONTA" class="control-label">Por conta</label>
        <input type="text" name="FINANC_PORCONTA" class="form-control moeda" id="FINANC_PORCONTA"/>
    </div>
    <div class="col-md-3">
        <label for="FINANC_VECIMENTO" class="control-label">Vecimento</label>
        <input type="text" name="FINANC_VECIMENTO" class="form-control data" id="FINANC_VECIMENTO"/>
    </div>
</div>

<button type="submit" class="btn btn-success">Grava</button>

<?php echo form_close(); ?>
<script>
    if (typeof finan_add_load == 'undefined') {
        finan_add_load = true;

        $("#PESSOA").autocomplete({
            source: function (request, response) {
                $.get("<?= site_url('pessoa/busca') ?>", request, response)
            },
            focus: function (event, ui) {
                event.preventDefault();
                $('#PES_ID').val(ui.item.PES_ID);
                $(this).val(ui.item.PES_NOME);
            },
            select: function (event, ui) {
                $('#PES_ID').val(ui.item.PES_ID);
                $(this).prop('readonly', true);
                return false;
            },
            minLength: 3
        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $('<li>').append(
                $('<div>').html(item.PES_NOME)
            ).appendTo(ul);
        };

    }
</script>
