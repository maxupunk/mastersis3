<?php echo form_open(uri_string(), array("class" => "form-horizontal", "id" => "FormCRUD")); ?>

<div class="row">
    <div class="col-xs-6">
        <?php if (isset($pessoa)) { ?>
            <label for="PES_ID" class="control-label">Pessoa</label>
            <pre><?= $pessoa->PES_NOME ?></pre>
        <?php } ?>
    </div>
    <div class="col-xs-6">
        <?php if ($financeiro->FINANC_DESCR) { ?>
            <label for="FINANC_DESCR" class="control-label">Descrição</label>
            <pre><?= $financeiro->FINANC_DESCR ?></pre>
        <?php } ?>
    </div>
</div>


<div class="row">

    <div class="col-md-3">
        <label for="FINANC_NATUREZA" class="control-label">Natureza</label>
        <?php
        $opt_natureza['R'] = "RECEITA";
        $opt_natureza['D'] = "DESPESA";

        echo form_dropdown('FINANC_NATUREZA', $opt_natureza, isset($financeiro) ? $financeiro->FINANC_NATUREZA : null, 'class="form-control"');
        ?>
    </div>

    <div class="col-xs-3">
        <?php if ($financeiro->ORDEM_ID) { ?>
            <label for="ORDEM_ID" class="control-label">Ordem</label>
            <input type="text" name="ORDEM_ID" value="<?= $financeiro->ORDEM_ID ?>" class="form-control" readonly/>
        <?php } ?>
    </div>

    <div class="col-md-3">
        <label for="FINANC_VECIMENTO" class="control-label">Vencimento</label>
        <input type="text" name="FINANC_VECIMENTO" value="<?= $this->mastersis->Data($financeiro->FINANC_VECIMENTO) ?>"
               class="form-control data"/>
    </div>

    <div class="col-md-3">
        <label for="USUARIO_APELIDO" class="control-label">Funcionario</label>
        <input type="text" name="USUARIO_APELIDO" value="<?= $financeiro->USUARIO_APELIDO ?>" class="form-control"
               readonly/>
    </div>

</div>

<div class="row">
    <div class="col-xs-3">
        <label for="FINANC_PORCONTA" class="control-label">Parte Recebida</label>
        <input type="text" name="FINANC_PORCONTA" value="<?= $this->mastersis->real($financeiro->FINANC_PORCONTA) ?>"
               class="form-control" readonly/>
    </div>

    <div class="col-xs-3">
        <label for="RESTANTE" class="control-label">Restante</label>
        <input type="text" name="RESTANTE"
               value="<?= $this->mastersis->real($financeiro->FINANC_VALOR - $financeiro->FINANC_PORCONTA) ?>"
               class="form-control" readonly/>
    </div>

    <div class="col-xs-3">
        <label for="FINANC_VALOR" class="control-label">Valor total</label>
        <input type="text" name="FINANC_VALOR" value="<?= $this->mastersis->real($financeiro->FINANC_VALOR) ?>"
               class="form-control moeda"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <hr>
        <button type="submit" class="btn btn-success">Salva</button>
    </div>
</div>

<?php echo form_close(); ?>
