<div class="panel panel-default">
    <div class="panel-heading">
        Financeiro
        <div class="pull-right">
            <div class="btn-group">
                <label>Exibindo</label>
                <input type="number" class="input-mostra" id="exibindo" value="0" readonly>
                <input type="text" id="buscar" class="input-basico" placeholder="Buscar...">

                <a data-toggle="dropdown">
                    <i class="fa fa-bars"></i>
                </a>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                        <a href="<?php echo site_url('financeiro/add'); ?>" class="InWindow">Adiciona</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">Exporta excel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-condensed table-bordered table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Usuario</th>
                    <th>Criado</th>
                    <th>Venc.</th>
                    <th>Pago</th>
                    <th>Pessoa/Empresa</th>
                    <th>Valor</th>
                    <th>Estatus</th>
                    <th>Natureza</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody id="corpotabela"></tbody>
        </table>
    </div>
</div>
<!-- /.panel -->
<script>
    urlbusca = "<?= base_url('financeiro/lista') ?>";
    current_tmpl = "TblFinanceiroTmpl";

    $(document).on("click", "#RESTANTE", function () {
        var valor = $("#RESTANTE").val();
        $('#FINANC_PORCONTA').val(valor);
    })
</script>
<script src="<?= base_url('assets/mastersis3/js/crud.js'); ?>"></script>