<?php echo form_open(uri_string(), array("class" => "form-horizontal", "id" => "FormCRUD")); ?>
<div class="row">
    <div class="col-xs-6">
        <?php if (isset($pessoa)) { ?>
            <label for="PES_ID" class="control-label">Pessoa</label>
            <input type="text" name="PESSOA" value="<?= $pessoa->PES_NOME ?>" class="form-control" readonly/>
        <?php } ?>
    </div>
    <div class="col-xs-6">
        <?php if ($financeiro->FINANC_DESCR) { ?>
            <label for="FINANC_DESCR" class="control-label">Descrição</label>
            <input type="text" name="FINANC_DESCR" value="<?= $financeiro->FINANC_DESCR ?>" class="form-control" readonly/>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-3">
        <label for="FINANC_NATUREZA" class="control-label">Natureza</label>
        <input type="text" name="FINANC_NATUREZA"
               value="<?= $this->mastersis->Natureza[$financeiro->FINANC_NATUREZA] ?>" class="form-control" readonly/>
    </div>
    <div class="col-xs-3">
        <?php if ($financeiro->ORDEM_ID) { ?>
            <label for="ORDEM_ID" class="control-label">Ordem</label>
            <input type="text" name="ORDEM_ID" value="<?= $financeiro->ORDEM_ID ?>" class="form-control" readonly/>

        <?php } ?>
    </div>

    <div class="col-md-3">
        <label for="FINANC_VECIMENTO" class="control-label">Vencimento</label>
        <input type="text" name="FINANC_VECIMENTO" value="<?= $this->mastersis->Data($financeiro->FINANC_VECIMENTO) ?>"
               class="form-control" readonly/>
    </div>

    <div class="col-md-3">
        <label for="USUARIO_APELIDO" class="control-label">Funcionario</label>
        <input type="text" name="USUARIO_APELIDO" value="<?= $financeiro->USUARIO_APELIDO ?>" class="form-control"
               readonly/>
    </div>

</div>

<div class="row">
    <div class="col-xs-3">
        <label for="PARTE_RECEBIDA" class="control-label">Parte Recebida</label>
        <input type="text" name="PARTE_RECEBIDA" value="<?= $this->mastersis->real($financeiro->FINANC_PORCONTA) ?>"
               class="form-control" readonly/>
    </div>

    <div class="col-xs-3">
        <label for="FINANC_VALOR" class="control-label">Valor total</label>
        <input type="text" name="FINANC_VALOR" value="<?= $this->mastersis->real($financeiro->FINANC_VALOR) ?>"
               class="form-control" readonly/>
    </div>

    <div class="col-xs-3">
        <label for="RESTANTE" class="control-label">Restante</label>
        <input type="text" name="RESTANTE"
               value="<?= $this->mastersis->real($financeiro->FINANC_VALOR - $financeiro->FINANC_PORCONTA) ?>"
               class="form-control" id="RESTANTE" readonly/>
    </div>

    <div class="col-xs-3">
        <label for="FINANC_PORCONTA" class="control-label">Baixar</label>
        <input type="text" name="FINANC_PORCONTA" id="FINANC_PORCONTA" class="form-control moeda"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <hr>
        <button type="submit" class="btn btn-success">Salva</button>
    </div>
</div>

<?php echo form_close(); ?>
