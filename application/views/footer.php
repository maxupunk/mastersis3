</div> <!-- /container -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                MasterSis v3.0 ( {memory_usage} | CG: <?= CI_VERSION ?> | PHP: <?= substr(phpversion(), 0, 5) ?> )<br>
                By Maxuel Aguiar
            </div>  
        </div>
    </div>
</footer>

<script>
    baseurl = "<?= base_url()?>";
</script>

</body>

</html>