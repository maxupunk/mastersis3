<div class="panel panel-default">
    <div class="panel-heading">
        Vendas
        <div class="pull-right">
            <div class="btn-group">
                <label>Exibindo</label>
                <input type="number" class="input-mostra" id="exibindo" value="0" readonly>
                <label>Buscar</label>
                <input type="text"  id="buscar" placeholder="Buscar...">

                <a data-toggle="dropdown">
                    <i class="fa fa-bars"></i>
                </a>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                        <a href="#">Exporta excel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-condensed table-bordered table-striped">
            <thead>
                <tr>
                    <th width="5%">ID</th>
                    <th width="10%">Data</th>
                    <th width="10%">Vendedor</th>
                    <th >Cliente</th>
                    <th width="10%">Estatus</th>
                    <th width="10%">Pagamento</th>
                    <th width="10%">Valor</th>
                    <th width="10%">Ação</th>
                </tr>
            </thead>

            <tbody id="corpotabela">
            </tbody>

        </table>
    </div>
</div>
<!-- /.panel -->

<script>
    urlbusca = "<?= base_url('ordens/lista/v') ?>";
    current_tmpl = "TblVendasTmpl";
</script>
<script src="<?= base_url('assets/mastersis3/js/crud.js'); ?>"></script>