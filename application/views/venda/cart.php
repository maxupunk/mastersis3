<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-lg-11">
                <div class="input-group">
                    <input type="text" class="form-control" id="PRODUTO" style="margin-top: 0px;"
                           placeholder="Digite dados do produto!">
                    <span class="input-group-btn">
                        <a href="<?= base_url('ordens/faturar/vnd') ?>" class="btn btn-default InWindow" data-modal="true">Faturar</a>
                        <a href="cart/lmp" id="nova" class="btn btn-default">Limpa</a>
                    </span>
                </div>
            </div>
            <div class="col-lg-1">
                <input type="text" class="form-control pedido_id" style="margin-top: 0px;" readonly>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-condensed table-bordered table-striped">
            <thead>
            <tr>
                <th width="5%">ID</th>
                <th width="15%">COD</th>
                <th>DESCRICAO</th>
                <th width="5%">QNT</th>
                <th width="5%">MEDIDA</th>
                <th width="10%">VALOR UN</th>
                <th width="5%">SUBTOTAL</th>
                <th width="5%">AÇÃO</th>
            </tr>
            </thead>

            <tbody id="lista-venda">
            </tbody>

            <tfoot>
            <tr>
                <td colspan="5"></td>
                <td>TOTAL :</td>
                <td colspan="2" id="total"></td>
            </tr>
            </tfoot>

        </table>
    </div>

</div>

<script>
    $('.btn-salva, .btn-finalizar, .btn-novo').hide();

    urlbusca = "<?= base_url('venda/cart/lst') ?>";

    if (typeof venda_cart_load == 'undefined') {
        venda_cart_load = true;

        $(document).on("click", "#buscar", function () {
            $(this).val('');
        });

        $(document).on("change", "#quantidade", function () {
            id = $(this).parents().children("td:first").text();
            var qnt = $(this).val();
            $.post('cart/qnt', {id: id, qnt: qnt}, function (data) {
                if (data.msg) {
                    utils.Window(data.msg);
                }
                EscreveTabela(data);
            });
        });

        $(document).on("click", "#detalhe", function (e) {
            e.preventDefault();
            var id = $(this).parents().children("td:first").text();
            $.ajax("<?= base_url('produto/detalhe/') ?>" + id).done(function (data) {
                utils.Window(data);
            });
        });

        $(document).on("click", "#exclusao", function (e) {
            e.preventDefault();
            var id = $(this).parents().children("td:first").text();
            $.post('cart/rm', {id: id}, function (data) {
                EscreveTabela(data);
            });
        });

        $(document).on("click", "#nova", function (e) {
            e.preventDefault();
            $.post($(this).attr('href'), function (data) {
                EscreveTabela(data);
            });
        });

        $("#PRODUTO").autocomplete({
            source: function (request, response) {
                $.getJSON(baseurl + "produto/getproduto/s", request, response)
            },
            focus: function (event, ui) {
                $(this).val(ui.item.PRO_DESCRICAO);
                return false;
            },
            select: function (event, ui) {
                $.post('cart/add', {id: ui.item.PRO_ID}, function (data) {
                    if (data.msg) {
                        utils.Window(data.msg);
                    } else {
                        EscreveTabela(data);
                    }
                });
                $(this).val("");
                return false;
            },
            minLength: 3
        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $('<li>').append(
                $('<div>').html(item.PRO_DESCRICAO
                    + " <br> disponivel: " + utils.ParseFloat(item.ESTOQ_ATUAL)
                    + "      valor: " + utils.FloatMoeda(item.ESTOQ_VENDA)
                )
            ).appendTo(ul);
        };

        function EscreveTabela(data) {
            $('#lista-venda').empty();
            if (data !== "") {
                $("#total").html(utils.FloatMoeda(data.total));
                if (data.produtos === "") {
                    $('.btn-novo, .pedido_id').hide();
                } else {
                    $('.btn-novo').show();
                    $('.pedido_id').hide();
                    if (data.ordem) {
                        $('.btn-salva').show();
                        $('.pedido_id').val(data.ordem);
                        $('.pedido_id').show();
                    }
                }
                utils.RenderPage("TblCartVendaTmpl", data, '#lista-venda');
            }
        }
    }

</script>
<script src="<?= base_url('assets/mastersis3/js/crud.js') ?>"></script>