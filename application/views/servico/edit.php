<?php echo form_open(uri_string(), array("class" => "form-horizontal", "id" => "FormCRUD")); ?>

<div class="row">
    <div class="col-md-12">
        <label for="PES_ID" class="control-label">Pessoa</label>
        <pre><?= $os->PES_NOME ?></pre>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label for="ORDEM_EQUIPAMENT" class="control-label">Equipamento</label>
        <pre><?= $os->ORDEM_EQUIPAMENT ?></pre>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label for="ORDEM_DSC_DEFEITO" class="control-label">Defeito relatado</label>
        <pre><?= $os->ORDEM_DSC_DEFEITO ?></pre>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <input type="text" class="form-control" id="Produto-Servico" placeholder="Digite dados do produto ou serviço!">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="1%">ID</th>
                            <th>DESCRICAO</th>
                            <th width="1%">QNT</th>
                            <th width="1%">MEDIDA</th>
                            <th width="1%"></th>
                        </tr>
                        </thead>

                        <tbody id="lista-compra">
                        </tbody>

                        <tfoot>
                        <tr>
                            <td colspan="3"></td>
                            <td>TOTAL:</td>
                            <td colspan="2" id="total"></td>
                        </tr>
                        </tfoot>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label for="ORDEM_DSC_PENDENT" class="control-label">Pendentes</label>
        <textarea name="ORDEM_DSC_PENDENT" class="form-control"
                  id="ORDEM_DSC_PENDENT"><?= $os->ORDEM_DSC_PENDENT ?></textarea>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label for="ORDEM_DSC_SOLUC" class="control-label">Solução</label>
        <textarea name="ORDEM_DSC_SOLUC" class="form-control"
                  id="ORDEM_DSC_SOLUC"><?= $os->ORDEM_DSC_SOLUC ?></textarea>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label for="ORDEM_OBS" class="control-label">Observação</label>
        <textarea name="ORDEM_OBS" class="form-control" id="ORDEM_OBS"><?= $os->ORDEM_OBS ?></textarea>
    </div>
</div>

<div class="row">
    <div class="col-xs-4">
        <label for="ORDEM_ESTATUS" class="control-label">Estatus</label>
        <?php
        $opt_status['EA'] = "ABERTO";
        $opt_status['CC'] = "CONCLUIDO";

        echo form_dropdown('ORDEM_ESTATUS', $opt_status, isset($os) ? $os->ORDEM_ESTATUS : null, 'class="form-control"');
        ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <hr>
        <button type="submit" class="btn btn-success">Grava</button>
    </div>
</div>

<?php echo form_close(); ?>
<script>

    var os = <?= $os->ORDEM_ID ?>;

    $.get("servico/lstitem/" + os, function (data) {
        EscreveProduto(data);
    });

    $("#Produto-Servico").autocomplete({
        source: function (request, response) {
            ajaxes=[
                $.getJSON("<?= site_url('produto/getservico') ?>", request, response),
                $.getJSON("<?= site_url('produto/getproduto') ?>", request, response)
            ]
            $.when.apply(0,ajaxes).then(function() {
                response(Array.prototype.map.call(arguments, function(res) {
                    return res[0]
                }).reduce(function(p, c) {
                    return p.concat(c)
                }))
            })
            $.getJSON("<?= site_url('produto/getproduto/s') ?>", request, response)
        },
        focus: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.PRO_DESCRICAO);
        },
        select: function (event, ui) {
            dados = {
                ORDEM_ID: os,
                PRO_ID: ui.item.PRO_ID
            };
            $.post("<?= site_url('servico/additem') ?>", dados, function (data) {
                if (data.msg) {
                    utils.Window(data.msg);
                } else {
                    EscreveProduto(data);
                }
            });
            $(this).val("");
            return false;
        },
        minLength: 3
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $('<li>').append(
            $('<div>').html(item.PRO_DESCRICAO
                + " ( " + utils.FloatMoeda(item.ESTOQ_VENDA) + " )"
            )
        ).appendTo(ul);
    };


    // carregamento unico
    if (typeof os_edit_load === 'undefined') {
        os_edit_load = true;

        $(document).on("click", "#Produto-Servico", function () {
            $(this).val('');
        });

        $(document).on("change", "#quantidade", function () {
            dados = {
                ORDEM_ID: os,
                PRO_ID: $(this).parents('tr').attr('id'),
                QNT: $(this).val()
            };
            $.post("<?= site_url('servico/upditem') ?>", dados, function (data) {
                EscreveProduto(data);
            });
        });

        $(document).on("click", "#exclusao", function (e) {
            e.preventDefault();
            dados = {
                ORDEM_ID: os,
                PRO_ID: $(this).parents('tr').attr('id')
            };
            $.post("<?= site_url('servico/delitem') ?>", dados, function (data) {
                EscreveProduto(data);
            });
        });

        function EscreveProduto(data) {
            if (data !== "") {
                $('#lista-compra').empty();
                $("#total").html(utils.FloatMoeda(data.total));
                $.each(data.produtos, function (key, value) {
                    $('#lista-compra').append(
                        $('<tr>', {id: value.PRO_ID}).append(
                            $('<td>').html(value.PRO_ID),
                            $('<td>').html(value.PRO_DESCRICAO),
                            $('<td>').html(
                                $('<input>', {
                                    type: 'number',
                                    id: 'quantidade',
                                    value: parseFloat(value.LIST_PED_QNT)
                                })),
                            $('<td>').html(utils.FloatMoeda(value.LIST_PED_VALOR)),
                            $('<td>').html(
                                // links
                                $('<a>', {
                                    class: 'fa fa-times opcoes',
                                    id: 'exclusao'
                                })
                                // link fim
                            )
                        ));
                });
            }
        }

    } // fim do carregamento unico
</script>
