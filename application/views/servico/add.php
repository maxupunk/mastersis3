<?php echo form_open(uri_string(), array("class" => "form-horizontal", "id" => "FormCRUD")); ?>
<div class="row">
    <div class="col-md-12">
        <div class="input-group">
            <label for="PES_ID" class="control-label">Pessoa</label>
            <input type="text" id="PESSOA" class="form-control">
            <input type="hidden" name="PES_ID" value="" id="PES_ID">
            <span class="input-group-btn">
                    <a class="btn btn-default editar-pessoa" style="margin-top: 27px;"><i
                                class="fa fa-pencil-square-o"></i></a>
                    <a href="<?= site_url('pessoa/add'); ?>" style="margin-top: 27px;" class="btn btn-default InWindow"><i
                                class="fa fa-plus"></i></a>
                </span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label for="ORDEM_EQUIPAMENT" class="control-label">Equipamento</label>
        <textarea name="ORDEM_EQUIPAMENT" class="form-control" id="ORDEM_EQUIPAMENT"/>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label for="ORDEM_DSC_DEFEITO" class="control-label">Defeito relatado</label>
        <textarea name="ORDEM_DSC_DEFEITO" class="form-control" id="ORDEM_DSC_DEFEITO"/>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label for="ORDEM_OBSERVACAO" class="control-label">Observação</label>
        <textarea name="ORDEM_OBSERVACAO" class="form-control" id="ORDEM_OBSERVACAO"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <hr>
        <button type="submit" class="btn btn-success">Grava</button>
    </div>
</div>

<?php echo form_close(); ?>
