<div class="panel panel-default">
    <div class="panel-heading">
        Ordem de Serviços
        <div class="pull-right">

            <div class="btn-group">
                <select id="estatus">
                    <option value="">Estatus</option>
                    <option value="1">Em aberto</option>
                    <option value="2">Pendente</option>
                    <option value="3">Concluido</option>
                    <option value="4">Entregue</option>
                </select>
                <label>Exibindo</label>
                <input type="number" class="input-mostra" id="exibindo" value="0" readonly>
                <label>Buscar</label>
                <input type="text" id="buscar" placeholder="Buscar...">

                <a data-toggle="dropdown">
                    <i class="fa fa-bars"></i>
                </a>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                        <a href="<?php echo site_url('servico/add'); ?>" class="InWindow">Adiciona</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('pessoa/add'); ?>" class="InWindow">Add pessoa</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">Exporta DPF</a>
                    </li>
                    <li>
                        <a href="#">Exporta excel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-condensed table-bordered table-striped">
            <thead>
            <tr>
                <th width="5%">ID</th>
                <th width="20%">Cliente</th>
                <th>Equipamento</th>
                <th width="15%">Entrada</th>
                <th width="10%">Status</th>
                <th width="10%">Acão</th>
            </tr>
            </thead>

            <tbody id="corpotabela">
            </tbody>
        </table>
    </div>
</div>

<script>
    urlbusca = "<?= base_url('ordens/lista/s') ?>";
    current_tmpl = "TblServicoTmpl";

    // comportamento dos formularios json
    $(document).on("submit", '#FormFinaliza', function () {
        $.post($(this).attr('action'), $(this).serialize(), function (dados) {
            $(".help-inline").remove();
            if (dados.ok) {
                $('#ModalUnic').modal('hide');
                var filtro = {
                    buscar: $('#buscar').val(),
                    estatus: $('#estatus').val()
                };
                $.get("servico/lista", filtro, function (data) {
                    $('#exibindo').val(0);
                    $('#corpotabela').empty();
                    EscreveTabela(data);
                });
            } else if (dados.msg) {
                utils.Window(dados.msg);
            } else {
                $.each(dados, function (index, value) {
                    input = $('[name=' + index + ']');
                    input.after($('<div>', {class: 'help-inline'}).html(value));
                    input.focus();
                });
            }
        });
        return false;
    });

    $(document).on("click", ".reabrir", function (e) {
        e.preventDefault();

        var href = $(this).attr('href');

        $.post(href, function (data) {
            if (data.msg) {
                utils.Window(data.msg);
            } else {
                var dados_post = {
                    buscar: $('#buscar').val(),
                    estatus: $('#estatus').val()
                };
                $.post(urlbusca, dados_post, function (retorno) {
                    $('#exibindo').val(0);
                    $('#corpotabela').empty();
                    EscreveTabela(retorno);
                });
            }
        });

    });

</script>
<script src="<?= base_url('assets/mastersis3/js/crud.js'); ?>"></script>