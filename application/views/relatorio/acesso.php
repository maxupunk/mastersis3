<div class="panel panel-default">
    <div class="panel-heading">
        <div class="pull-right">
            <div class="btn-group">
                <label>Exibindo</label>
                <input type="text" value="0" class="input-mostra" id="mostra" readonly>
                
                <input type="text" value="" class="input-basico" id="busca" placeholder="buscar">

                <label>de</label>
                <input type="text" class="data" id="dti">
                <label>ate</label>
                <input type="text" class="data" id="dtf">

                <button class="fa fa-filter botao-basico" id="filtra"></button>

                <a data-toggle="dropdown">
                    <i class="fa fa-bars"></i>
                </a>

                <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                        <a href="#">Exporta DPF</a>
                    </li>
                    <li>
                        <a href="#">Exporta excel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-condensed table-bordered table-striped">
            <thead>
                <tr>
                    <th width="10%">USUARIO</th>
                    <th width="10%">CLASSE</th>
                    <th width="10%">METODO</th>
                    <th width="10%">DESCR</th>
                    <th width="10%">DATA</th>
                    <th width="10%">MENSAGEM</th>
                </tr>
            </thead>
            <tbody id="acessos">
            </tbody>
        </table>
    </div>
</div>

<!-- /.panel -->
<script>
    $(document).on('click', '#filtra', function () {
        $('#acessos').empty();
        $('#mostra').val(0);
        CarregaRelatorios();
    });

    $(document).on('change', '#natu, #estatus, #USER', function () {
        $('#acessos').empty();
        $('#mostra').val(0);
        CarregaRelatorios();
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() == ($(document).height() - $(window).height())) {
            CarregaRelatorios();
        }
    });

    function CarregaRelatorios() {
        var dados = {
            busca: $('#busca').val(),
            dti: $('#dti').val(),
            dtf: $('#dtf').val(),
            ini: $('#mostra').val()
        };
        $.post("acessojson", dados, function (data) {
            utils.RenderPage("RelatTblAcessoTmpl", data, '#acessos');
        });
    }
</script>