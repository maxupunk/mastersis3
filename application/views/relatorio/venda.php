<div class="panel panel-default">
    <div class="panel-heading">
        <div class="pull-right">
            <div class="btn-group">
                <label>Exibindo</label>
                <input type="text" value="0" class="input-mostra" id="mostra" readonly>
                <label>Usuario</label>
                <?php
                $opt_usuario = [null => "Usuarios"]; // array iniciado para evitar erros caso não aja cargo cadastrado
                foreach ($all_usuario as $usuario) {
                    $opt_usuario[$usuario->USUARIO_ID] = $usuario->USUARIO_APELIDO;
                }
                echo form_dropdown('USUARIO_ID', $opt_usuario, "", 'id="USER"');
                ?>
                <label>de</label>
                <input type="text" class="data" id="dti">
                <label>ate</label>
                <input type="text" class="data" id="dtf">

                <button class="fa fa-filter botao-basico" id="filtra"></button>

                <a data-toggle="dropdown">
                    <i class="fa fa-bars"></i>
                </a>

                <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                        <a href="#">Exporta DPF</a>
                    </li>
                    <li>
                        <a href="#">Exporta excel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-condensed table-bordered table-striped">
            <thead>
                <tr>
                    <th width="5%">ID</th>
                    <th width="10%">Data</th>
                    <th width="10%">Usuario</th>
                    <th >Cliente</th>
                    <th width="10%">Estatus</th>
                    <th width="10%">Pagamento</th>
                    <th width="10%">Valor</th>
                </tr>
            </thead>
            <tbody id="vendas">
            </tbody>
            <tr>
                <td colspan="6"></td>
                <td><span id="total">0,00</span></td>
            </tr>
        </table>
    </div>
</div>
<!-- /.panel -->
<script>
    //var exibindo = 0;
    var total = 0;

    $(document).on('click', '#filtra', function () {
        $('#vendas').empty();
        $('#mostra').val(0);
        total = 0;
        CarregaRelatorios();
    });

    $(document).on('change', '#USER', function () {
        $('#financeiro').empty();
        $('#mostra').val(0);
        total = 0;
        CarregaRelatorios();
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() == ($(document).height() - $(window).height())) {
            CarregaRelatorios();
        }
    });

    function CarregaRelatorios() {
        var dados = {
            user: $('#USER').val(),
            dti: $('#dti').val(),
            dtf: $('#dtf').val(),
            ini: $('#mostra').val()
        };
        $.post("vndjson", dados, function (data) {
            EscreveTabela(data);
            $("#total").html(utils.FloatMoeda(total));
        });
    }

    function EscreveTabela(data) {
        if (data !== "") {
            $.each(data.conteudo, function (key, value) {
                $('#vendas').append(
                        $('<tr>').append(
                        $('<td>').html(value.ORDEM_ID),
                        $('<td>').html(utils.MostraData(value.ORDEM_DATA)),
                        $('<td>').html(value.USUARIO_APELIDO),
                        $('<td>').html(value.PES_NOME),
                        $('<td>').html(OrdemEstatus[value.ORDEM_ESTATUS]),
                        $('<td>').html(FinancEstatus[value.FINANC_ESTATUS]),
                        $('<td>').html(utils.FloatMoeda(value.FINANC_PORCONTA) + "/" + utils.FloatMoeda(value.TOTAL))
                        ));
            });

            $('#mostra').val(parseInt($('#mostra').val()) + data.conteudo.length)

            total += data.total;
        }
    }
</script>