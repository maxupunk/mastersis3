<div class="panel panel-default">
    <div class="panel-heading">
        Alerta de estoque
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-condensed table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Cod. barras</th>
                    <th>Descrição</th>
                    <th>Medida</th>
                    <th>Custo</th>
                    <th>Venda</th>
                    <th>Stq. at.</th>
                    <th>Stq. min.</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($produtos as $produto) { ?>
                    <tr>
                        <td><?= $produto->PRO_ID ?></td>
                        <td><?= $produto->PRO_CODBARRA ?></td>
                        <td><?= $produto->PRO_DESCRICAO ?></td>
                        <td><?= $produto->MEDI_SIGLA ?></td>
                        <td><?= $produto->ESTOQ_CUSTO ?></td>
                        <td><?= $produto->ESTOQ_VENDA ?></td>
                        <td><?= $produto->ESTOQ_ATUAL ?></td>
                        <td><?= $produto->ESTOQ_MIN ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<!-- /.panel -->