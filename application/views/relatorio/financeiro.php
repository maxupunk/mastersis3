<div class="panel panel-default">
    <div class="panel-heading">
        <div class="pull-right">
            <div class="btn-group">
                <label>Exibindo</label>
                <input type="text" value="0" class="input-mostra" id="mostra" readonly>
                <?php
                $opt_usuario = [null => "Usuarios"]; // array iniciado para evitar erros caso não aja cargo cadastrado
                foreach ($all_usuario as $usuario) {
                    $opt_usuario[$usuario->USUARIO_ID] = $usuario->USUARIO_APELIDO;
                }
                echo form_dropdown('USUARIO_ID', $opt_usuario, "", 'id="USER"');
                ?>

                <select name="natu" id="natu">
                    <option value="">Natureza</option>
                    <option value="R">Receita</option>
                    <option value="D">Despeza</option>
                </select>

                <select name="estatus" id="estatus">
                    <option value="">Estatus</option>
                    <option value="ab">Aberto</option>
                    <option value="pg">Pago</option>
                    <option value="pp">Parc. pago</option>
                    <option value="cn">Cancelado</option>
                    <option value="dv">Devolvido</option>
                </select>

                <label>de</label>
                <input type="text" class="data" id="dti">
                <label>ate</label>
                <input type="text" class="data" id="dtf">

                <button class="fa fa-filter botao-basico" id="filtra"></button>

                <a data-toggle="dropdown">
                    <i class="fa fa-bars"></i>
                </a>

                <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                        <a href="#">Exporta DPF</a>
                    </li>
                    <li>
                        <a href="#">Exporta excel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-condensed table-bordered table-striped">
            <thead>
                <tr>
                    <th width="5%">ID</th>
                    <th width="10%">Data</th>
                    <th width="10%">Data pg</th>
                    <th width="10%">Usuario</th>
                    <th width="10%">Natureza</th>
                    <th >Descrição</th>
                    <th width="10%">Valor</th>
                    <th width="10%">Estatus</th>
                </tr>
            </thead>
            <tbody id="financeiro">
            </tbody>

            <tfoot>
            <tr>
                <td colspan="6"></td>
                <td><span id="porconta"></span>/<span id="total"></span></td>
                <td></td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
<!-- /.panel -->
<script>
    //var exibindo = 0;
    var total = 0;
    var porconta = 0;

    $(document).on('click', '#filtra', function () {
        $('#financeiro').empty();
        $('#mostra').val(0);
        total = 0;
        porconta = 0;
        CarregaRelatorios();
    });

    $(document).on('change', '#natu, #estatus, #USER', function () {
        $('#financeiro').empty();
        $('#mostra').val(0);
        total = 0;
        porconta = 0;
        CarregaRelatorios();
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() == ($(document).height() - $(window).height())) {
            CarregaRelatorios();
        }
    });

    function CarregaRelatorios() {
        var dados = {
            user: $('#USER').val(),
            dti: $('#dti').val(),
            dtf: $('#dtf').val(),
            natu: $('#natu').val(),
            estatus: $('#estatus').val(),
            ini: $('#mostra').val()
        };
        $.post("financjson", dados, function (data) {
            EscreveTabela(data);
            $("#total").html(utils.FloatMoeda(total));
            $("#porconta").html(utils.FloatMoeda(porconta));
        });
    }

    function EscreveTabela(data) {
        if (data !== "") {
            console.log(data.conteudo);
            utils.RenderPage("RelatTblFinanceiro", data.conteudo, '#financeiro');
            $('#mostra').val(parseInt($('#mostra').val()) + data.conteudo.length)
            total += data.total;
            porconta += data.porconta;
        }
    }
</script>