<div class="panel panel-default">
    <div class="panel-heading">
        Unidade de Medida
        <div class="pull-right">

            <div class="btn-group">

                <label>Exibindo</label>
                <input type="number" class="input-mostra" id="exibindo" value="0" readonly>
                <label>Buscar</label>
                <input type="text"  id="buscar" placeholder="Buscar...">

                <a data-toggle="dropdown">
                    <i class="fa fa-bars"></i>
                </a>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li><a href="<?php echo site_url('medida/add'); ?>" class="InWindow">Adiciona</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">Exporta DPF</a>
                    </li>
                    <li>
                        <a href="#">Exporta excel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-condensed table-bordered table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Medida</th>
                    <th>Sigla</th>
                    <th>Ação</th>
                </tr>
            </thead>

            <tbody id="corpotabela">
            </tbody>
        </table>
    </div>
</div>

<script>
    urlbusca = "<?= base_url('medida/lista') ?>";
    current_tmpl = "TblMedidaTmpl";
</script>
<script src="<?= base_url('assets/mastersis3/js/crud.js'); ?>"></script>