<?php echo form_open(uri_string(), array("class" => "form-horizontal", "id" => "FormCRUD")); ?>

    <div class="form-group">
        <div class="col-md-7">
            <label for="MEDI_NOME" class="control-label">Nome da medida</label>
            <input type="text" name="MEDI_NOME" value="<?= isset($medida) ? $medida->MEDI_NOME : null ?>"
                   class="form-control" id="MEDI_NOME"/>
        </div>
        <div class="col-md-5">
            <label for="MEDI_SIGLA" class="control-label">Sigla</label>
            <input type="text" name="MEDI_SIGLA" value="<?= isset($medida) ? $medida->MEDI_SIGLA : null ?>"
                   class="form-control" id="MEDI_SIGLA"/>
        </div>
    </div>

    <button type="submit" class="btn btn-success">Grava</button>

<?php echo form_close();