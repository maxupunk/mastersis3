<div class="panel panel-default">
    <div class="panel-heading">
        Configurações
    </div>

    <div class="panel-body">

        <div class="form-group">
            <div class="col-xs-6">
                <label for="PRO_TIPO">Quantidade de linha por pagina</label>
                <input type="number" value="20"/>
            </div>
            <div class="col-xs-6">
                <label for="PRO_CODBARRA">Cod. barras</label>
                <div class="input-group">
                    <input type="text" name="PRO_CODBARRA" class="form-control" value="" id="PRO_CODBARRA" />
                </div>
            </div>
        </div>

    </div>

</div>
<!-- /.panel -->
<script>
    urlbusca = "<?= site_url('avaria/lista'); ?>";
    function EscreveTabela(data) {
        if (data !== "") {
            $.each(data, function (key, value) {
                $('#corpotabela').append(
                        $('<tr>').append(
                        $('<td>').html(value.PRO_ID),
                        $('<td>').html(value.PRO_CODBARRA),
                        $('<td>').html(value.PRO_DESCRICAO),
                        $('<td>').html(value.PES_NOME),
                        $('<td>').html(parseFloat(value.AVA_QNT)),
                        $('<td>').html(value.MEDI_SIGLA),
                        $('<td>').html(value.AVA_MOTIVO),
                        $('<td>').append(
                        // links
                        $('<a>', {
                            href: 'avaria/remove/' + value.AVA_ID,
                            class: 'fa fa-trash opcoes ConfirmDialog'
                        }).data("nome", value.PRO_DESCRICAO)
                        // link fim
                        )
                        ));
            });
            $('#exibindo').val(parseInt($('#exibindo').val()) + data.length);
        }
    }
</script>