<?php echo form_open(uri_string(), array("class" => "form-horizontal", "id" => "FormCRUD")); ?>

    <div class="form-group">
        <div class="col-md-5">
            <label for="FPG_DESCR" class="control-label">Descrição</label>
            <input type="text" name="FPG_DESCR" value="<?= isset($forma_pg) ? $forma_pg->FPG_DESCR : null ?>"
                   class="form-control" id="FPG_DESCR"/>
        </div>
        <div class="col-md-2">
            <label for="FPG_CUSTO" class="control-label">Custo</label>
            <input type="text" name="FPG_CUSTO" value="<?= isset($forma_pg) ? $forma_pg->FPG_CUSTO : null ?>"
                   class="form-control" id="FPG_CUSTO"/>
        </div>
        <div class="col-md-2">
            <label for="FPG_PARCE" class="control-label">Parcelas</label>
            <input type="text" name="FPG_PARCE" value="<?= isset($forma_pg) ? $forma_pg->FPG_PARCE : null ?>"
                   class="form-control" id="FPG_PARCE"/>
        </div>
        <div class="col-md-3">
            <label for="FPG_AJUSTE" class="control-label">Ajuste A.M(%)</label>
            <input type="text" name="FPG_AJUSTE" value="<?= isset($forma_pg) ? $forma_pg->FPG_AJUSTE : null ?>"
                   class="form-control" id="FPG_AJUSTE"/>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-6 col-xs-offset-3">
            <label for="FPG_ESTATUS" class="control-label">Estatus</label>
            <?php
            $opt_estatus['a'] = "Ativo";
            $opt_estatus['d'] = "Desativo";

            echo form_dropdown('FPG_ESTATUS', $opt_estatus, isset($forma_pg) ? $forma_pg->FPG_ESTATUS : null, 'class="form-control"');
            ?>
        </div>
    </div>

    <button type="submit" class="btn btn-success">Grava</button>

<?php
echo form_close();
