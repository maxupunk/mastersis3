<div class="panel panel-default">
    <div class="panel-heading">
        Estoque
        <div class="pull-right">
            <div class="btn-group">
                <label>Exibindo</label>
                <input type="number" class="input-mostra" id="exibindo" value="0" readonly>
                <input type="text" id="buscar" class="input-basico" placeholder="Buscar...">

                <a data-toggle="dropdown">
                    <i class="fa fa-bars"></i>
                </a>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                        <a href="#">Exporta excel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-condensed table-bordered table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Codigo</th>
                    <th>Produto</th>
                    <th>Medida</th>
                    <th>Minimo</th>
                    <th>Atual</th>
                    <th>Custo</th>
                    <th>Venda</th>
                </tr>
            </thead>
            <tbody id="corpotabela"></tbody>
        </table>
    </div>
</div>
<!-- /.panel -->
<script>
    urlbusca = "<?= base_url('estoque/lista') ?>";
    current_tmpl = "TblEstoqueTmpl";
</script>
<script src="<?= base_url('assets/mastersis3/js/crud.js'); ?>"></script>