<div class="modal-body">
    <table class="cabecario table-striped">
        <tr>
            <th>ID da venda:</th>
            <th><?= $Ordem->ORDEM_ID ?></th>
            <th>Subtotal:</th>
            <th><?= $this->mastersis->real($Ordem->SUBTOTAL) ?></th>
        </tr>
        <tr>
            <th>Data:</th>
            <th><?= $this->mastersis->DataTempo($Ordem->ORDEM_DATA) ?></th>
            <th>Acrecimo:</th>
            <th>+ <?= $this->mastersis->real($Ordem->ORDEM_ACRECIMO) ?></th>
        </tr>
        <tr>
            <th>Estatus:</th>
            <th><?= $this->mastersis->OrdemEstatus[$Ordem->ORDEM_ESTATUS] ?></th>
            <th>Frete:</th>
            <th>+ <?= $this->mastersis->real($Ordem->ORDEM_FRETE) ?></th>
        </tr>
        <tr>
            <th>N. Documento:</th>
            <th><?= $Ordem->ORDEM_N_DOC ?></th>
            <th>Imposto:</th>
            <th>+ <?= $this->mastersis->real($Ordem->ORDEM_IMPOSTO) ?></th>
        </tr>
        <tr>
            <th>Pagamento:</th>
            <th><?= $Ordem->FPG_DESCR ?> ( <?= $Ordem->ORDEM_NPARC ?>x | <?= (float)$Ordem->FPG_AJUSTE ?>% )</th>
            <th>Desconto:</th>
            <th>- <?= $this->mastersis->real($Ordem->ORDEM_DESCO) ?></th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th>Total:</th>
            <th><?= $this->mastersis->real($Ordem->TOTAL) ?></th>
        </tr>
    </table>
    <div class="row">
        <div class="col-md-12">
            Obs.: <?= $Ordem->ORDEM_OBS ?>
        </div>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-hover table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>COD. BARRA</th>
            <th>QNT</th>
            <th>DESCRICAO</th>
            <th>VENDA</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($lst_produto as $produto) { ?>
            <tr>
                <td><?= $produto->PRO_ID ?></td>
                <td><?= $produto->PRO_CODBARRA ?></td>
                <td><?= floatval($produto->LIST_PED_QNT) ?> <?= $produto->MEDI_SIGLA ?></td>
                <td><?= $produto->PRO_DESCRICAO ?></td>
                <td>
                    <?= $this->mastersis->real($produto->LIST_PED_VALOR) ?>
                    ( <?= $this->mastersis->real($produto->LIST_PED_DESC) ?> )
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<div class="btn-group btn-group-justified" role="group">
    <div class="btn-group" role="group">
        <button type="button" class="btn btn-primary">E-mail</button>
    </div>
    <div class="btn-group" role="group">
        <button type="button" class="btn btn-primary">PDF</button>
    </div>
    <div class="btn-group" role="group">
        <a href="venda/alterar/<?= $Ordem->ORDEM_ID ?>" class="btn btn-warning">Editar</a>
    </div>
    <div class="btn-group" role="group">
        <a href="venda/excluir/<?= $Ordem->ORDEM_ID ?>" data-nome=" de venda, ID: <?= $Ordem->ORDEM_ID ?>"
           class="btn btn-danger ConfirmDialog">Excluir</a>
    </div>
</div>