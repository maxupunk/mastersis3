<?php echo form_open(uri_string(), array("class" => "form-horizontal", "id" => "FormCRUD")); ?>
    <div class="row">
        <div class="col-lg-11">
            <table class="cabecario">
                <tr>
                    <th>ID da Ordem:</th>
                    <th><?= $Ordem->ORDEM_ID ?></th>
                    <th>Total:</th>
                    <th><?= $this->mastersis->real($Ordem->TOTAL) ?></th>
                </tr>
                <tr>
                    <th>Data:</th>
                    <th><?= $Ordem->ORDEM_DATA ?></th>
                    <th>Desconto:</th>
                    <th><?= $this->mastersis->real($Ordem->ORDEM_DESCO) ?></th>
                </tr>
                <tr>
                    <th>Estatus:</th>
                    <th>
                        <?= $this->mastersis->OrdemEstatus[$Ordem->ORDEM_ESTATUS] ?>
                    </th>
                </tr>
            </table>
        </div>
    </div>
    <input type="hidden" name="ORDEM_ID" value="<?= $Ordem->ORDEM_ID ?>" id="ORDEM_ID">
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-hover table-condensed table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>QNT</th>
                        <th>DESCRICAO</th>
                        <th>CUSTO</th>
                        <th>VENDA</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($lst_produto as $produto) { ?>
                        <tr id="<?= $produto->PRO_ID ?>">
                            <th><?= $produto->PRO_ID ?></th>
                            <th><?= floatval($produto->LIST_PED_QNT) ?> <?= $produto->MEDI_SIGLA ?></th>
                            <th><?= $produto->PRO_DESCRICAO ?></th>
                            <th><?= $this->mastersis->real($produto->LIST_PED_VALOR) ?></th>
                            <th><input type="text" class="inputTab moeda" id="PrecoVenda"
                                       value="<?= $this->mastersis->real($produto->ESTOQ_VENDA) ?>"></th>
                            <th>
                                <?php if ($produto->LIST_PED_ESTATUS == 0) { ?>
                                    <input type="checkbox" id="marcar">
                                <?php } elseif ($produto->LIST_PED_ESTATUS == 1) { ?>
                                    <input type="checkbox" id="marcar" checked>
                                <?php } ?>
                            </th>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <label for="ORDEM_DESCO" class="control-label">Desconto(R$)</label>
            <input type="text" name="ORDEM_DESCO" value="<?= $this->mastersis->real($Ordem->ORDEM_DESCO) ?>"
                   class="form-control moeda"/>
        </div>
        <div class="col-md-3">
            <label for="ORDEM_ACRECIMO" class="control-label">Acrecimo(R$)</label>
            <input type="text" name="ORDEM_ACRECIMO"
                   value="<?= $this->mastersis->real($Ordem->ORDEM_ACRECIMO) ?>" class="form-control moeda"/>
        </div>
        <div class="col-md-3">
            <label for="ORDEM_IMPOSTO" class="control-label">Imposto(R$)</label>
            <input type="text" name="ORDEM_IMPOSTO" value="<?= $this->mastersis->real($Ordem->ORDEM_IMPOSTO) ?>"
                   class="form-control moeda"/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label for="FINANC_VECIMENTO" class="control-label">Obs.:</label>
            <input type="text" name="FINANC_VECIMENTO" value="<?= $Ordem->ORDEM_OBS ?>" class="form-control"/>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <hr>
            <button type="submit" class="btn btn-success">Receber</button>
        </div>
    </div>

<?php echo form_close(); ?>