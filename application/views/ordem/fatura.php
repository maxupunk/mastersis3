<?php echo form_open(uri_string(), array("class" => "form-horizontal", "id" => "FormCRUD")); ?>

<div class="row">
    <div class="col-md-12">
        <div class="input-group">
            <label for="PES_ID" class="control-label">Pessoa</label>
            <input type="text" value="<?= isset($Ordem) ? $Ordem->PES_NOME : null ?>" id="PESSOA" class="form-control"
                   autocomplete="off">
            <input type="hidden" name="PES_ID" value="<?= isset($Ordem) ? $Ordem->PES_ID : null ?>" id="PES_ID">
            <span class="input-group-btn">
                    <a class="btn btn-default editar-pessoa" style="margin-top: 27px;"><i
                                class="fa fa-pencil-square-o"></i></a>
                </span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-4">
        <label for="FPG_ID" class="control-label">Forma de Pagamento</label>
        <?php
        $opt_fpg[""] = "Selecione";
        foreach ($form_pgs as $form_pg) {
            $opt_fpg[$form_pg->FPG_ID] = $form_pg->FPG_DESCR;
        }
        echo form_dropdown('FPG_ID', $opt_fpg, isset($Ordem) ? $Ordem->FPG_ID : null, 'class="form-control" id="FPG_ID"');
        ?>
    </div>
    <div class="col-xs-2">
        <label for="FPG_ID" class="control-label">Parc</label>
        <?php
        $opt_nparcela = array();
        if (isset($form_pg_pcs)) {
            for ($nparcela = 1; $nparcela <= $form_pg_pcs->FPG_PARCE; $nparcela++) {
                $opt_nparcela[$nparcela] = $nparcela;
            }
        }
        echo form_dropdown('NPARCELA', $opt_nparcela, isset($Ordem) ? $Ordem->ORDEM_NPARC : null, 'class="form-control" id="NPARCELA"');
        ?>
    </div>

    <div class="col-xs-3">
        <label for="VPARCELA" class="control-label">De</label>
        <input type="text" name="VPARCELA" id="VPARCELA" class="form-control" readonly/>
    </div>
    <div class="col-xs-3">
        <label for="totalapagar" class="control-label">Total</label>
        <input type="text" name="totalapagar" id="totalapagar" value="<?= $this->mastersis->real($total); ?>"
               class="form-control" readonly/>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <label for="PEDIDO_DESCO" class="control-label">Desconto(R$)</label>
        <input type="text" name="ORDEM_DESCO"
               value="<?= isset($Ordem) ? $this->mastersis->real($Ordem->ORDEM_DESCO) : null ?>"
               class="form-control moeda"/>
    </div>
    <div class="col-md-3">
        <label for="ORDEM_ACRECIMO" class="control-label">Acrecimo(R$)</label>
        <input type="text" name="ORDEM_ACRECIMO"
               value="<?= isset($Ordem) ? $this->mastersis->real($Ordem->ORDEM_ACRECIMO) : null ?>"
               class="form-control moeda"/>
    </div>
    <div class="col-md-3">
        <label for="FINANC_VECIMENTO" class="control-label">Vencimento</label>
        <input type="text" name="FINANC_VECIMENTO"
               value="<?= isset($financeiro) ? $financeiro->FINANC_VECIMENTO : null ?>" class="form-control data"/>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <label for="FINANC_PORCONTA" class="control-label">Valor pago</label>
        <input type="text" name="FINANC_PORCONTA"
               value="<?= isset($financeiro) ? $this->mastersis->real($financeiro->FINANC_PORCONTA) : null ?>"
               id="FINANC_PORCONTA" class="form-control moeda"/>
    </div>
    <div class="col-md-3">
        <label for="ORDEM_IMPOSTO" class="control-label">Imposto</label>
        <input type="text" name="ORDEM_IMPOSTO"
               value="<?= isset($Ordem) ? $this->mastersis->real($Ordem->ORDEM_IMPOSTO) : null ?>"
               class="form-control moeda"/>
    </div>
    <div class="col-md-2">
        <label for="ORDEM_FRETE" class="control-label">Frete</label>
        <input type="text" name="ORDEM_FRETE"
               value="<?= isset($Ordem) ? $this->mastersis->real($Ordem->ORDEM_FRETE) : null ?>"
               class="form-control moeda"/>
    </div>
    <div class="col-md-4">
        <label for="ORDEM_N_DOC" class="control-label">N do documento</label>
        <input type="text" name="ORDEM_N_DOC" value="<?= isset($Ordem) ? $Ordem->ORDEM_N_DOC : null ?>"
               class="form-control"/>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label for="ORDEM_OBS" class="control-label">OBSERVAÇÃO</label>
        <textarea name="ORDEM_OBS" rows="3"
                  class="form-control"><?= isset($Ordem) ? $Ordem->ORDEM_OBS : null ?></textarea>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <hr>
        <button type="submit" class="btn btn-success">Finalizar</button>
    </div>
</div>

<?php echo form_close(); ?>

<script>
    var total = <?= $total ?>;
    var ajuste = <?= isset($form_pg_pcs) ? $form_pg_pcs->FPG_AJUSTE : 0 ?>;

    $("#PESSOA").autocomplete({
        source: function (request, response) {
            $.get("<?= site_url('pessoa/busca') ?>", request, response)
        },
        focus: function (event, ui) {
            event.preventDefault();
            $('#PES_ID').val(ui.item.PES_ID);
            $(this).val(ui.item.PES_NOME);
        },
        select: function (event, ui) {
            $('#PES_ID').val(ui.item.PES_ID);
            $(this).prop('readonly', true);
            return false;
        },
        minLength: 3
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $('<li>').append(
            $('<div>').html(item.PES_NOME)
        ).appendTo(ul);
    };

    $(document).on('change', '#FPG_ID', function () {
        if ($('#FPG_ID').val() !== "") {
            $.getJSON("<?= site_url('formapg/pega/') ?>" + $('#FPG_ID').val(), function (data) {
                if (data.msg) {
                    utils.Window(data.msg);
                } else {
                    ajuste = data.FPG_AJUSTE;
                    $('#NPARCELA').empty();
                    for (var x = 1; x <= data.FPG_PARCE; x++) {
                        $('#NPARCELA').append(new Option(x, x));
                    }
                }
            }).done(function () {
                $('#NPARCELA').change();
            });
        }
    });

    $(document).on('change', '#NPARCELA', function () {
        var nparcela = $(this).val();
        var juros = nparcela * ((ajuste / 100) * total);
        var totaljurus = (total + juros);
        var valorparcela = (totaljurus / nparcela);

        $('#totalapagar').val(utils.FloatMoeda(totaljurus));
        $('#VPARCELA').val(utils.FloatMoeda(valorparcela));
    });

</script>
