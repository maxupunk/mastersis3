<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>MasterSis Login</title>

        <!-- Bootstrap Core CSS -->
        <link href="<?= base_url('assets/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?= base_url('assets/mastersis3/css/login.css'); ?>" rel="stylesheet">

        <!-- jQuery -->
        <script src="<?= base_url('assets/jquery/jquery.min.js'); ?>"></script>

    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Entre com seus dados</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" id="form_login" method="post" action="dologin">
                                <fieldset>
                                    <div class="form-group">
                                        <input type="text" name="USUARIO_LOGIN" class="form-control" placeholder="Usuario" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="USUARIO_SENHA" class="form-control" placeholder="senha" value="">
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <button class="btn btn-lg btn-success btn-block">Logar</button>
                                </fieldset>
                            </form>
                        </div>
                        <div class="panel-footer resposta"></div>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>

<script>

    $(document).on('submit', '#form_login', function (e) {
        e.preventDefault();
        //var href = $(this).attr('href');
        $.ajax({
            dataType: "json",
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(),
            beforeSend: function () {
                $('.resposta').html("Deixa eu ver os dados, aguarde!");
            },
            success: function (data) {
                if (data.error) {
                    $('.resposta').html(data.error);
                }
                if (data.url) {
                    $('.resposta').html("Ok, bom trabalho!!!");
                    window.location = data.url;
                }
            },
            error: function (data) {
                alert("Ops, ouve um erro!");
                console.log(data);
            }
        });
    });
</script>