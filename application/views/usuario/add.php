<?php echo form_open(uri_string(), array("class" => "form-horizontal", "id" => "FormCRUD")); ?>
<div class="form-group">
    <div class="col-md-8">
        <div class="input-group">
            <label for="PES_ID" class="control-label">Pessoa</label>
            <input type="text" value="" id="PESSOA" class="form-control">
            <input type="hidden" name="PES_ID" value="" id="PES_ID">
            <span class="input-group-btn">
                    <a class="btn btn-default editar-pessoa" style="margin-top: 27px;"><i
                                class="fa fa-pencil-square-o"></i></a>
                </span>
        </div>
    </div>
    <div class="col-md-4">
        <label for="CARG_ID" class="control-label">Cargo</label>
        <div class="input-group">
            <?php
            $opt_carg = array(); // array iniciado para evitar erros caso não aja cargo cadastrado
            foreach ($all_cargos as $cargos) {
                $opt_carg[$cargos->CARG_ID] = $cargos->CARG_NOME;
            }
            echo form_dropdown('CARG_ID', $opt_carg, "", 'class="form-control" id="CARG_ID"');
            ?>
            <span class="input-group-btn">
                    <a href="<?= site_url('cargo/add'); ?>" class="btn btn-default InWindow"><i class="fa fa-plus"></i></a>
                </span>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-md-6">
        <label for="USUARIO_APELIDO" class="control-label">Apelido</label>
        <input type="text" name="USUARIO_APELIDO" class="form-control" id="USUARIO_APELIDO"/>
    </div>
    <div class="col-md-6">
        <label for="USUARIO_LOGIN" class="control-label">Login</label>
        <input type="text" name="USUARIO_LOGIN" class="form-control" id="USUARIO_LOGIN"/>
    </div>
</div>

<div class="form-group">
    <div class="col-md-6">
        <label for="USUARIO_SENHA" class="control-label">Senha</label>
        <input type="password" name="USUARIO_SENHA" class="form-control" id="USUARIO_SENHA"/>
    </div>
    <div class="col-md-6">
        <label for="USUARIO_SENHARE" class="control-label">Repita a senha</label>
        <input type="password" name="USUARIO_SENHARE" class="form-control" id="USUARIO_SENHARE"/>
    </div>
</div>

<div class="form-group">
    <div class="col-xs-6 col-xs-offset-3">
        <label for="USUARIO_ESTATUS">Estatus</label>
        <select name="USUARIO_ESTATUS" class="form-control">
            <option value="a">Ativo</option>
            <option value="d">Destivo</option>
        </select>
    </div>
</div>

</div>

<button type="submit" class="btn btn-success">Grava</button>

<?php echo form_close(); ?>
<script>

    if (typeof usuario_load == 'undefined') {
        usuario_load = true;

        // recarrega a lista de cargos
        $(document).on("click", "#CARG_ID", function () {
            var select = $(this);
            $.ajax(baseurl + "cargo/lista").done(function (data) {
                $.each(data, function (key, value) {
                    if (!select.find('option[value="' + value.CARG_ID + '"]').length) {
                        $('<option>').val(value.CARG_ID).text(value.CARG_NOME).appendTo(select);
                    }
                })
            });
        });

    }
</script>
