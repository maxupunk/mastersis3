<?php echo form_open(uri_string(), array("class" => "form-horizontal", "id" => "FormCRUD")); ?>
    <div class="form-group">
        <div class="col-md-4">
            <label for="USUARIO_APELIDO" class="control-label">Apelido</label>
            <input type="text" name="USUARIO_APELIDO" value="<?= $usuario->USUARIO_APELIDO ?>" class="form-control"
                   id="USUARIO_APELIDO"/>
        </div>
        <div class="col-md-4">
            <label for="USUARIO_LOGIN" class="control-label">Login</label>
            <input type="text" name="USUARIO_LOGIN" value="<?= $usuario->USUARIO_LOGIN ?>" class="form-control"
                   id="USUARIO_LOGIN"/>
        </div>
        <div class="col-md-4">
            <label for="CARG_ID" class="control-label">Cargo</label>
            <?php
            foreach ($all_cargos as $cargos) {
                $opt_medi[$cargos->CARG_ID] = $cargos->CARG_NOME;
            }
            echo form_dropdown('CARG_ID', $opt_medi, $usuario->CARG_ID, 'class="form-control"');
            ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-6 col-xs-offset-3">

            <label for="USUARIO_ESTATUS" class="control-label">Estatus</label>
            <?php
            $opt_estatus['a'] = "Ativo";
            $opt_estatus['d'] = "Desativo";

            echo form_dropdown('USUARIO_ESTATUS', $opt_estatus, $usuario->USUARIO_ESTATUS, 'class="form-control"');
            ?>
        </div>
    </div>

    <button type="submit" class="btn btn-success">Grava</button>

<?php
echo form_close();
