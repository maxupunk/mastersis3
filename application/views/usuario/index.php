<div class="panel panel-default">
    <div class="panel-heading">
        Usuarios
        <div class="pull-right">

            <div class="btn-group">

                <label>Exibindo</label>
                <input type="number" class="input-mostra" id="exibindo" value="0" readonly>
                <label>Buscar</label>
                <input type="text" id="buscar" placeholder="Buscar...">

                <a data-toggle="dropdown">
                    <i class="fa fa-bars"></i>
                </a>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                        <a href="<?= site_url('usuario/add'); ?>" class="InWindow">Adiciona</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="<?= site_url('pessoa/add'); ?>" class="InWindow">Adiciona pessoa</a>
                    </li>
                    <li>
                        <a href="<?= site_url('cargo/add'); ?>" class="InWindow">Adiciona cargo</a>
                    </li>
                    <li>
                        <a href="#">Exporta excel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-condensed table-bordered table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Cargo</th>
                <th>Apelido</th>
                <th>Login</th>
                <th>Estatus</th>
                <th>Ação</th>
            </tr>
            </thead>
            <tbody id="corpotabela">
            </tbody>
        </table>
    </div>

</div>
<!-- /.panel -->


<div id="PermissaoWindow" title="Permissões de usuario">
    <div class="row">
        <div class="col-lg-12">
            <div class="btn-group" role="group">
                <button class="btn btn-default btn-xs btn-invert">Inverter seleção</button>
                <button class="btn btn-default btn-xs AddAll">Adiciona todos</button>
                <button class="btn btn-default btn-xs RemAll">Remover todos</button>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-12">
            <?php
            $class_atual = "";
            foreach ($metodos

                     as $metodo) {

                if ($class_atual != $metodo->METOD_CLASS) {
                    $class_atual = $metodo->METOD_CLASS;
                    ?>
                    <legend><?= $class_atual; ?></legend>

                <?php } ?>

                <label>
                    <input type="checkbox" class="metodo <?= $class_atual; ?>" id="<?= $metodo->METOD_ID; ?>"
                           value="<?= $metodo->METOD_ID; ?>"/>
                    <?= $metodo->METOD_DESCR == null ? $metodo->METOD_METODO : $metodo->METOD_DESCR; ?>
                </label>

                <?php
            }
            ?>
        </div>
    </div>
</div>

<script>

    urlbusca = "<?= base_url('usuario/lista') ?>";
    current_tmpl = "TblUsuariosTmpl";
    var iduser;

    $(function () {
        $("#PermissaoWindow").dialog({
            autoOpen: false,
            resizable: false,
            width: "900",
            maxWidth: $(window).width(),
            maxHeight: $(window).height(),
            appendTo: ".container",
            modal: true,
            open: function () {
                $(this).css("overflow-x", "hidden");
            }
        });

        $(".metodo").checkboxradio({icon: false});

        /// Genrenciamento de permisões
        $(document).on('click', '.permisoes', function (e) {
            e.preventDefault();
            login = $(this).data('login');
            iduser = $(this).attr('href');

            $("#PermissaoWindow").dialog({
                autoOpen: true,
                title: `Permisões de ${login}`
            });
            $.get('usuario/permissoes/' + iduser, function (data) {
                $(".metodo").prop('checked', false);
                $.each(data, function (index, value) {
                    $("#" + value.METOD_ID).prop('checked', true);
                });
                $(".metodo").checkboxradio("refresh");
            });
        });

        $(document).on('click', '.metodo', function () {
            var dados = {
                METOD_ID: $(this).val(),
                USUARIO_ID: iduser
            };
            $.post("usuario/setpermissao", dados, function (data) {
                if (data.add) {
                    $('#' + data.add).prop('checked', true).checkboxradio('refresh');
                } else if (data.rm) {
                    $('#' + data.rm).prop('checked', false).checkboxradio('refresh');
                }
            });
        });

        $(document).on('click', '.btn-invert', function () {
            $(".metodo").prop('checked', $(this).prop("checked")).click().checkboxradio("refresh");
        });

        $(document).on("click", ".AddAll", function () {
            $(".metodo").each(function () {
                if (!$(this).prop("checked")) {
                    $(this).prop('checked', true).click().checkboxradio("refresh");
                }
            });
        });

        $(document).on("click", ".RemAll", function () {
            $(".metodo").each(function () {
                if ($(this).prop("checked")) {
                    $(this).prop('checked', true).click().checkboxradio("refresh");
                }
            });
        });


    }); // final do fuction do jquery

</script>
<script src="<?= base_url('assets/mastersis3/js/crud.js'); ?>"></script>