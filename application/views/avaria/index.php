
<div class="panel panel-default">
	<div class="panel-heading">
		Produtos
		<div class="pull-right">

			<div class="btn-group">

				<label>Exibindo</label>
				<input type="number" class="input-mostra" id="exibindo" value="0" readonly />
				<input type="text" id="buscar" class="input-basico" placeholder="Buscar..." />

				<a data-toggle="dropdown">
					<i class="fa fa-bars"></i>
				</a>
				<ul class="dropdown-menu pull-right" role="menu">
					<li>
						<a href="<?= site_url('avaria/add'); ?>" class="InWindow">Adiciona Avaria</a>
					</li>
					<li class="divider"></li>
					<li>
						<a href="#">Exporta DPF</a>
					</li>
					<li>
						<a href="#">Exporta excel</a>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="table-responsive">
		<table class="table table-hover table-condensed table-bordered table-striped">
			<thead>
				<tr>
					<th>PRO ID</th>
					<th>CODBARRA</th>
					<th>DESCRICAO</th>
					<th>ESTOQUE</th>
					<th>QUANTIDADE</th>
					<th>SIGLA</th>
					<th>MOTIVO</th>
					<th>AÇÃO</th>
				</tr>
			</thead>
			<tbody id="corpotabela"></tbody>
		</table>
	</div>

</div>
<!-- /.panel -->
<script>
    urlbusca = "<?= site_url('avaria/lista'); ?>";
    current_tmpl = "TblAvariaTmpl";
</script>
<script src="<?= base_url('assets/mastersis3/js/crud.js'); ?>"></script>