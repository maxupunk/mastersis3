<?php echo form_open(uri_string(), array("class" => "form-horizontal", "id" => "FormCRUD")); ?>
<div class="modal-body">

    <div class="form-group">
        <div class="col-md-10">
            <div class="input-group">
                <label for="PRODUTO" class="control-label">Produto</label>
                <input type="text" name="PRODUTO" class="form-control" id="PRODUTO" />
                <input type="hidden" name="PRO_ID" value="" id="PRO_ID">
                <span class="input-group-btn" >
                    <a class="btn btn-default editprod" style="margin-top: 27px"><i class="fa fa-pencil-square-o"></i></a>
                </span>
            </div>
        </div>
        <div class="col-md-2">
            <label for="AVA_QNT" class="control-label">Qnt</label>
            <input type="text" name="AVA_QNT" class="form-control" id="AVA_QNT" />
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12">
            <label for="AVA_MOTIVO" class="control-label">Motivo</label>
            <input type="text" name="AVA_MOTIVO" class="form-control" id="AVA_MOTIVO" />
        </div>
    </div>

</div>

<div class="modal-footer">
    <button type="submit" class="btn btn-success">Grava</button>
</div>
<?php
echo form_close();
?>
<script>

    if (typeof avaria_load == 'undefined') {
        avaria_load = true;

        $(document).on("click", ".editprod", function () {
            $('#PRODUTO').prop('readonly', false);
            $('#PRODUTO').val('');
            $('#PRO_ID').val('');
            $('#PRODUTO').select();
        });

        $("#PRODUTO").autocomplete({
            source: function (request, response) {
                $.getJSON(baseurl + "produto/getproduto/s", request, response)
            },
            focus: function (event, ui) {
                $(this).val(ui.item.PRO_DESCRICAO);
                return false;
            },
            select: function (event, ui) {
                $('#PRO_ID').val(ui.item.PRO_ID);
                $(this).prop('readonly', true);
                return false;
            },
            minLength: 3
        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $('<li>').append(
                $('<div>').html(item.PRO_DESCRICAO
                    + " <br> disponivel: " + utils.ParseFloat(item.ESTOQ_ATUAL)
                    + "      valor: " + utils.FloatMoeda(item.ESTOQ_VENDA)
                )
            ).appendTo(ul);
        };

    }
</script>