<?= form_open(uri_string(), array("class" => "form-horizontal", "id" => "FormCRUD")); ?>

<div class="row">
    <div class="col-md-3">
        <label for="PES_TIPO" class="control-label">Tipo</label>
        <?php
        $opt_tipo['f'] = "Fisica";
        $opt_tipo['j'] = "Juridica";

        echo form_dropdown('PES_TIPO', $opt_tipo, isset($pessoa) ? $pessoa->PES_TIPO : null, 'class="form-control" id="PES_TIPO"');
        ?>
    </div>
    <div class="col-md-4">
        <label for="PES_CPF_CNPJ" class="control-label">CPF/CNPJ</label>
        <input type="text" name="PES_CPF_CNPJ" value="<?= isset($pessoa) ? $pessoa->PES_CPF_CNPJ : null; ?>"
               class="form-control" id="PES_CPF_CNPJ"/>
    </div>
    <div class="col-md-3">
        <label for="PES_DATA_NASC" class="control-label">Nascimento</label>
        <input type="text" name="PES_DATA_NASC"
               value="<?= isset($pessoa) ? $this->mastersis->Data($pessoa->PES_DATA_NASC) : null ?>"
               class="form-control data" id="PES_DATA_NASC"/>
    </div>
    <div class="col-md-2">
        <label for="PES_ESTATUS" class="control-label">Estatus</label>
        <?php
        $opt_estatus['a'] = "Ativo";
        $opt_estatus['d'] = "Desativo";

        echo form_dropdown('PES_ESTATUS', $opt_estatus, isset($pessoa) ? $pessoa->PES_ESTATUS : null, 'class="form-control"');
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label for="PES_NOME" class="control-label">Nome</label>
        <input name="PES_NOME" type="text" value="<?= isset($pessoa) ? $pessoa->PES_NOME : null ?>" class="form-control"
               id="PES_NOME">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label for="PES_NOME_PAI" class="control-label">Nome do pai</label>
        <input type="text" name="PES_NOME_PAI" value="<?= isset($pessoa) ? $pessoa->PES_NOME_PAI : null ?>"
               class="form-control" id="PES_NOME_PAI">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label for="PES_NOME_MAE" class="control-label">Nome da mãe</label>
        <input type="text" name="PES_NOME_MAE" value="<?= isset($pessoa) ? $pessoa->PES_NOME_MAE : null ?>"
               class="form-control" id="PES_NOME_MAE">
    </div>
</div>
<div class="row">

    <div class="col-md-4">
        <label for="PES_FONE" class="control-label">Telefone</label>
        <input type="text" name="PES_FONE" value="<?= isset($pessoa) ? $pessoa->PES_FONE : null ?>" class="form-control"
               id="PES_FONE"/>
    </div>
    <div class="col-md-4">
        <label for="PES_CEL1" class="control-label">Celular 1</label>
        <input type="text" name="PES_CEL1" value="<?= isset($pessoa) ? $pessoa->PES_CEL1 : null ?>" class="form-control"
               id="PES_CEL1"/>
    </div>
    <div class="col-md-4">
        <label for="PES_CEL2" class="control-label">Celular 2</label>
        <input type="text" name="PES_CEL2" value="<?= isset($pessoa) ? $pessoa->PES_CEL2 : null ?>" class="form-control"
               id="PES_CEL2"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 rua">
        <div class="input-group">
            <label for="RUA_ID" class="control-label">Endereço</label>
            <input type="text" class="form-control" id="rua" readonly
                   value="<?=
                   isset($rua) ?
                       $rua->RUA_NOME . " - " .
                       $rua->BAIRRO_NOME . " | " .
                       $rua->CIDA_NOME . "-" .
                       $rua->ESTA_UF : null
                   ?>">

            <input type="hidden" name="RUA_ID" value="<?= isset($rua) ? $rua->RUA_ID : null ?>" id="RUA_ID">
            <span class="input-group-btn">
                    <a class="btn btn-default editar-endereco" style="margin-top: 27px;"><i
                                class="fa fa-pencil-square-o"></i></a>
                    <a href="<?= site_url('endereco/add'); ?>" class="btn btn-default InWindow"
                       style="margin-top: 27px;"><i class="fa fa-plus"></i></a>
                </span>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="PES_END_REFERENCIA" class="control-label">Referencia</label>
        <input type="text" name="PES_END_REFERENCIA" class="form-control" id="PES_END_REFERENCIA"
               value="<?= isset($pessoa) ? $pessoa->PES_END_REFERENCIA : null ?>">
    </div>
</div>

<div class="row">
    <div class="col-lg-2">
        <label for="PES_END_NUMERO" class="control-label">Numero</label>
        <input type="text" name="PES_END_NUMERO" value="<?= isset($pessoa) ? $pessoa->PES_END_NUMERO : null ?>"
               class="form-control" id="PES_END_NUMERO"/>
    </div>

    <div class="col-lg-3">
        <label for="PES_END_CEP" class="control-label">CEP</label>
        <input type="text" name="PES_END_CEP" value="<?= isset($pessoa) ? $pessoa->PES_END_CEP : null ?>"
               class="form-control" id="PES_END_CEP"/>
    </div>
    <div class="col-md-7">
        <label for="PES_EMAIL" class="control-label">Email</label>
        <input type="text" name="PES_EMAIL" value="<?= isset($pessoa) ? $pessoa->PES_EMAIL : null ?>"
               class="form-control" id="PES_EMAIL"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <hr>
        <button type="submit" class="btn btn-success">Save</button>
    </div>
</div>

<?php form_close(); ?>

<script>

    $('#rua').autocomplete({
        source: function (request, response) {
            $.getJSON("<?= site_url('endereco/busca') ?>", request, response)
        },
        focus: function (event, ui) {
            event.preventDefault();
            $('#RUA_ID').val(ui.item.id);
            $(this).val(ui.item.endereco);
        },
        select: function (event, ui) {
            $('#RUA_ID').val(ui.item.id);
            $(this).prop('readonly', true);
            return false;
        },
        minLength: 3
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $('<li>').append(
            $('<div>').html(item.endereco)
        ).appendTo(ul);
    };

    if (typeof pessoa_load == 'undefined') {
        pessoa_load = true;

        $(".data").mask("00/00/0000");

        $('#PES_FONE').mask('(00) 0000-0000');
        $('#PES_CEL1').mask('(00) 0 0000-0000');
        $('#PES_CEL2').mask('(00) 0 0000-0000');

        $('#PES_END_CEP').mask('00000-000');
        $('#PES_CPF_CNPJ').mask('000.000.000-000');
        $(document).on('change', '#PES_TIPO', function () {
            if ($(this).val() == "f") {
                $('#PES_CPF_CNPJ').mask('000.000.000-000');
            } else {
                $('#PES_CPF_CNPJ').mask('00.000.000/0000-00');
            }
        });

        $(document).on("click", ".editar-endereco", function () {
            $('#rua').prop('readonly', false);
            $('#rua').val('');
            $('#RUA_ID').val('');
            $('#rua').select();
        });

    }
</script>
