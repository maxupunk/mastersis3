<div class="panel panel-default">
    <div class="panel-heading">
        Pessoas / Empresas
        <div class="pull-right">

            <div class="btn-group">

                <label>Exibindo</label>
                <input type="number" class="input-mostra" id="exibindo" value="0" readonly>
                <label>Buscar</label>
                <input type="text"  id="buscar" placeholder="Buscar...">

                <a data-toggle="dropdown">
                    <i class="fa fa-bars"></i>
                </a>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                        <a href="<?php echo site_url('pessoa/add'); ?>" class="InWindow">Adiciona</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="<?php echo site_url('endereco/add'); ?>" class="InWindow">Adiciona Endereço</a>
                    </li>
                    <li>
                        <a href="#">Exporta excel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-condensed table-bordered table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Fone</th>
                    <th>Cel 1</th>
                    <th>Cel 2</th>
                    <th>Tipo</th>
                    <th>E-mail</th>
                    <th>Estatus</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody id="corpotabela">
            </tbody>
        </table>
    </div>

</div>
<!-- /.panel -->
<script>
    urlbusca = "<?= base_url('pessoa/lista') ?>";
    current_tmpl = "TblPessoaTmpl";
</script>
<script src="<?= base_url('assets/mastersis3/js/crud.js'); ?>"></script>