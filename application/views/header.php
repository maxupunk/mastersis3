<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="icon" href="http://getbootstrap.com/favicon.ico"> -->

    <title>MasterSis v3</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url('assets/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">
    <!-- Mastersis css -->
    <link href="<?= base_url('assets/mastersis3/css/stilo.css'); ?>" rel="stylesheet" type="text/css">
    <!-- Custom Fonts -->
    <link href="<?= base_url('assets/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
    <!-- jquery ui -->
    <link href="<?= base_url('assets/jquery-ui/jquery-ui.min.css'); ?>" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="<?= base_url('assets/jquery/jquery.min.js'); ?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?= base_url('assets/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <!-- typeahead JavaScript -->
    <script src="<?= base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <!-- Pluin jquery mask -->
    <script src="<?= base_url('assets/jquery.mask/jquery.mask.min.js'); ?>"></script>
    <!-- Pluin monetario -->
    <script src="<?= base_url('assets/moment/moment.min.js'); ?>"></script>
    <!-- Custom mastersis -->
    <script src="<?= base_url('assets/handlebars/handlebars.min.js'); ?>"></script>
    <!-- Custom mastersis -->
    <script src="<?= base_url('assets/mastersis3/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/mastersis3/js/helpers.js'); ?>"></script>
    <script src="<?= base_url('assets/mastersis3/js/utils.js'); ?>"></script>
    <script src="<?= base_url('assets/mastersis3/js/globais.js'); ?>"></script>

</head>

<body>


<noscript>
    <div class="global-site-notice noscript">
        <div class="notice-inner">
            <p><strong>JavaScript está desabilitado no seu browser.</strong><br>
                Esse site só funciona com javascript ativo.</p>
        </div>
    </div>
</noscript>

<!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= site_url("dashboard"); ?>">MasterSis</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Cadastros</a>
                        <ul class="dropdown-menu">
                            <?= $this->auth->getlink('produto', null, 'Produtos/Serviços') ?>
                            <?= $this->auth->getlink('pessoa', null, 'Pessoa/Empresa') ?>
                            <?= $this->auth->getlink('endereco', null, 'Endereços') ?>
                            <?= $this->auth->getlink('categoria', null, 'Categoria') ?>
                            <?= $this->auth->getlink('medida', null, 'Unidade de medida') ?>
                            <?= $this->auth->getlink('Usuario', null, 'Usuarios') ?>
                            <?= $this->auth->getlink('formapg', null, 'Formas de pagamento') ?>
                            <?= $this->auth->getlink('cargo', null, 'Cargo') ?>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Venda</a>
                        <ul class="dropdown-menu">
                            <?= $this->auth->getlink('venda', 'cart', 'Carrinho') ?>
                            <?= $this->auth->getlink('servico', null, 'Serviços') ?>
                            <?= $this->auth->getlink('venda', null, 'Listar') ?>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Compra</a>
                        <ul class="dropdown-menu">
                            <?= $this->auth->getlink('compra', 'cart', 'Carrinho') ?>
                            <?= $this->auth->getlink('compra', null, 'Listar') ?>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Gerenciar</a>
                        <ul class="dropdown-menu">
                            <?= $this->auth->getlink('financeiro', null, 'Financeiro') ?>
                            <?= $this->auth->getlink('estoque', null, 'Estoque') ?>
                            <?= $this->auth->getlink('avaria', null, 'Avaria') ?>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"> Relatorios </a>
                        <ul class="dropdown-menu">
                            <?= $this->auth->getlink('relatorio', 'venda', 'Vendas') ?>
                            <?= $this->auth->getlink('relatorio', 'estoque', 'Estoque') ?>
                            <?= $this->auth->getlink('relatorio', 'financeiro', 'Financeiro') ?>
                            <?= $this->auth->getlink('relatorio', 'acesso', 'Acesso') ?>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"> Ferramentas </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#"> Configurações </a>
                            </li>
                            <?= $this->auth->getlink('ferramenta', 'logsistema', 'Log de erros') ?>
                            <?= $this->auth->getlink('ferramenta', 'backup', 'Backups') ?>
                            <li>
                                <a href="#"> Atualização </a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown dropdown-alerta hide">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i><span class="badge-alerta badge"></span>
                        </a>
                        <ul class="dropdown-menu">
                        </ul><!-- /.dropdown-alerta -->
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user-o"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#"> <?= $usuario ?> </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?= site_url('usuario/logout'); ?>"> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>

<div class="container" id="conteiner">