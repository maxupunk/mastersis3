<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Database Error</title>
        <style type="text/css">

            h1 {
                color: #444;
                background-color: transparent;
                border-bottom: 1px solid #D0D0D0;
                font-size: 19px;
                font-weight: normal;
                margin: 0 0 14px 0;
                padding: 14px 15px 10px 15px;
            }

            #container {
                margin: 10px;
            }

        </style>
    </head>
    <body>
        <div id="container">
            <h1><?php echo $heading; ?></h1>
            <?php echo $message; ?>
        </div>
    </body>
</html>