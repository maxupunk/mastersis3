<div class="panel panel-default">
    <div class="panel-heading">
        Cargos
        <div class="pull-right">

            <div class="btn-group">

                <label>Exibindo</label>
                <input type="number" class="input-mostra" id="exibindo" value="0" readonly>
                <label>Buscar</label>
                <input type="text"  id="buscar" placeholder="Buscar...">

                <a data-toggle="dropdown">
                    <i class="fa fa-bars"></i>
                </a>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                        <a href="<?php echo site_url('cargo/add'); ?>" class="InWindow">Adiciona</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">Exporta excel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-condensed table-bordered table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Cargo</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody id="corpotabela">
            </tbody>
        </table>
    </div>
</div>
<!-- /.panel -->
<script>
    urlbusca = "<?= site_url('cargo/lista'); ?>";
    current_tmpl = "TblCargoTmpl";
</script>
<script src="<?= base_url('assets/mastersis3/js/crud.js'); ?>"></script>