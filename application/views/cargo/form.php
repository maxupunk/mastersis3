<?php echo form_open(uri_string(), array("class" => "form-horizontal", "id" => "FormCRUD")); ?>
    <div class="form-group">
        <label for="CARG_NOME" class="col-md-4 control-label">Cargo</label>
        <div class="col-md-8">
            <input type="text" name="CARG_NOME" value="<?= isset($cargo) ? $cargo->CARG_NOME : null ?>"
                   class="form-control" id="CARG_NOME"/>
        </div>
    </div>

    <button type="submit" class="btn btn-success">Grava</button>

<?php
echo form_close();
