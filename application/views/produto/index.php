<div class="panel panel-default">
    <div class="panel-heading">
        Produtos
        <div class="pull-right">

            <div class="btn-group">

                <label>Exibindo</label>
                <input type="number" class="input-mostra" id="exibindo" value="0" readonly>
                <label>Buscar</label>
                <input type="text" id="buscar" placeholder="Buscar...">

                <a data-toggle="dropdown">
                    <i class="fa fa-bars"></i>
                </a>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                        <a href="<?php echo site_url('produto/add'); ?>" class="InWindow">Adiciona produto</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="<?php echo site_url('categoria/add'); ?>" class="InWindow">Adiciona Categoria</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('medida/add'); ?>" class="InWindow">Add Unidade de Medida</a>
                    </li>
                    <li>
                        <a href="#">Exporta DPF</a>
                    </li>
                    <li>
                        <a href="#">Exporta excel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-condensed table-bordered table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Cod. barras</th>
                <th>Descrição</th>
                <th>Med.</th>
                <th>Custo</th>
                <th>Venda</th>
                <th>Stq. at.</th>
                <th>Stq. min.</th>
                <th>Tipo</th>
                <th>Estatus</th>
                <th>Ação</th>
            </tr>
            </thead>

            <tbody id="corpotabela">
            </tbody>
         </table>
    </div>

</div>
<!-- /.panel -->

<script>

    urlbusca = "<?= base_url('produto/lista') ?>";
    current_tmpl = "TblProdutoTmpl";

    $(document).on("click", "#detalhe", function (e) {
        e.preventDefault();
        var id = $(this).parents().children("td:first").text();
        $.ajax("<?= base_url('produto/detalhe/') ?>" + id).done(function (data) {
            utils.Window(data);
        });
    });


</script>

<script src="<?= base_url('assets/mastersis3/js/crud.js') ?>"></script>