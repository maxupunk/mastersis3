<?php echo form_open(uri_string(), array("class" => "form-horizontal", "id" => "FormCRUD")); ?>
<div class="form-group">
    <div class="col-xs-4">
        <label for="PRO_TIPO">Tipo</label>
        <?php
        $opt_tipo['p'] = "Produto";
        $opt_tipo['s'] = "Serviço";

        echo form_dropdown('PRO_TIPO', $opt_tipo, isset($produto) ? $produto->PRO_TIPO : null, 'class="form-control" id="PRO_TIPO"');
        ?>
    </div>
    <div class="col-xs-5">
        <label for="PRO_CODBARRA">Cod. barras</label>
        <div class="input-group">
            <input type="text" name="PRO_CODBARRA" class="form-control"
                   value="<?= isset($produto) ? $produto->PRO_CODBARRA : null ?>" id="PRO_CODBARRA"/>
            <span class="input-group-addon" id="random_num">
                    <i class="fa fa-random"></i>
                </span>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-xs-12">
        <label for="PRO_DESCRICAO">Descrição</label>
        <input type="text" name="PRO_DESCRICAO" class="form-control"
               value="<?= isset($produto) ? $produto->PRO_DESCRICAO : null ?>" id="PRO_DESCRICAO"/>
    </div>
</div>

<div class="form-group">
    <div class="col-xs-12">
        <label for="PRO_CARAC_TEC">Caracteristicas tecnica</label>
        <textarea name="PRO_CARAC_TEC" class="form-control"
                  id="PRO_CARAC_TEC"><?= isset($produto) ? $produto->PRO_CARAC_TEC : null ?></textarea>
    </div>
</div>

<div class="form-group">
    <hr>
    <div class="col-xs-5">
        <label for="CATE_ID">Categoria</label>
        <div class="input-group">
            <?php
            $opt_cate = array(); // array iniciado para evitar erros caso não aja itens
            foreach ($all_categorias as $categoria) {
                $opt_cate[$categoria->CATE_ID] = $categoria->CATE_NOME;
            }
            echo form_dropdown('CATE_ID', $opt_cate, isset($produto) ? $produto->CATE_ID : null, 'class="form-control" id="CATE_ID"');
            ?>
            <span class="input-group-btn">
                    <a href="<?= site_url('categoria/add'); ?>" class="btn btn-default InWindow"><i
                                class="fa fa-plus"></i></a>
                </span>
        </div>

    </div>

    <div class="col-xs-5">
        <label for="MEDI_ID">Medida</label>
        <div class="input-group">
            <?php
            $opt_medi = array(); // array iniciado para evitar erros caso não aja itens
            foreach ($all_medidas as $medida) {
                $opt_medi[$medida->MEDI_ID] = $medida->MEDI_NOME;
            }
            echo form_dropdown('MEDI_ID', $opt_medi, isset($produto) ? $produto->MEDI_ID : null, 'class="form-control" id="MEDI_ID"');
            ?>
            <span class="input-group-btn">
                    <a href="<?= site_url('medida/add'); ?>" class="btn btn-default InWindow"><i class="fa fa-plus"></i></a>
                </span>
        </div>
    </div>

    <div class="col-xs-2">
        <label for="PRO_PESO">Peso (g)</label>
        <input type="text" name="PRO_PESO" value="<?= isset($produto) ? $produto->PRO_PESO : null ?>"
               class="form-control" id="PRO_PESO"/>
    </div>
</div>

<div class="form-group">
    <hr>

    <?php if (!strstr(uri_string(), 'edit')) { ?>
        <div class="col-xs-3">
            <label for="ESTOQ_VENDA">Venda R$</label>
            <input type="text" name="ESTOQ_VENDA" class="form-control moeda" id="ESTOQ_VENDA"/>
        </div>

        <div class="col-xs-3 no-srv">
            <label for="ESTOQ_CUSTO">Custo R$</label>
            <input type="text" name="ESTOQ_CUSTO" class="form-control moeda" id="ESTOQ_CUSTO"/>
        </div>

        <div class="col-xs-3 no-srv">
            <label for="ESTOQ_ATUAL">Estoq. atual</label>
            <input type="text" name="ESTOQ_ATUAL" class="form-control" id="ESTOQ_ATUAL"/>
        </div>

    <?php } ?>

    <div class="col-xs-3 no-srv">
        <label for="ESTOQ_MIN">Estoq. min</label>
        <input type="text" name="ESTOQ_MIN" value="<?= isset($produto) ? $produto->ESTOQ_MIN : null ?>"
               class="form-control" id="ESTOQ_MIN"/>
    </div>

</div>

<div class="form-group">
    <hr>
    <div class="col-xs-4">
        <?php
        $opt_estatus['a'] = "Ativo";
        $opt_estatus['d'] = "Desativo";

        echo form_dropdown('PRO_ESTATUS', $opt_estatus, isset($produto) ? $produto->PRO_ESTATUS : null, 'class="form-control"');
        ?>
    </div>
    <div class="col-lg-8">
        <button type="submit" class="btn btn-success">Grava</button>
    </div>
</div>
<?php echo form_close(); ?>

<script>

    if (typeof form_produto == 'undefined') {
        form_produto = true;

        // ativa o mascaramento
        $(document).on("change", "#PRO_TIPO", function (e) {
            switch ($(this).val()) {
                case "s":
                    $('.no-srv').hide(1000);
                    $('.kit').hide(1000);
                    break;
                default:
                    $('.no-srv').show(1000);
                    $('.kit').hide(1000);

            }
        });

        $(document).on("click", "#CATE_ID", function () {
            var select = $(this);
            $.ajax("<?= base_url('categoria/lista') ?>").done(function (data) {
                $.each(data, function (key, value) {
                    if (!select.find('option[value="' + value.CATE_ID + '"]').length) {
                        $('<option>').val(value.CATE_ID).text(value.CATE_NOME).appendTo(select);
                    }
                })
            });
        });

        $(document).on("click", "#MEDI_ID", function () {
            var select = $(this);
            $.ajax("<?= base_url('medida/lista') ?>").done(function (data) {
                $.each(data, function (key, value) {
                    if (!select.find('option[value="' + value.MEDI_ID + '"]').length) {
                        $('<option>').val(value.MEDI_ID).text(value.MEDI_NOME).appendTo(select);
                    }
                })
            });
        });

        $(document).on("click", "#random_num", function () {
            $(this).parent('.input-group').children('input').val(utils.GeraCodbarra(13));
        });

    }

</script>