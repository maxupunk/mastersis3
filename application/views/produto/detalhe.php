<div class="row">
    <div class="col-lg-4">
        <img src="<?= base_url('produto/QRcode') ?>?t=<?= $produto->PRO_DESCRICAO ?>">
    </div>
    <div class="col-lg-8">
        <img src="<?= base_url('produto/codbarras/') . $produto->PRO_CODBARRA ?>?t=2&s=70">
    </div>
</div>
<div class="table-responsive">
    <table class="table table-striped table-detalhe">

        <tr>
            <td width="35%" class="titulo">TIPO</td>
            <td><?= $this->mastersis->ProTipo[$produto->PRO_TIPO]; ?></td>
        </tr>

        <tr>
            <td class="titulo">CODIGO DE BARRAS</td>
            <td><?= $produto->PRO_CODBARRA ?></td>
        </tr>

        <tr>
            <td class="titulo">DESCRIÇÃO</td>
            <td><?= $produto->PRO_DESCRICAO ?></td>
        </tr>

        <tr>
            <td class="titulo">CARACTERISTICA TECNICA</td>
            <td><?= $produto->PRO_CARAC_TEC ?></td>
        </tr>

        <tr>
            <td class="titulo">CATEGORIA</td>
            <td><?= $produto->CATE_NOME ?></td>
        </tr>

        <tr>
            <td class="titulo">MEDIDA</td>
            <td><?= $produto->MEDI_NOME ?></td>
        </tr>

        <tr>
            <td class="titulo">PESO</td>
            <td><?= $produto->PRO_PESO ?></td>
        </tr>

        <tr>
            <td class="titulo">ESTATUS</td>
            <td><?= $this->mastersis->Estatus[$produto->PRO_ESTATUS]; ?></td>
        </tr>

    </table>
</div>
<label>Sobre o estoque</label>
<div class="table-responsive">
    <table class="table table-striped table-detalhe">
        <tr>
            <td width="35%" class="titulo">EM ESTOQUE</td>
            <td><?= $produto->ESTOQ_ATUAL; ?></td>
        </tr>
        <tr>
            <td width="35%" class="titulo">ALERTA DE ESTOQUE</td>
            <td><?= $produto->ESTOQ_MIN; ?></td>
        </tr>
    </table>
</div>