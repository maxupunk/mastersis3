<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Mastersis
{

    private $CI;
    public $Estatus = [
        'a' => 'Ativo',
        'd' => 'Desativo'
    ];
    public $ProTipo = [
        'p' => 'Produto',
        's' => 'Serviço'
    ];
    public $Natureza = [
        "R" => "Receita",
        "D" => "Despesa"
    ];
    public $OrdemEstatus = [
        'EA' => 'EM ABERTA',
        'PA' => 'PAGAMENTO EM ANALISE',
        'AP' => 'AGUARDANDO PAGAMENTO',
        'EE' => 'ENVIADO/ESPERANDO',
        'RP' => 'RECEB/ENTREG PARCIAL',
        'RE' => 'RECEBIDO/ENTREGUE',
        'CC' => 'CONCLUIDO',
        'CA' => 'CANCELADA'
    ];
    public $EstatusOs = [
        1 => 'ABERTO',
        2 => 'PENDENTE',
        3 => 'CONCLUIDO',
        4 => 'ENTREGUE'
    ];

    public function __construct()
    {
        $this->CI = &get_instance();
    }

    function real($campo = NULL)
    {
        if ($campo != NULL) {
            return number_format((float)$campo, 2, ",", ".");
        }
    }

    function Data($campo)
    {
        if ($campo and $campo !== "0000-00-00") {
            $date = new DateTime($campo);
            return $date->format('d/m/Y');
        }
    }

    function DataTempo($campo)
    {
        if ($campo and $campo !== "0000-00-00") {
            $date = new DateTime($campo);
            return $date->format('d/m/Y H:i:s');
        }
    }

    function DataToDB($campo)
    {
        if ($campo) {
            return implode("-", array_reverse(explode("/", $campo)));
        }
    }

    function RealToDB($valor)
    {
        $output = str_replace(',', '.', str_replace('.', '', $valor));
        return $output;
    }

    function CompareData($dataInicio = NULL, $dataFinal = NULL)
    {
        // verifica se a data inicial é menor que a final
        if ($dataInicio != NULL and $dataFinal != NULL) {
            //$dataInicio = implode("-", array_reverse(explode("/", $dataInicio)));
            //$dataFinal = implode("-", array_reverse(explode("/", $dataFinal)));
            if ($dataInicio < $dataFinal) {
                return true;
            }
        }
    }

    function roundMoney($num, $nearest = 0.05)
    {
        return round($num * (1 / $nearest)) * $nearest;
    }

}
