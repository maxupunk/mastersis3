<?php
class MY_Form_validation extends CI_Form_validation
{
    function edit_unique($value, $params)
    {
        $this->set_message('edit_unique', "O campo %s já esta em uso!");
        list($table, $field, $coluna ,$current_id) = explode(".", $params);
        $result = $this->CI->db->where($field, $value)->get($table)->row();
        return ($result && $result->$coluna != $current_id) ? FALSE : TRUE;
    }
    
}
