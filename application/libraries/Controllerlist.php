<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/* * *
 * $this->load->library('controllerlist');
 * print_r($this->controllerlist->getControllers());
 * @author Peter Prins 
 */

class ControllerList {

    private $CI;
    private $aControllers;
    private $db = true;
    // coloca tudo os novas calsse como privado true ou false
    private $privado = true;

    // Construct
    function __construct() {
        // Get Codeigniter instance 
        $this->CI = get_instance();

        define('EXT', '.php');

        // Get all controllers 
        $this->setControllers();
    }

    // Verifica se já existe a MEDOTO e CLASS na tabela
    function check_tabela($classe, $metodo) {
        $this->CI->db->where(array('METOD_CLASS' => $classe, 'METOD_METODO' => $metodo));
        $result = $this->CI->db->get('METODOS')->row();
        // Se este metodo ainda não existir na tabela será cadastrado
        if (count($result) == 0) {
            $data['METOD_CLASS'] = $classe;
            $data['METOD_METODO'] = $metodo;
            $data['METOD_PRIVADO'] = $this->privado;
            $this->CI->db->insert('METODOS', $data);
        }
    }

    /**
     * Return all controllers and their methods
     * @return array
     */
    public function getControllers() {
        return $this->aControllers;
    }

    /**
     * Set the array holding the controller name and methods
     */
    public function setControllerMethods($p_sControllerName, $p_aControllerMethods) {
        $this->aControllers[$p_sControllerName] = $p_aControllerMethods;
    }

    /**
     * Search and set controller and methods.
     */
    private function setControllers() {
        // Loop through the controller directory
        foreach (glob(APPPATH . 'controllers/*') as $controller) {

            if (pathinfo($controller, PATHINFO_EXTENSION) == "php") {
                // value is no directory get controller name				
                $controllername = basename($controller, EXT);

                // Load the class in memory (if it's not loaded already)
                if (!class_exists($controllername)) {
                    $this->CI->load->file($controller);
                }

                // Add controller and methods to the array
                $aMethods = get_class_methods($controllername);
                $aUserMethods = array();
                if (is_array($aMethods)) {
                    foreach ($aMethods as $method) {
                        if ($method != '__construct' &&
                                $method != 'get_instance' &&
                                $method != "page_construct" &&
                                $method != "send_json" &&
                                $method != "getconf" &&
                                $method != $controllername) {
                            
                            $aUserMethods[] = $method;
                            // veririfca se já foi cadastrado a classe e o metodo
                            $this->check_tabela($controllername, $method);
                            
                        }
                    }
                }

                $this->setControllerMethods($controllername, $aUserMethods);
            }
        }
    }

}

// EOF