<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MastersisCart {

    protected $CI;
    protected $_cart_contents = array();
    protected $id;

    public function __construct() {
        $this->CI = & get_instance();
    }

    public function id($id) {
        $this->CI->load->driver('session');
        $this->id = $id;
        $this->_cart_contents = $this->CI->session->userdata('cart_contents_' . $id);
        if ($this->_cart_contents === NULL) {
            $this->_cart_contents = array('CART_TOTAL' => 0, 'TOTAL_ITENS' => 0, 'ORDEM_ID' => null);
        }

        log_message('info', 'Cart Class Initialized');
    }

    public function insert($items = array()) {
        // Was any cart data passed? No? Bah...
        if (!is_array($items) OR count($items) === 0) {
            log_message('error', 'The insert method must be passed an array containing data.');
            return FALSE;
        }

        $save_cart = FALSE;
        if (isset($items['PRO_ID'])) {
            if (($id = $this->_insert($items))) {
                $save_cart = TRUE;
            }
        } else {
            foreach ($items as $val) {
                if (is_array($val) && isset($val['PRO_ID'])) {
                    if ($this->_insert($val)) {
                        $save_cart = TRUE;
                    }
                }
            }
        }

        // Save the cart data if the insert was successful
        if ($save_cart === TRUE) {
            $this->_save_cart();
            return isset($val['PRO_ID']) ? $val['PRO_ID'] : TRUE;
        }

        return FALSE;
    }

    protected function _insert($items = array()) {
        // Was any cart data passed? No? Bah...
        if (!is_array($items) OR count($items) === 0) {
            log_message('error', 'The insert method must be passed an array containing data.');
            return FALSE;
        }

        // --------------------------------------------------------------------
        // Does the $items array contain an PRO_ID, quantity, LIST_PED_VALOR, and PRO_DESCRICAO?  These are required
        if (!isset($items['PRO_ID'], $items['LIST_PED_QNT'], $items['LIST_PED_VALOR'], $items['PRO_DESCRICAO'])) {
            log_message('error', 'The cart array must contain a product ID, quantity, LIST_PED_VALOR, and PRO_DESCRICAO.');
            return FALSE;
        }

        // --------------------------------------------------------------------
        // Prep the quantity. It can only be a number.  Duh... also trim any leading zeros
        $items['LIST_PED_QNT'] = (float) $items['LIST_PED_QNT'];

        // If the quantity is zero or blank there's nothing for us to do
        if ($items['LIST_PED_QNT'] == 0) {
            return FALSE;
        }

        // --------------------------------------------------------------------

        $items['LIST_PED_VALOR'] = (float) $items['LIST_PED_VALOR'];

        if (isset($items['options']) && count($items['options']) > 0) {
            $id = $items['PRO_ID'] . serialize($items['options']);
        } else {
            $id = $items['PRO_ID'];
        }

        // --------------------------------------------------------------------
        // Now that we have our unique "row ID", we'll add our cart items to the master array
        // grab quantity if it's already there and add it on
        $old_quantity = isset($this->_cart_contents[$id]['LIST_PED_QNT']) ? (int) $this->_cart_contents[$id]['LIST_PED_QNT'] : 0;

        // Re-create the entry, just to make sure our index contains only the data from this submission
        //$items['rowid'] = $rowid;
        $items['LIST_PED_QNT'] += $old_quantity;
        $this->_cart_contents[$id] = $items;

        return $id;
    }

    public function update($items = array()) {
        // Was any cart data passed?
        if (!is_array($items) OR count($items) === 0) {
            return FALSE;
        }

        // You can either update a single product using a one-dimensional array,
        // or multiple products using a multi-dimensional one.  The way we
        // determine the array type is by looking for a required array key PRO_DESCRICAOd "rowid".
        // If it's not found we assume it's a multi-dimensional array
        $save_cart = FALSE;
        if (isset($items['PRO_ID'])) {
            if ($this->_update($items) === TRUE) {
                $save_cart = TRUE;
            }
        } else {
            foreach ($items as $val) {
                if (is_array($val) && isset($val['PRO_ID'])) {
                    if ($this->_update($val) === TRUE) {
                        $save_cart = TRUE;
                    }
                }
            }
        }

        // Save the cart data if the insert was successful
        if ($save_cart === TRUE) {
            $this->_save_cart();
            return TRUE;
        }

        return FALSE;
    }

    protected function _update($items = array()) {
        // Without these array indexes there is nothing we can do
        if (!isset($items['PRO_ID'], $this->_cart_contents[$items['PRO_ID']])) {
            return FALSE;
        }

        // Prep the quantity
        if (isset($items['LIST_PED_QNT'])) {
            $items['LIST_PED_QNT'] = (float) $items['LIST_PED_QNT'];
            if ($items['LIST_PED_QNT'] == 0) {
                unset($this->_cart_contents[$items['PRO_ID']]);
                return TRUE;
            }
        }

        // find updatable keys
        $keys = array_intersect(array_keys($this->_cart_contents[$items['PRO_ID']]), array_keys($items));

        if (isset($items['LIST_PED_VALOR'])) {
            $items['LIST_PED_VALOR'] = (float) $items['LIST_PED_VALOR'];
        }

        // product PRO_ID & PRO_DESCRICAO shouldn't be changed
        foreach (array_diff($keys, array('PRO_ID', 'PRO_DESCRICAO')) as $key) {
            $this->_cart_contents[$items['PRO_ID']][$key] = $items[$key];
        }

        return TRUE;
    }

    protected function _save_cart() {

        $this->_cart_contents['TOTAL_ITENS'] = $this->_cart_contents['CART_TOTAL'] = 0;
        foreach ($this->_cart_contents as $key => $val) {
            if (!is_array($val) OR ! isset($val['LIST_PED_VALOR'], $val['LIST_PED_QNT'])) {
                continue;
            }

            $this->_cart_contents['CART_TOTAL'] += ($val['LIST_PED_VALOR'] * $val['LIST_PED_QNT']);
            $this->_cart_contents['TOTAL_ITENS'] += $val['LIST_PED_QNT'];
            $this->_cart_contents[$key]['SUBTOTAL'] = ($this->_cart_contents[$key]['LIST_PED_VALOR'] * $this->_cart_contents[$key]['LIST_PED_QNT']);
        }

        if (count($this->_cart_contents) <= 2) {
            $this->CI->session->unset_userdata('cart_contents_' . $this->id);

            return FALSE;
        }

        $this->CI->session->set_userdata(array('cart_contents_' . $this->id => $this->_cart_contents));

        return TRUE;
    }

    function setpedido($PED_ID = null) {

        if (!$PED_ID) {
            return FALSE;
        }

        $this->_cart_contents['ORDEM_ID'] = $PED_ID;

        $this->CI->session->set_userdata(array('cart_contents_' . $this->id => $this->_cart_contents));

        return TRUE;
    }

    function getpedido($PED_ID = null) {

        if ($PED_ID !== null) {
            $this->setpedido($PED_ID);
        }

        if (!$this->_cart_contents['ORDEM_ID'] and ! $PED_ID) {
            return FALSE;
        }

        foreach ($this->_cart_contents as $key => $val) {
            if (!is_array($val)) {
                continue;
            }

            $retorno[] = array(
                'PRO_ID' => $this->_cart_contents[$key]['PRO_ID'],
                'LIST_PED_QNT' => $this->_cart_contents[$key]['LIST_PED_QNT'],
                'LIST_PED_VALOR' => $this->_cart_contents[$key]['LIST_PED_VALOR'],
                'ORDEM_ID' => $this->_cart_contents['ORDEM_ID'],
            );
        }

        return isset($retorno) ? $retorno : FALSE;
    }

    public function pedido() {
        return isset($this->_cart_contents['ORDEM_ID']) ? $this->_cart_contents['ORDEM_ID'] : false;
    }

    public function total() {
        return $this->_cart_contents['CART_TOTAL'];
    }

    public function remove($id) {
        // unset & save
        unset($this->_cart_contents[$id]);
        $this->_save_cart();
        return TRUE;
    }

    public function total_itens() {
        return $this->_cart_contents['TOTAL_ITENS'];
    }

    public function contents($newest_first = FALSE) {
        // do we want the newest first?
        $cart = ($newest_first) ? array_reverse($this->_cart_contents) : $this->_cart_contents;

        // Remove these so they don't create a problem when showing the cart table
        unset($cart['TOTAL_ITENS']);
        unset($cart['CART_TOTAL']);
        unset($cart['ORDEM_ID']);

        return $cart;
    }

    public function get_item($PRO_ID) {
        return (in_array($PRO_ID, array('TOTAL_ITENS', 'CART_TOTAL'), TRUE) OR ! isset($this->_cart_contents[$PRO_ID])) ? FALSE : $this->_cart_contents[$PRO_ID];
    }

    public function has_options($PRO_ID = '') {
        return (isset($this->_cart_contents[$PRO_ID]['options']) && count($this->_cart_contents[$PRO_ID]['options']) !== 0);
    }

    public function product_options($PRO_ID = '') {
        return isset($this->_cart_contents[$PRO_ID]['options']) ? $this->_cart_contents[$PRO_ID]['options'] : array();
    }

    public function format_number($n = '') {
        return ($n === '') ? '' : number_format((float) $n, 2, '.', ',');
    }

    public function destroy() {
        $this->_cart_contents = array('CART_TOTAL' => 0, 'TOTAL_ITENS' => 0);
        $this->CI->session->unset_userdata('cart_contents_' . $this->id);
    }

}
