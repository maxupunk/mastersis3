<?php

class Config extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
    }


    public function index() {
        $this->page_construct("config/index");
    }

    public function configs() {
        $this->send_json("");
    }

}
