<?php

class Venda extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
    }

    function index() {
        $this->page_construct("venda/index");
    }

    function excluir($ped_id) {

        $this->load->library('MastersisCart');
        $this->mastersiscart->id("vnd");
        $this->load->model('Ordens_model');
        $query_ped = $this->Pedido_model->get($ped_id);

        if ($query_ped) {

            $this->db->trans_start();

            $this->load->model('Venda_model');
            $this->Venda_model->abrir($ped_id);

            $this->Pedido_model->delete($ped_id);

            $this->load->model('Financeiro_model');
            $this->Financeiro_model->delByOrdemID($ped_id);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $this->send_json(['msg' => 'Erro: problema com o fechamento da venda! (Falha no DB)']);
            } else {
                $this->db->trans_commit();
                $this->mastersiscart->destroy();
                $this->send_json(['ok' => 'ok']);
            }
        } else {
            $this->send_json(['msg' => 'Erro: O ordem não existe ou foi apagado!']);
        }
    }

    function cart($cmd = NULL) {

        if (isset($cmd)) {

            $this->load->library('MastersisCart');
            $this->mastersiscart->id("vnd");

            $post = $this->input->post();

            switch ($cmd) {

                case 'add':
                    // adicionar item
                    $this->load->model('Produto_model');
                    $produto = $this->Produto_model->get($post['id']);

                    if (isset($produto->PRO_ID)) {
                        if ($produto->PRO_TIPO == "p" and $produto->ESTOQ_ATUAL > 0 or $produto->PRO_ESTATUS == "a") {
                            $data = array(
                                'PRO_ID' => $produto->PRO_ID,
                                'PRO_CODBARRA' => $produto->PRO_CODBARRA,
                                'PRO_DESCRICAO' => $produto->PRO_DESCRICAO,
                                'LIST_PED_QNT' => 1,
                                'MEDI_SIGLA' => $produto->MEDI_SIGLA,
                                'LIST_PED_VALOR' => $produto->ESTOQ_VENDA,
                            );

                            $this->mastersiscart->insert($data);
                        } else {
                            $this->send_json(['msg' => 'Erro ao adicionar o produto, produto sem estoque']);
                        }
                    } else {
                        $this->send_json(['msg' => 'Erro ao adicionar o produto, considere o seguinte:<br>- Não existe<br>- foi removido']);
                    }
                    break;

                case 'rm':
                    // Remover o item
                    $this->mastersiscart->remove($post['id']);
                    break;

                case 'qnt':
                    // quantidade
                    $this->load->model('Produto_model');
                    $produto = $this->Produto_model->get($post['id']);
                    $data['PRO_ID'] = $post['id'];
                    if ($produto->ESTOQ_ATUAL >= $post['qnt'] or $produto->PRO_TIPO === 's') {
                        $data['LIST_PED_QNT'] = $post['qnt'];
                    } else {
                        $data['LIST_PED_QNT'] = $produto->ESTOQ_ATUAL;
                        $json_out['msg'] = "Não existe estoque suficiente!!";
                    }
                    $this->mastersiscart->update($data);
                    break;

                case 'lmp':
                    // limpar
                    $this->mastersiscart->destroy();
                    break;
            }

            $json_out['total'] = $this->mastersiscart->total();
            $json_out['produtos'] = $this->mastersiscart->contents();
            $json_out['ordem'] = $this->mastersiscart->pedido();
            $this->send_json($json_out);
            
        } else {
            $this->page_construct("venda/cart");
        }
    }

}
