<?php

class Endereco extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
        $this->load->model('Endereco_model');
    }

    function index() {
        $this->page_construct("endereco/index");
    }

    function lista() {
        $mostra = $this->input->get('mostrar');
        $busca = $this->input->get('buscar') ? $this->input->get('buscar') : null;

        $regpload = $this->getconf("reg_por_load");
        $output = $this->Endereco_model->get_all($busca, $regpload, $mostra);

        $this->send_json($output);
        //echo "<pre>";
        //print_r($output);
    }

    function add() {

        $this->load->library('form_validation');

        $this->form_validation->set_rules('ESTA_NOME', 'ESTADO', 'required');
        $this->form_validation->set_rules('ESTA_UF', 'UF', 'required');
        $this->form_validation->set_rules('CIDA_NOME', 'CIDADE', 'required');
        $this->form_validation->set_rules('BAIRRO_NOME', 'BAIRRO', 'required');
        $this->form_validation->set_rules('RUA_NOME', 'RUA', "required");

        if ($this->form_validation->run()) {

            $this->db->trans_start();
            // estado
            if (!$this->input->post('ESTA_ID')) {
                $params_estado = [
                    'ESTA_NOME' => $this->input->post('ESTA_NOME'),
                    'ESTA_UF' => strtoupper($this->input->post('ESTA_UF'))
                ];
                $estado_id = $this->Endereco_model->add_estado($params_estado);
            } else {
                $estado_id = $this->input->post('ESTA_ID');
            }
            // cidade
            if (!$this->input->post('CIDA_ID')) {
                $params_cidade = [
                    'CIDA_NOME' => $this->input->post('CIDA_NOME'),
                    'ESTA_ID' => $estado_id
                ];

                $cidade_id = $this->Endereco_model->add_cidade($params_cidade);
            } else {
                $cidade_id = $this->input->post('CIDA_ID');
            }
            // bairro
            if (!$this->input->post('BAIRRO_ID')) {
                $params_bairro = [
                    'BAIRRO_NOME' => $this->input->post('BAIRRO_NOME'),
                    'CIDA_ID' => $cidade_id
                ];

                $bairro_id = $this->Endereco_model->add_bairro($params_bairro);
            } else {
                $bairro_id = $this->input->post('BAIRRO_ID');
            }

            $params = [
                'RUA_NOME' => $this->input->post('RUA_NOME'),
                'BAIRRO_ID' => $bairro_id
            ];

            $this->Endereco_model->add_rua($params);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $this->send_json(['msg' => 'Erro: problama no DB']);
            } else {
                $this->send_json(['ok' => 'ok']);
            }
        } else {

            if ($this->form_validation->error_array()) {
                $this->send_json($this->form_validation->error_array());
                exit;
            }

            $this->load->view('endereco/form');
        }
    }

    function edit($RUA_ID) {
        // check if the rua exists before trying to edit it
        $rua = $this->Endereco_model->get_rua($RUA_ID);

        if (isset($rua->RUA_ID)) {

            $this->load->library('form_validation');

            $estado_id = $this->input->post('ESTA_ID');
            $cidade_id = $this->input->post('CIDA_ID');
            $bairro_id = $this->input->post('BAIRRO_ID');
            $rua_id = $this->input->post('RUA_ID');

            $this->form_validation->set_rules('ESTA_ID', 'ID DO ESTADO', 'required');
            $this->form_validation->set_rules('ESTA_NOME', 'ESTADO', "required|edit_unique[ESTADOS.ESTA_NOME.ESTA_ID.$estado_id]");
            $this->form_validation->set_rules('ESTA_UF', 'ESTADO', "required|edit_unique[ESTADOS.ESTA_UF.ESTA_ID.$estado_id]");
            $this->form_validation->set_rules('CIDA_ID', 'ID DA CIDADE', 'required');
            $this->form_validation->set_rules('CIDA_NOME', 'CIDADE', "required|edit_unique[CIDADES.CIDA_NOME.CIDA_ID.$cidade_id]");
            $this->form_validation->set_rules('BAIRRO_ID', 'ID DO BAIRRO', 'required');
            $this->form_validation->set_rules('BAIRRO_NOME', 'BAIRRO', "required|edit_unique[BAIRROS.BAIRRO_NOME.BAIRRO_ID.$bairro_id]");
            $this->form_validation->set_rules('RUA_ID', 'ID DA RUA', "required");
            $this->form_validation->set_rules('RUA_NOME', 'RUA', "required|edit_unique[RUAS.RUA_NOME.RUA_ID.$rua_id]");

            if ($this->form_validation->run()) {

                $this->db->trans_start();
                $post = $this->input->post();

                // estado
                $params_estado = [
                    'ESTA_NOME' => $post['ESTA_NOME'],
                    'ESTA_UF' => strtoupper($post['ESTA_UF'])
                ];
                $this->Endereco_model->update_estado($post['ESTA_ID'], $params_estado);

                // cidade
                $params_cidade = [
                    'CIDA_NOME' => $post['CIDA_NOME'],
                    'ESTA_ID' => $post['ESTA_ID']
                ];
                $this->Endereco_model->update_cidade($post['CIDA_ID'], $params_cidade);

                // bairro
                $params_bairro = [
                    'BAIRRO_NOME' => $post['BAIRRO_NOME'],
                    'CIDA_ID' => $post['CIDA_ID']
                ];
                $this->Endereco_model->update_bairro($post['BAIRRO_ID'], $params_bairro);

                // RUA
                $params_rua = [
                    'RUA_NOME' => $post['RUA_NOME'],
                    'BAIRRO_ID' => $post['BAIRRO_ID']
                ];
                $this->Endereco_model->update_rua($post['RUA_ID'], $params_rua);

                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE) {
                    $this->send_json(['msg' => 'Erro: problama no DB']);
                } else {
                    $this->send_json(['ok' => 'ok']);
                }
            } else {

                if ($this->form_validation->error_array()) {
                    $this->send_json($this->form_validation->error_array());
                    exit;
                }

                $data['end'] = $rua;

                $this->load->view('endereco/form', $data);
            }
        }
    }

    function remove($RUA_ID) {
        $rua = $this->Endereco_model->get_rua($RUA_ID);
        if (isset($rua->RUA_ID)) {
            $delet_rua = $this->Endereco_model->delete_rua($RUA_ID);
            if ($delet_rua) {
                $this->send_json(['ok' => 'ok']);
            }
        }
    }

    function busca() {

        $busca = $this->input->get('term');

        $retorno = $this->Endereco_model->get_all($busca);

        $json_array = array();
        foreach ($retorno as $rua) {
            array_push($json_array, array('id' => $rua->RUA_ID, 'endereco' => $rua->RUA_NOME . " - " . $rua->BAIRRO_NOME . " | " . $rua->CIDA_NOME . "-" . $rua->ESTA_UF));
        }

        $this->send_json($json_array);
    }

    function buscaestado() {

        $busca = $this->input->get('term');

        $retorno = $this->Endereco_model->busca_estados($busca);

        $this->send_json($retorno);
    }

    function buscacidade($id = null) {

        $busca = $this->input->get('term');

        $retorno = $this->Endereco_model->busca_cidades($busca, $id);

        $this->send_json($retorno);
    }

    function buscabairro($id = null) {

        $busca = $this->input->get('term');

        $retorno = $this->Endereco_model->busca_bairros($busca, $id);

        $this->send_json($retorno);
    }

    function buscarua($id = null) {

        $busca = $this->input->get('term');

        $retorno = $this->Endereco_model->busca_ruas($busca, $id);

        $this->send_json($retorno);
    }

}
