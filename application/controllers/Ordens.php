<?php

class Ordens extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
        $this->load->model('Ordens_model');
    }

    function alterar($tipo, $ORDEM_ID)
    {

        $ordem = $this->Ordens_model->get($ORDEM_ID);

        if ($ordem->ORDEM_ID) {

            $lst_produto = $this->Ordens_model->get_lst_produto($ORDEM_ID)->result_array();

            $this->load->library('MastersisCart');
            $this->mastersiscart->id($tipo);
            $this->mastersiscart->destroy();
            $this->mastersiscart->setpedido($ordem->ORDEM_ID);
            $this->mastersiscart->insert($lst_produto);

            if ($tipo === "vnd") {
                $this->send_json(['url' => base_url('venda/cart')]);
            } else {
                if ($ordem->ORDEM_ESTATUS <= 4) {
                    $this->send_json(['url' => base_url('compra/cart')]);
                } else {
                    $this->send_json(['msg' => 'Você não pode alterar esse ordem!']);
                }
            }
        }
    }

    function faturar($tipo)
    {
        $this->load->library('form_validation');

        $post = $this->input->post();

        if (isset($post['FINANC_ESTATUS']) and $post['FINANC_ESTATUS'] === "pp") {
            $this->form_validation->set_rules('FINANC_PORCONTA', 'Valor pago', 'required');
        }

        $this->form_validation->set_rules('PES_ID', 'PESSOA', 'required');
        $this->form_validation->set_rules('FPG_ID', 'FORMA DE PAGAMENTO', 'required');

        $this->load->library('MastersisCart');
        $this->mastersiscart->id($tipo);
        $ordem_cart_id = $this->mastersiscart->pedido();

        if ($this->form_validation->run()) {

            $this->db->trans_start();

            $params = [
                'USUARIO_ID' => $this->session->USUARIO_ID,
                'PES_ID' => $post['PES_ID'],
                'ORDEM_DATA' => date("Y-m-d H:i:s"),
                'ORDEM_LOCAL' => "l",
                'ORDEM_OBS' => $post['ORDEM_OBS'],
                'ORDEM_N_DOC' => $post['ORDEM_N_DOC'],
                'ORDEM_IMPOSTO' => $this->mastersis->RealToDB($post['ORDEM_IMPOSTO']),
                'ORDEM_FRETE' => $this->mastersis->RealToDB($post['ORDEM_FRETE']),
                'ORDEM_TIPO' => ($tipo === "cmp" ? "c" : "v"),
                'FPG_ID' => $post['FPG_ID'],
                'ORDEM_NPARC' => $post['NPARCELA'],
                'ORDEM_DESCO' => $this->mastersis->RealToDB($post['ORDEM_DESCO']),
                'ORDEM_ACRECIMO' => $this->mastersis->RealToDB($post['ORDEM_ACRECIMO']),
                'ORDEM_ESTATUS' => ($tipo === "cmp" ? "EE" : "RE"),
            ];

            $finan_porconta = (float)$this->mastersis->RealToDB($post['FINANC_PORCONTA']);

            $param_finan = [
                'PES_ID' => $post['PES_ID'],
                'USUARIO_ID' => $this->session->USUARIO_ID,
                'FINANC_NATUREZA' => ($tipo === "cmp" ? "D" : "R"),
                'FINANC_PORCONTA' => $finan_porconta,
                'FINANC_VECIMENTO' => $this->mastersis->DataToDB($post['FINANC_VECIMENTO']),
                'FINANC_DATA' => date('Y-m-d'),
            ];

            $this->load->model('Financeiro_model');

            $this->load->model('Venda_model');
            // entra aqui se for venda ou compra
            if ($ordem_cart_id) {
                // entra aqui se existir um carrinho de venda
                if ($this->Ordens_model->update($ordem_cart_id, $params)) {

                    $total = (float)$this->Ordens_model->Total($ordem_cart_id);

                    if ($tipo === "vnd") {
                        $this->Venda_model->abrir($ordem_cart_id);
                    }

                    $itens = $this->mastersiscart->getpedido($ordem_cart_id);
                    $this->Ordens_model->upd_lst_produto($ordem_cart_id, $itens);

                    if ($finan_porconta and $total > $finan_porconta) {
                        $param_finan['FINANC_ESTATUS'] = "pp";
                    }

                    if ($total == $finan_porconta) {
                        $param_finan['FINANC_ESTATUS'] = "pg";
                        $param_finan['FINANC_DATA_PG'] = date("Y-m-d");
                    }
                    $param_finan['FINANC_VALOR'] = $total;

                    $this->Financeiro_model->updadeByOrdemID($ordem_cart_id, $param_finan);

                    if ($tipo === "vnd") {
                        $this->Venda_model->fecha($ordem_cart_id, $post['PES_ID']);

                    }
                }

            } else {

                $ordem_id = $this->Ordens_model->add($params);

                if ($ordem_id) {

                    $itens = $this->mastersiscart->getpedido($ordem_id);

                    $this->Ordens_model->add_lst_produto($itens);

                    $total = (float)$this->Ordens_model->Total($ordem_id);

                    $param_finan['ORDEM_ID'] = $ordem_id;
                    $param_finan['FINANC_VALOR'] = $total;

                    if ($finan_porconta and $total > $finan_porconta) {
                        $param_finan['FINANC_ESTATUS'] = "pp";
                    }

                    if ($total == $finan_porconta) {
                        $param_finan['FINANC_ESTATUS'] = "pg";
                        $param_finan['FINANC_DATA_PG'] = date("Y-m-d");
                    }

                    $this->Financeiro_model->add($param_finan);

                    if ($tipo === "vnd") {
                        $this->Venda_model->fecha($ordem_id, $post['PES_ID']);
                    }

                }

            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $this->mastersiscart->destroy();
                $this->send_json(['msg' => 'Erro: problema com o fechamento da venda! (Falha no DB)']);
            } else {
                $this->db->trans_commit();
                $this->mastersiscart->destroy();
                $this->send_json(['ok' => 'ok']);
            }

        } else {

            if ($this->mastersiscart->total()) {
                if ($this->form_validation->error_array()) {
                    $this->send_json($this->form_validation->error_array());
                    exit;
                }

                $this->load->model('Forma_pg_model');
                $data['form_pgs'] = $this->Forma_pg_model->get_all(null, null, null, TRUE);
                $data['total'] = $this->mastersiscart->total();

                if ($ordem_cart_id) {
                    $data['Ordem'] = $this->Ordens_model->get($ordem_cart_id);

                    $data['form_pg_pcs'] = $this->Forma_pg_model->get($data['Ordem']->FPG_ID);

                    $this->load->model('Financeiro_model');
                    $data['financeiro'] = $this->Financeiro_model->getByOrdemID($ordem_cart_id)->row();
                }

                $this->load->view("ordem/fatura", $data);
            } else {
                $this->send_json(['msg' => 'Não existe itens na compra!']);
            }
        }
    }

    function lista($tipo)
    {
        $mostra = $this->input->get('mostrar');
        $busca = $this->input->get('buscar') ? $this->input->get('buscar') : null;

        $regpload = $this->getconf("reg_por_load");
        $ordens = $this->Ordens_model->get_all($busca, $regpload, $mostra, $tipo)->result();

        $this->send_json($ordens);
    }

    function detalhe($ORDEM_ID = null)
    {

        if ($ORDEM_ID !== null) {
            $ordem = $this->Ordens_model->get($ORDEM_ID);

            if ($ordem->ORDEM_ID) {
                $this->load->library('form_validation');

                $data['Ordem'] = $ordem;
                $data['lst_produto'] = $this->Ordens_model->get_lst_produto($ORDEM_ID)->result();
                $this->load->model('Pessoa_model');

                $this->load->view('ordem/detalhe', $data);
            } else {
                echo "Erro: O ordem que você tentou ver detalhes foi deletado ou não existe.";
            }
        } else {
            echo "Não há uma ordem vinculada a esse item.";
        }
    }

    function receber($ORDEM_ID)
    {
        $ordem = $this->Ordens_model->get($ORDEM_ID);
        if ($ordem->ORDEM_ID) {
            if ($ordem->ORDEM_ESTATUS !== "RE") {
                if ($ordem->ORDEM_ESTATUS !== "CC") {

                    $this->load->library('form_validation');
                    $this->form_validation->set_rules('ORDEM_ID', 'Ordem', 'required');

                    if ($this->form_validation->run()) {

                        $post = $this->input->post();

                        $this->db->trans_start();

                        $this->load->model('Compra_model');
                        $this->Compra_model->fecha($ORDEM_ID);

                        $QntItensPend = $this->Ordens_model->get_rest_lst_pro($ORDEM_ID);
                        // se falta 0 itens para ser baixado vai para concluido
                        $ped_params['ORDEM_ESTATUS'] = ($QntItensPend == 0) ? 'RE' : 'RP';
                        $ped_params['ORDEM_DESCO'] = $this->mastersis->RealToDB($post['ORDEM_DESCO']);
                        $ped_params['ORDEM_ACRECIMO'] = $this->mastersis->RealToDB($post['ORDEM_ACRECIMO']);
                        $ped_params['ORDEM_IMPOSTO'] = $this->mastersis->RealToDB($post['ORDEM_IMPOSTO']);

                        $this->Ordens_model->update($ORDEM_ID, $ped_params);

                        $this->load->model('Financeiro_model');
                        $param_finan['FINANC_VALOR'] = $this->Ordens_model->Total($ORDEM_ID);
                        $this->Financeiro_model->updadeByOrdemID($ORDEM_ID, $param_finan);

                        if ($this->db->trans_status() === FALSE) {
                            $this->db->trans_rollback();
                            $this->send_json(['msg' => 'Erro: problema no banco de dados! (Falha no DB)']);
                        } else {
                            $this->db->trans_commit();
                            $this->send_json(['ok' => 'ok']);
                        }
                    } else {
                        $data['Ordem'] = $ordem;
                        $data['lst_produto'] = $this->Ordens_model->get_lst_produto($ORDEM_ID)->result();

                        $this->load->view('ordem/receber', $data);
                    }
                } else {
                    $this->send_json(['msg' => 'A ordem já foi concluida']);
                }
            } else {
                $this->send_json(['msg' => 'A ordem já foi recebido']);
            }
        } else {
            $this->send_json(['msg' => 'O ordem não existe!']);
        }
    }

    // seta recebido
    public function confereproduto()
    {
        $post = $this->input->post();
        $data = $this->Ordens_model->set_produto_estatus($post['ped_id'], $post['pro_id'], $post['estatus']);
        if (!$data) {
            $this->send_json(['msg' => 'Erro ao alterar, esse produto pode já ter sido recebido!']);
        }
    }

}
