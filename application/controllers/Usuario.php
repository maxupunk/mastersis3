<?php

class Usuario extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
        $this->load->model('Usuario_model');
    }

    function index() {
        $data['metodos'] = $this->Usuario_model->metodos();
        $this->page_construct("usuario/index", $data);
    }

    function lista() {

        $mostra = $this->input->get('mostrar');
        $busca = $this->input->get('buscar') ? $this->input->get('buscar') : null;

        $regpload = $this->getconf("reg_por_load");
        $output = $this->Usuario_model->get_all($busca, $regpload, $mostra);

        $this->send_json($output);
    }

    function add() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('PES_ID', 'PESSOA', 'required|is_unique[USUARIOS.PES_ID]');
        $this->form_validation->set_rules('USUARIO_LOGIN', 'LOGIN', 'required|is_unique[USUARIOS.USUARIO_LOGIN]');
        $this->form_validation->set_rules('USUARIO_SENHA', 'SENHA', 'required');
        $this->form_validation->set_rules('USUARIO_SENHARE', 'REPITA A SENHA', 'required|matches[USUARIO_SENHA]');
        $this->form_validation->set_rules('CARG_ID', 'CARGO', 'required');

        if ($this->form_validation->run()) {
            $senha = $this->input->post('USUARIO_SENHA');
            $params = array(
                'CARG_ID' => $this->input->post('CARG_ID'),
                'PES_ID' => $this->input->post('PES_ID'),
                'USUARIO_APELIDO' => $this->input->post('USUARIO_APELIDO'),
                'USUARIO_LOGIN' => $this->input->post('USUARIO_LOGIN'),
                'USUARIO_SENHA' => password_hash($senha, PASSWORD_DEFAULT),
                'USUARIO_ESTATUS' => $this->input->post('USUARIO_ESTATUS'),
            );

            $usuario = $this->Usuario_model->add($params);

            if ($usuario) {
                $this->send_json(['ok' => 'ok']);
            } else {
                $this->send_json(['msg' => 'Erro: ao cadastrar o usuario! (Falha no DB)']);
            }
        } else {

            if ($this->form_validation->error_array()) {
                $this->send_json($this->form_validation->error_array());
                exit;
            }

            $this->load->model('Cargo_model');
            $data['all_cargos'] = $this->Cargo_model->get_all();

            $this->load->view('usuario/add', $data);
        }
    }

    function edit($USUARIO_ID) {

        $usuario = $this->Usuario_model->get($USUARIO_ID);

        if (isset($usuario->USUARIO_ID)) {
            $this->load->library('form_validation');

            $login = $this->input->post('USUARIO_LOGIN');

            $this->form_validation->set_rules('CARG_ID', 'CARG ID', 'required');
            $this->form_validation->set_rules('USUARIO_LOGIN', 'USUARIO LOGIN', "required|edit_unique[PESSOAS.PES_ID.PES_ID.$login]");

            if ($this->form_validation->run()) {
                $params = array(
                    'CARG_ID' => $this->input->post('CARG_ID'),
                    'USUARIO_APELIDO' => $this->input->post('USUARIO_APELIDO'),
                    'USUARIO_LOGIN' => $this->input->post('USUARIO_LOGIN'),
                    'USUARIO_ESTATUS' => $this->input->post('USUARIO_ESTATUS'),
                );

                $usuario = $this->Usuario_model->update($USUARIO_ID, $params);

                if ($usuario) {
                    $this->send_json(['ok' => 'ok']);
                } else {
                    $this->send_json(['msg' => 'Erro ao alterar o cadastro']);
                }
            } else {

                if ($this->form_validation->error_array()) {
                    $this->send_json($this->form_validation->error_array());
                    exit;
                }

                $data['usuario'] = $usuario;

                $this->load->model('Cargo_model');
                $data['all_cargos'] = $this->Cargo_model->get_all();

                $this->load->view('usuario/edit', $data);
            }
        } else {
            $this->send_json(['msg' => 'O usuario que você tentou editar não existe!']);
        }
    }

    function remove($USUARIO_ID) {
        $usuario = $this->Usuario_model->get($USUARIO_ID);

        if (isset($usuario['USUARIO_ID'])) {
            if ($this->Usuario_model->delete($USUARIO_ID)) {
                $this->send_json(['ok' => 'ok']);
            } else {
                $this->send_json(['msg' => 'Erro: O Itens não pode ser deletado!']);
            }
        } else {
            $this->send_json(['msg' => 'Erro: O usuario que você tentou deletar já foi deletado ou não existe.']);
        }
    }

    function permissoes($USUARIO_ID) {
        $permissoes = $this->Usuario_model->permissoes($USUARIO_ID);
        $this->send_json($permissoes);
    }

    function setpermissao() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('USUARIO_ID', 'USUARIO', 'required');
        $this->form_validation->set_rules('METOD_ID', 'METODO', 'required');

        if ($this->form_validation->run() == TRUE) {
            $post = $this->input->post();
            $params = [
                'USUARIO_ID' => $post['USUARIO_ID'],
                'METOD_ID' => $post['METOD_ID']
            ];

            if ($this->Usuario_model->getpermissao($params)) {
                $this->Usuario_model->delpermissao($params);
                $this->send_json(['rm' => $post['METOD_ID']]);
            } else {
                $this->Usuario_model->setpermissao($params);
                $this->send_json(['add' => $post['METOD_ID']]);
            }
        }
    }
    
    // parte de login do sistema
    function login() {
        $this->load->view("usuario/login");
    }

    function dologin() {
        $post = $this->input->post();

        if ($post['USUARIO_LOGIN'] == "" || $post['USUARIO_SENHA'] == "") {
            $this->send_json(['error' => "Digite o login e senha!"]);
        } else {

            $usuario = $this->Usuario_model->getlogin($post['USUARIO_LOGIN']);
            if ($usuario) {
                $login = password_verify($post['USUARIO_SENHA'], $usuario->USUARIO_SENHA);

                if ($login) {

                    $this->load->model('Estoque_model');

                    $sessao = [
                        'USUARIO_ID' => $usuario->USUARIO_ID,
                        'USUARIO_LOGIN' => $usuario->USUARIO_LOGIN,
                        'USUARIO_APELIDO' => $usuario->USUARIO_APELIDO,
                        'PES_ID' => $usuario->PES_ID,
                        'DATA' => date("d/m/Y h:i:s"),
                        'LOGGET_IN' => TRUE,
                    ];

                    $this->session->set_userdata($sessao);

                    $this->auth->log($this->router->class, $this->router->method, "login");

                    $this->send_json(["url" => base_url()]);
                } else {
                    $this->send_json(['error' => "Senha invalido"]);
                }
            } else {
                $this->send_json(['error' => "Usuario invalido"]);
            }
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        $this->login();
    }
    // fim da parte de login

}
