<?php

class Medida extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
        $this->load->model('Medida_model');
    }

    function index() {
        $this->page_construct("medida/index");
    }

    function lista() {
        $mostra = $this->input->get('mostrar');
        $busca = $this->input->get('buscar') ? $this->input->get('buscar') : null;

        //$regpload = $this->getconf("reg_por_load");
        $output = $this->Medida_model->get_all($busca, null, $mostra);

        $this->send_json($output);
    }

    function add() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('MEDI_NOME', 'NOME DA MEDIDA', 'required|is_unique[MEDIDAS.MEDI_NOME]');
        $this->form_validation->set_rules('MEDI_SIGLA', 'SIGLA', 'required|is_unique[MEDIDAS.MEDI_SIGLA]');

        if ($this->form_validation->run()) {
            $params = array(
                'MEDI_NOME' => $this->input->post('MEDI_NOME'),
                'MEDI_SIGLA' => $this->input->post('MEDI_SIGLA'),
            );

            $medida_id = $this->Medida_model->add($params);
            if ($medida_id) {
                $this->send_json(['ok' => 'ok']);
            } else {
                $this->send_json(['msg' => 'Erro ao cadastrar a unidade de medida! DB.']);
            }
        } else {

            if ($this->form_validation->error_array()) {
                $this->send_json($this->form_validation->error_array());
                exit;
            }

            $this->load->view('medida/form');
        }
    }

    function edit($MEDI_ID) {
        $medida = $this->Medida_model->get($MEDI_ID);

        if (isset($medida->MEDI_ID)) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('MEDI_NOME', 'MEDI NOME', "required|edit_unique[MEDIDAS.MEDI_NOME.MEDI_ID.$medida->MEDI_ID]");
            $this->form_validation->set_rules('MEDI_SIGLA', 'MEDI SIGLA', "required|edit_unique[MEDIDAS.MEDI_SIGLA.MEDI_ID.$medida->MEDI_ID]");

            if ($this->form_validation->run()) {
                $params = array(
                    'MEDI_NOME' => $this->input->post('MEDI_NOME'),
                    'MEDI_SIGLA' => $this->input->post('MEDI_SIGLA'),
                );

                $medida = $this->Medida_model->update($MEDI_ID, $params);

                if ($medida) {
                    $this->send_json(['ok' => 'ok']);
                } else {
                    $this->send_json(['msg' => 'Erro no DB!']);
                }
            } else {

                if ($this->form_validation->error_array()) {
                    $this->send_json($this->form_validation->error_array());
                    exit;
                }

                $data['medida'] = $medida;

                $this->load->view('medida/form', $data);
            }
        } else {
            $this->send_json(['msg' => 'Erro: A categoria que você tentou edita não existe ou foi deletada.']);
        }
    }

    function remove($MEDI_ID) {
        $medida = $this->Medida_model->get($MEDI_ID);

        // check if the medida exists before trying to delete it
        if (isset($medida->MEDI_ID)) {
            if ($this->Medida_model->delete($MEDI_ID)) {
                $this->send_json(['ok' => 'ok']);
            } else {
                $this->send_json(['msg' => 'Erro: O Itens não pode ser deletado!']);
            }
        } else {
            $this->send_json(['msg' => 'Erro: A medida que você tentou deletar já foi deletado ou não existe.']);
        }
    }

}
