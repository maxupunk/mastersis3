<?php

class Relatorio extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
    }

    //venda inicio
    function venda() {
        $this->load->library('form_validation');
        $this->load->model('Usuario_model');
        $data['all_usuario'] = $this->Usuario_model->get_all();
        $this->page_construct("relatorio/venda", $data);
    }

    function vndjson() {

        $post = $this->input->post();
        $this->load->model('Ordens_model');
        $usuario_id = isset($post['usu']) ? $post['usu'] : null;
        $dti = $this->mastersis->DataToDB($post['dti']);
        $dtf = $this->mastersis->DataToDB($post['dtf']);

        $regpload = $this->getconf("reg_por_load");
        $inicio = (int) $post['ini'];

        $conteudo = $this->Ordens_model->get_all(null, $regpload, $inicio, "v", $dti, $dtf, $usuario_id)->result();

        $total = 0;
        foreach ($conteudo as $value) {
            $total += $value->TOTAL;
        }

        $retorno['conteudo'] = $conteudo;
        $retorno['total'] = $total;

        $this->send_json(isset($retorno) ? $retorno : "");
    }

    // Estoque inicio
    function estoque($stt = NULL) {
        $this->load->model('Estoque_model');
        $data['produtos'] = $this->Estoque_model->estatus($stt)->result();
        $this->page_construct("relatorio/estoque", $data);
    }

    // Financeiro inicio
    function financeiro() {
        $this->load->library('form_validation');
        $this->load->model('Usuario_model');
        $data['all_usuario'] = $this->Usuario_model->get_all();
        $this->page_construct("relatorio/financeiro", $data);
    }

    function financjson() {
        $post = $this->input->post();
        $this->load->model('Financeiro_model');
        $user_id = isset($post['usu']) ? $post['usu'] : null;
        $dti = $this->mastersis->DataToDB($post['dti']);
        $dtf = $this->mastersis->DataToDB($post['dtf']);

        $regpload = $this->getconf("reg_por_load");
        $inicio = (int) $post['ini'];

        $conteudo = $this->Financeiro_model->get_all(
                        null, $regpload, $inicio, $dti, $dtf, $post['natu'], $post['estatus'], $user_id)->result();

        $porconta = 0;
        $total = 0;

        foreach ($conteudo as $value) {
            if ($value->FINANC_NATUREZA == 'R') {
                $porconta += $value->FINANC_PORCONTA;
                $total += $value->FINANC_VALOR;
            } else {
                $porconta -= $value->FINANC_PORCONTA;
                $total -= $value->FINANC_VALOR;
            }
        }

        $retorno['conteudo'] = $conteudo;
        $retorno['porconta'] = $porconta;
        $retorno['total'] = $total;

        $this->send_json(isset($retorno) ? $retorno : "");
    }

    // log inicio
    public function acesso() {
        $this->page_construct("relatorio/acesso");
    }

    public function acessojson() {
        $post = $this->input->post();
        $this->load->model('log_model');
        
        $busca = $post['busca'];
        $dti = $this->mastersis->DataToDB($post['dti']);
        $dtf = $this->mastersis->DataToDB($post['dtf']);

        $regpload = $this->getconf("reg_por_load");
        $inicio = (int) $post['ini'];

        $retorno = $this->log_model->get_all($busca, $regpload, $inicio, $dti, $dtf);
        
        $this->send_json(isset($retorno) ? $retorno : "");
    }

}
