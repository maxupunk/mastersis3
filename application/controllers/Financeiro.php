<?php

class Financeiro extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
        $this->load->model('Financeiro_model');
    }

    function index() {
        $this->page_construct("financeiro/index");
    }

    function lista() {
        $mostra = $this->input->get('mostrar');
        $busca = $this->input->get('buscar') ? $this->input->get('buscar') : null;

        $regpload = $this->getconf("reg_por_load");
        $output = $this->Financeiro_model->get_all($busca, $regpload, $mostra)->result();

        $this->send_json($output);
    }

    function add() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('FINANC_VALOR', 'Valor', 'required');
        $this->form_validation->set_rules('FINANC_DESCR', 'Descrição', 'required');
        $this->form_validation->set_rules('PES_ID', 'pessoa', 'required');

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $finan_valor = $this->mastersis->RealToDB($post['FINANC_VALOR']);
            $finan_porconta = $this->mastersis->RealToDB($post['FINANC_PORCONTA']);
            $params = [
                'PES_ID' => $post['PES_ID'],
                'USUARIO_ID' => $this->session->USUARIO_ID,
                'FINANC_DATA' => date('Y-m-d'),
                'FINANC_NATUREZA' => $post['FINANC_NATUREZA'],
                'FINANC_DESCR' => $post['FINANC_DESCR'],
                'FINANC_VALOR' => $finan_valor,
                'FINANC_PORCONTA' => $finan_porconta,
                'FINANC_VECIMENTO' => $this->mastersis->DataToDB($post['FINANC_VECIMENTO']),
                'FINANC_ESTATUS' => 'ab',
            ];

            if ($finan_porconta and $finan_valor > $finan_porconta) {
                $params['FINANC_ESTATUS'] = "pp";
            }

            if ($finan_valor === $finan_porconta) {
                $params['FINANC_ESTATUS'] = "pg";
                $params['FINANC_DATA_PG'] = date("Y-m-d");
            }

            $desrec_id = $this->Financeiro_model->add($params);

            if ($desrec_id) {
                $this->auth->log($this->router->class, $this->router->method, $params);
                $this->send_json(['ok' => 'ok']);
            } else {
                $this->send_json(['msg' => 'Erro: ao addiciona financeiro! (Falha no DB)']);
            }
        } else {

            if ($this->form_validation->error_array()) {
                $this->send_json($this->form_validation->error_array());
                exit;
            }

            $this->load->view('financeiro/add');
        }
    }

    /**
     * @param $DESREC_ID
     */
    function baixar($DESREC_ID) {

        $query = $this->Financeiro_model->get($DESREC_ID);

        if (isset($query->DESREC_ID)) {

            if ($query->FINANC_ESTATUS == "ab" or $query->FINANC_ESTATUS == "pp") {

                $this->load->library('form_validation');

                $this->form_validation->set_rules('FINANC_PORCONTA', 'Pago', "required");

                if ($this->form_validation->run()) {
                    $post = $this->input->post();

                    $porconta = $this->mastersis->RealToDB($post['FINANC_PORCONTA']);
                    $params['FINANC_PORCONTA'] = $query->FINANC_PORCONTA + $porconta;

                    if ($params['FINANC_PORCONTA'] >= $query->FINANC_VALOR) {
                        $params['FINANC_DATA_PG'] = date("Y-m-d");
                        $params['FINANC_ESTATUS'] = "pg";
                    }

                    if ($params['FINANC_PORCONTA'] < $query->FINANC_VALOR) {
                        $params['FINANC_ESTATUS'] = "pp";
                    }

                    $params['USUARIO_ID'] = $this->session->USUARIO_ID;

                    $update = $this->Financeiro_model->update($DESREC_ID, $params);
                    if ($update) {
                        $this->auth->log($this->router->class, $this->router->method, $params);
                        $this->send_json(['ok' => 'ok']);
                    } else {
                        $this->send_json(['msg' => 'Erro: Problema ao atualizar servico dados! (Falha no DB)']);
                    }
                } else {

                    if ($this->form_validation->error_array()) {
                        $this->send_json($this->form_validation->error_array());
                        exit;
                    }

                    $data['financeiro'] = $query;
                    if ($query->PES_ID) {
                        $this->load->model('Pessoa_model');
                        $data['pessoa'] = $this->Pessoa_model->get($query->PES_ID);
                    }

                    $this->load->view('financeiro/baixar', $data);
                }
            } else {
                $this->send_json(['msg' => 'Já foi pago, devolvido ou canselado']);
            }
        } else {
            $this->send_json(['msg' => 'O financiero não existe!']);
        }
    }

    function edit($DESREC_ID) {

        $query = $this->Financeiro_model->get($DESREC_ID);

        if ($query->DESREC_ID) {

            if (($query->FINANC_ESTATUS == "ab" || $query->FINANC_ESTATUS == "pp") || ($query->ORDEM_ID === NULL)) {

                $this->load->library('form_validation');

                $this->form_validation->set_rules('FINANC_VECIMENTO', 'vencimento', "required");
                $this->form_validation->set_rules('FINANC_VALOR', 'valor', "required");

                if ($this->form_validation->run()) {
                    $post = $this->input->post();

                    $params['FINANC_NATUREZA'] = $post['FINANC_NATUREZA'];
                    $params['FINANC_VECIMENTO'] = $this->mastersis->DataToDB($post['FINANC_VECIMENTO']);
                    $params['FINANC_VALOR'] = $post['FINANC_VALOR'];
                    $params['USUARIO_ID'] = $this->session->USUARIO_ID;

                    $update = $this->Financeiro_model->update($DESREC_ID, $params);
                    if ($update) {
                        $this->send_json(['ok' => 'ok']);
                    } else {
                        $this->send_json(['msg' => 'Erro: Problema ao atualizar servico dados! (Falha no DB)']);
                    }
                } else {

                    if ($this->form_validation->error_array()) {
                        $this->send_json($this->form_validation->error_array());
                        exit;
                    }

                    $data['financeiro'] = $query;
                    if ($query->PES_ID) {
                        $this->load->model('Pessoa_model');
                        $data['pessoa'] = $this->Pessoa_model->get($query->PES_ID);
                    }

                    $this->load->view('financeiro/edit', $data);
                }
            } else {
                $this->send_json(['msg' => 'Esse financeiro não pode ser alterado!']);
            }
        } else {
            $this->send_json(['msg' => 'O financiero não existe!']);
        }
    }

    function remove($DESREC_ID) {

        $query = $this->Financeiro_model->get($DESREC_ID);

        if (isset($query->DESREC_ID)) {

            if (!$query->ORDEM_ID) {
                if ($query->ORDEM_ID) {
                    $this->send_json(['msg' => 'O financiero não pode ser removido pois pertence a uma ordem de serviço ou ordem!']);
                } else {
                    $query = $this->Financeiro_model->del($DESREC_ID);
                    if ($query) {
                        $this->auth->log($this->router->class, $this->router->method, $query);
                        $this->send_json(['ok' => 'ok']);
                    } else {
                        $this->send_json(['msg' => 'Erro: Problema ao deletar servico dados! (Falha no DB)']);
                    }
                }
            } else {
                $this->send_json(['msg' => 'Não pode ser apagado pois pertence a uma ordem faturada']);
            }
        } else {
            $this->send_json(['msg' => 'O financiero não existe!']);
        }
    }

    // função para atualizar o preço de venda
    function setpreco() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('PRO_ID', 'id', "required");
        if ($this->form_validation->run()) {
            $post = $this->input->post();
            if (isset($post['ESTOQ_VENDA']) or isset($post['ESTOQ_CUSTO'])) {
                if (isset($post['ESTOQ_VENDA'])) {
                    $valor['ESTOQ_VENDA'] = $this->mastersis->RealToDB($post['ESTOQ_VENDA']);
                } else {
                    $valor['ESTOQ_CUSTO'] = $this->mastersis->RealToDB($post['ESTOQ_CUSTO']);
                }
                $this->load->model('Estoque_model');
                $query = $this->Estoque_model->set($post['PRO_ID'], $valor);
                if (!$query) {
                    $this->send_json(['msg' => 'Erro ao seta o preço']);
                } else {
                    $this->send_json(['ok' => 'ok']);
                }
            }
        }
    }

}
