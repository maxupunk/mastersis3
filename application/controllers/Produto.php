<?php

class Produto extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
        $this->load->model(array('Produto_model', 'Estoque_model'));
    }

    function index() {
        $this->page_construct("produto/index");
    }

    function lista() {
        $mostra = $this->input->get('mostrar');
        $busca = $this->input->get('buscar') ? $this->input->get('buscar') : null;

        $regpload = $this->getconf("reg_por_load");
        $output = $this->Produto_model->get_all($busca, $regpload, $mostra)->result();

        $this->send_json($output);
    }

    function getproduto($stq = null) {
        $busca = $this->input->get('term');
        $output = $this->Produto_model->get_produto($busca, 10, $stq)->result();
        $this->send_json($output);
    }

    function getservico() {
        $busca = $this->input->get('term');
        $output = $this->Produto_model->get_servico($busca, 10)->result();
        $this->send_json($output);
    }

    function add() {

        $this->load->library('form_validation');

        $this->form_validation->set_rules('PRO_DESCRICAO', 'DESCRICAO', 'required|is_unique[PRODUTOS.PRO_DESCRICAO]');
        $this->form_validation->set_rules('PRO_CODBARRA', 'CODIGO DE BARRA', 'required|is_unique[PRODUTOS.PRO_CODBARRA]');

        if ($this->input->post('PRO_TIPO') === "p") {
            $this->form_validation->set_rules('ESTOQ_ATUAL', 'ESTOQUE ATUAL', 'required');
        }

        $this->form_validation->set_rules('ESTOQ_VENDA', 'VALOR DE VENDA', 'required');

        if ($this->form_validation->run()) {

            $post = $this->input->post();
            $params = array(
                'PRO_CODBARRA' => $post['PRO_CODBARRA'],
                'PRO_CODBARRA' => $post['PRO_CODBARRA'],
                'PRO_DESCRICAO' => $post['PRO_DESCRICAO'],
                'PRO_CARAC_TEC' => $post['PRO_CARAC_TEC'],
                'CATE_ID' => $post['CATE_ID'],
                'MEDI_ID' => $post['MEDI_ID'],
                'PRO_PESO' => $post['PRO_PESO'],
                'PRO_TIPO' => $post['PRO_TIPO'],
                'PRO_ESTATUS' => $post['PRO_ESTATUS'],
            );

            $this->db->trans_start();

            $produto_id = $this->Produto_model->add($params);

            if ($post['PRO_TIPO'] === "p") {
                $params_estoq = [
                    'PRO_ID' => $produto_id,
                    'ESTOQ_ATUAL' => $post['ESTOQ_ATUAL'],
                    'ESTOQ_MIN' => $post['ESTOQ_MIN'],
                    'ESTOQ_CUSTO' => $this->mastersis->RealToDB($post['ESTOQ_CUSTO']),
                    'ESTOQ_VENDA' => $this->mastersis->RealToDB($post['ESTOQ_VENDA'])
                ];
            } else {
                $params_estoq = [
                    'ESTOQ_VENDA' => $post['ESTOQ_VENDA'],
                    'PRO_ID' => $produto_id
                ];
            }

            $this->Estoque_model->add($params_estoq);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $this->send_json(['msg' => 'Erro: ao cadastrar o produto! (Falha no DB)']);
            } else {
                $this->db->trans_commit();
                $this->send_json(['ok' => 'ok']);
            }
        } else {

            if ($this->form_validation->error_array()) {
                $this->send_json($this->form_validation->error_array());
                exit;
            }

            $this->load->model('Estoque_model');
            $data['estoques'] = $this->Estoque_model->get_all();

            $this->load->model('Categoria_model');
            $data['all_categorias'] = $this->Categoria_model->get_all(null, null, null, TRUE);

            $this->load->model('Medida_model');
            $data['all_medidas'] = $this->Medida_model->get_all();

            $this->load->view('produto/form', $data);
        }
    }

    function edit($PRO_ID) {
        $produto = $this->Produto_model->get($PRO_ID);

        if (isset($produto->PRO_ID)) {

            $this->load->library('form_validation');

            $this->form_validation->set_rules('PRO_DESCRICAO', 'DESCRICAO', "required|edit_unique[PRODUTOS.PRO_DESCRICAO.PRO_ID.$PRO_ID]");
            $this->form_validation->set_rules('PRO_CODBARRA', 'CODIGO DE BARRA', "required|edit_unique[PRODUTOS.PRO_CODBARRA.PRO_ID.$PRO_ID]");

            if ($this->form_validation->run()) {
                $post = $this->input->post();

                $params = array(
                    'PRO_CODBARRA' => $post['PRO_CODBARRA'],
                    'PRO_DESCRICAO' => $post['PRO_DESCRICAO'],
                    'PRO_CARAC_TEC' => $post['PRO_CARAC_TEC'],
                    'CATE_ID' => $post['CATE_ID'],
                    'MEDI_ID' => $post['MEDI_ID'],
                    'PRO_PESO' => $post['PRO_PESO'],
                    'PRO_TIPO' => $post['PRO_TIPO'],
                    'PRO_ESTATUS' => $post['PRO_ESTATUS']
                );

                if ($post['PRO_TIPO'] == "p") {
                    $estoque = ['ESTOQ_MIN' => $post['ESTOQ_MIN']];
                } else {
                    $estoque = ['ESTOQ_MIN' => null];
                }

                if ($this->Produto_model->update($PRO_ID, $params, $estoque)) {
                    $this->send_json(['ok' => 'ok']);
                } else {
                    $this->send_json(['msg' => 'Erro ao alterar o cadastro']);
                }
            } else {

                if ($this->form_validation->error_array()) {
                    $this->send_json($this->form_validation->error_array());
                    exit;
                }

                $data['produto'] = $produto;

                $this->load->model('Categoria_model');
                $data['all_categorias'] = $this->Categoria_model->get_all(null, null, null, TRUE);

                $this->load->model('Medida_model');
                $data['all_medidas'] = $this->Medida_model->get_all();

                $this->load->view('produto/form', $data);
            }
        } else {
            $this->send_json(['msg' => 'O produto que você tentou editar não existe!']);
        }
    }

    function remove($PRO_ID) {
        $produto = $this->Produto_model->get($PRO_ID);

        if (isset($produto->PRO_ID)) {

            if ($this->Produto_model->delete($PRO_ID)) {
                $this->send_json(['ok' => 'ok']);
            } else {
                $this->send_json(['msg' => 'Erro: O Itens não pode ser deletado!']);
            }
        } else {
            $this->send_json(['msg' => 'Erro: O produto que você tentou deletar já foi deletado ou não existe.']);
        }
    }

    function detalhe($PRO_ID) {

        $produto = $this->Produto_model->get($PRO_ID);

        if ($produto->PRO_ID) {

            $data['produto'] = $produto;

            $this->load->view('produto/detalhe', $data);
        } else {

            echo "Erro: O produto que você tentou ver detalhes foi deletado ou não existe.";
        }
    }

    function QRcode() {
        $texto = $this->input->get('t');
        if ($texto) {
            include(APPPATH . "/third_party/phpqrcode/qrlib.php");
            QRcode::png($texto);
        }
    }

    function CodBarras($cod) {
        $this->load->library('codbarras');

        if ($cod) {
            $this->codbarras->text = $cod;
            if ($this->input->get('t')) {
                $this->codbarras->SizeFactor = $this->input->get('t');
            }
            if ($this->input->get('s')) {
                $this->codbarras->size = $this->input->get('s');
            }
            $this->codbarras->mostrar();
        }
    }

}
