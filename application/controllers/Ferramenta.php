<?php

class Ferramenta extends MY_Controller {

    var $pasta_bkp = "backup";
    var $arq_permi = "permisao";

    function __construct() {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
    }

    public function resetpermisoes() {

        $this->load->library('form_validation');

        $this->form_validation->set_rules('aceitatermo', 'aceita servico termos', 'required|numeric');
        if ($this->form_validation->run()) {
            if (file_exists($this->arq_permi)) {
                ///
                echo "- Apagando as permisões. <br>";
                $this->db->query('SET FOREIGN_KEY_CHECKS = 0');
                $this->db->query('TRUNCATE PERMISSOES');
                $this->db->query('SET FOREIGN_KEY_CHECKS = 1');
                ///
                echo "- Apagando todos servico metodos. <br>";
                $this->db->query('SET FOREIGN_KEY_CHECKS = 0');
                $this->db->query('TRUNCATE METODOS');
                $this->db->query('SET FOREIGN_KEY_CHECKS = 1');
                ///
                $this->db->query('ALTER TABLE METODOS AUTO_INCREMENT = 1');
                ///
                echo "- Cerregando servico controles. <br>";
                $this->load->library('controllerlist');
                ///
                echo "- Cerregando as permissões de [" . $this->session->USUARIO_APELIDO . "]. <br>";
                $this->load->model('Usuario_model');
                $metodos = $this->Usuario_model->metodos();
                foreach ($metodos as $metodo) {
                    $params = [
                        'USUARIO_ID' => $this->session->USUARIO_ID,
                        'METOD_ID' => $metodo->METOD_ID
                    ];
                    $this->Usuario_model->setpermissao($params);
                }
                //
                echo "- Cerregando servico controles publico. <br>";
                $motod_public[] = ['METOD_CLASS' => 'usuario', 'METOD_METODO' => 'login'];
                $motod_public[] = ['METOD_CLASS' => 'usuario', 'METOD_METODO' => 'dologin'];
                $motod_public[] = ['METOD_CLASS' => 'usuario', 'METOD_METODO' => 'logout'];
                $motod_public[] = ['METOD_CLASS' => 'Dashboard', 'METOD_METODO' => 'manutencao'];
                $paramet_public = ['METOD_PRIVADO' => 0];

                foreach ($motod_public as $metodo) {
                    $this->db->where($metodo);
                    $this->db->update('METODOS', $paramet_public);
                }

                echo "- Cerregando servico controles publico para logados. <br>";
                $metod_logado[] = ['METOD_CLASS' => 'Dashboard', 'METOD_METODO' => 'index'];
                $paramt_priv = ['METOD_PRIVADO' => 2];

                foreach ($metod_logado as $metodo) {
                    $this->db->where($metodo);
                    $this->db->update('METODOS', $paramt_priv);
                }

                echo "reset concluido<br>";

                unlink($this->arq_permi);
            } else {
                echo "Não esta liberado o carregamento!";
            }
        } else {

            if ($this->form_validation->error_array()) {
                echo $this->form_validation->error_array()['aceitatermo'];
                exit;
            }

            $this->page_construct('ferramenta/resetperm');
        }
    }

    public function logsistema() {
        $this->load->helper('file');

        $data['local'] = $this->config->item('log_path') == null ? 'application/logs/' : $this->config->item('log_path');
        $data['lista'] = get_filenames($data['local']);

        $this->page_construct("ferramenta/logsistema", $data);
    }

    public function backup() {
        $this->page_construct("ferramenta/backup");
    }

    public function backupjson() {
        $local = $this->pasta_bkp;

        if (!file_exists($local)) {
            mkdir($local, 0770, true);
        }

        $arquivos = scandir($local);

        $retorno = array();
        foreach ($arquivos as $arquivo) {
            $ext = substr($arquivo, strrpos($arquivo, '.'));
            if ($ext === ".sql") {
                $url = base_url('ferramenta/backupdawnload/' . $arquivo);
                $data = date('d m Y H:i:s', filemtime($local . DIRECTORY_SEPARATOR . $arquivo));
                $retorno[] = [
                    'URL' => $url,
                    'ARQUIVO' => $arquivo,
                    'DATA' => $data
                ];
            }
        }

        $this->send_json($retorno);
    }

    public function backupcriar($nome = null) {
        
        set_time_limit(0);

        $this->load->dbutil();

        $basedados = $this->dbutil->list_databases();
        foreach ($basedados as $tabela) {
            $this->dbutil->repair_table($tabela);
        }

        $this->dbutil->optimize_database();

        $prefs = [
            'format' => 'txt',
            'foreign_key_checks' => false
        ];
        $conteudo = $this->dbutil->backup($prefs);
        // guarda no pasta backup
        if (!$nome) {
            $nomefile = date("Y-m-d-h_i_s") . '.sql';
        } else {
            $nomefile = $nome . ".sql";
        }

        $this->load->library('encryption');
        $backup = $this->encryption->encrypt($conteudo);

        $this->load->helper('file');
        write_file($this->pasta_bkp . DIRECTORY_SEPARATOR . $nomefile, $backup);

        return ($nome) ? true : $this->backupjson();
    }

    public function backuprestore() {

        header('X-Accel-Buffering: no'); // nginx related - turn the output buffer off
        header('Connection', 'keep-alive');

        $file_name = $this->input->get('arquivo');
        $pontrestoracao = $this->backupcriar("PontoDeRestauracao");

        if ($file_name and $pontrestoracao) {

            ob_start();
            set_time_limit(0);

            $file_conteudo = file_get_contents($this->pasta_bkp . DIRECTORY_SEPARATOR . $file_name);

            $this->load->library('encryption');
            $file_decript = $this->encryption->decrypt($file_conteudo);


            $this->load->helper('file');
            write_file($this->pasta_bkp . DIRECTORY_SEPARATOR . 'descript.sql', $file_decript);

            $file_array = explode(';', $file_decript, -1);
            $query_total = count($file_array);

            header("Content-length: $query_total");

            $this->db->trans_start();

            foreach ($file_array as $query) {
                $this->db->query($query);
                ob_flush();
                flush();
                echo 0;
            }

            ob_end_flush();

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                echo "Erro, problema com a restouração dos dados!";
            } else {
                $this->db->trans_commit();
            }
        }
    }

    public function backupdel() {
        $file_name = $this->input->post('arquivo');
        if ($file_name) {
            $arquivo = $this->pasta_bkp . DIRECTORY_SEPARATOR . $file_name;
            if (file_exists($arquivo)) {
                if (!unlink($arquivo)) {
                    $this->send_json(['msg' => 'Erro: Problema ao apagar arquivo!']);
                }
            } else {
                $this->send_json(['msg' => 'Não esta liberado o carregamento!']);
            }
        }
        $this->backupjson();
    }

    public function backupdawnload($arquivo = null) {
        if ($arquivo) {
            $url = $this->pasta_bkp . DIRECTORY_SEPARATOR . $arquivo;
            $this->load->helper('download');
            $conteudo = file_get_contents($url);
            force_download($arquivo, $conteudo);
        }
    }

    function make_base(){

        $this->load->library('VpxMigration');

        // All Tables:

        $this->vpxmigration->generate();

        //Single Table:

        //$this->vpxmigration->generate('table');

    }

}
