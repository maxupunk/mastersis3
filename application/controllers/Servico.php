<?php

class Servico extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
        $this->load->model(array('Servico_model','Ordens_model'));
    }

    function index() {
        $this->page_construct("servico/index");
    }

    function add() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('ORDEM_EQUIPAMENT', 'Equipamento', 'required');
        $this->form_validation->set_rules('ORDEM_DSC_DEFEITO', 'Defeito relatado', 'required');
        $this->form_validation->set_rules('PES_ID', 'Pessoa', 'required');

        if ($this->form_validation->run()) {
            $params = [
                'PES_ID' => $this->input->post('PES_ID'),
                'USUARIO_ID' => $this->session->userdata('USUARIO_ID'),
                'ORDEM_EQUIPAMENT' => $this->input->post('ORDEM_EQUIPAMENT'),
                'ORDEM_DSC_DEFEITO' => $this->input->post('ORDEM_DSC_DEFEITO'),
                'ORDEM_DATA' => date("Y-m-d h:i:s"),
                'ORDEM_TIPO' => 's',
                'ORDEM_ESTATUS' => 'EA'
            ];

            $os_id = $this->Ordens_model->add($params);
            if ($os_id) {
                $this->send_json(['ok' => 'ok']);
            } else {
                $this->send_json(['msg' => 'Erro ao cadastrar a ordem de serviço!']);
            }
        } else {

            if ($this->form_validation->error_array()) {
                $this->send_json($this->form_validation->error_array());
                exit;
            }

            $this->load->view('servico/add');
        }
    }

    function edit($ORDEM_ID) {

        $result = $this->Ordens_model->get($ORDEM_ID);

        if (isset($result->ORDEM_ID)) {

            $this->load->library('form_validation');

            $this->form_validation->set_rules('ORDEM_ESTATUS', '', 'required');

            $post = $this->input->post();

            if ($this->form_validation->run()) {
                $params = [
                    'ORDEM_DSC_SOLUC' => $post['ORDEM_DSC_SOLUC'],
                    'ORDEM_DSC_PENDENT' => $post['ORDEM_DSC_PENDENT'],
                    'ORDEM_OBS' => $post['ORDEM_OBS'],
                    'ORDEM_ESTATUS' => $post['ORDEM_ESTATUS'],
                ];

                $query = $this->Ordens_model->update($result->ORDEM_ID, $params);

                if ($query) {
                    $this->send_json(['ok' => 'ok']);
                } else {
                    $this->send_json(['msg' => 'Erro ao alterar o cadastro']);
                }
            } else {

                if ($this->form_validation->error_array()) {
                    $this->send_json($this->form_validation->error_array());
                    exit;
                }

                $data['os'] = $result;

                $this->load->view('servico/edit', $data);
            }
        } else {
            $this->send_json(['msg' => 'A ordem de serviço que você tentou editar não existe!']);
        }
    }

    function excluir($ORDEM_ID) {

        $this->load->model('Servico_model');
        $query_ordem = $this->Ordens_model->get($ORDEM_ID);

        if ($query_ordem and $query_ordem->ORDEM_ESTATUS != 'RE') {

            $this->db->trans_start();

            $this->Ordens_model->delete($ORDEM_ID);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $this->send_json(['msg' => 'Erro: problema com o fechamento da ordem ser serviço! (Falha no DB)']);
            } else {
                $this->db->trans_commit();
                $this->send_json(['ok' => 'ok']);
            }
        } else {
            $this->send_json(['msg' => 'Erro: A ordem se serviço não existe ou foi entregue!']);
        }
    }

    function faturar($ORDEM_ID) {

        $ordem = $this->Ordens_model->get($ORDEM_ID);

        if ($ordem->ORDEM_ESTATUS !== 'RE') {

            $this->load->library('form_validation');

            $post = $this->input->post();

            $this->form_validation->set_rules('FPG_ID', 'Forma de pagamento', 'required');

            if ($this->form_validation->run()) {
                
                if ($ordem and $ordem->ORDEM_ESTATUS != 4) {

                    $params = [
                        'ORDEM_DATA' => date("Y-m-d H:i:s"),
                        'ORDEM_OBS' => $post['ORDEM_OBSERVACAO'],
                        'ORDEM_DESCO' => $this->mastersis->RealToDB($post['ORDEM_DESCO']),
                        'ORDEM_FRETE' => $this->mastersis->RealToDB($post['ORDEM_FRETE']),
                        'ORDEM_IMPOSTO' => $this->mastersis->RealToDB($post['ORDEM_IMPOSTO']),
                        'FPG_ID' => $post['FPG_ID'],
                        'ORDEM_NPARC' => $post['NPARCELA'],
                    ];

                    $total_ordem = $this->Ordens_model->total($ordem->ORDEM_ID);

                    $param_finan = [
                        'ORDEM_ID' => $ordem->ORDEM_ID,
                        'USUARIO_ID' => $this->session->USUARIO_ID,
                        'PES_ID' => $ordem->PES_ID,
                        'FINANC_NATUREZA' => 'R',
                        'FINANC_VALOR' => $total_ordem,
                        'FINANC_PORCONTA' => isset($post['FINANC_PORCONTA']) ? $post['FINANC_PORCONTA'] : 0,
                        'FINANC_VECIMENTO' => $this->mastersis->DataToDB($post['FINANC_VECIMENTO']),
                        'FINANC_DATA' => date('Y-m-d'),
                        'FINANC_ESTATUS' => 'ab'
                    ];

                    $finan_porconta = $this->mastersis->RealToDB($post['FINANC_PORCONTA']);
                    
                    if ($finan_porconta and $total_ordem > $finan_porconta) {
                        $param_finan['FINANC_ESTATUS'] = "pp";
                    }

                    if ($total_ordem <= $finan_porconta) {
                        $param_finan['FINANC_ESTATUS'] = "pg";
                        $param_finan['FINANC_DATA_PG'] = date("Y-m-d");
                    }

                    $this->db->trans_start();

                    if ($this->Ordens_model->update($ordem->ORDEM_ID, $params)) {
                        $this->load->model('Financeiro_model');
                        $this->Financeiro_model->add($param_finan);
                        $this->Ordens_model->fechar($ordem->ORDEM_ID);
                    }

                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        $this->send_json(['msg' => 'Erro: problema com o fechamento da ordem ser serviço! (Falha no DB)']);
                    } else {
                        $this->db->trans_commit();
                        $this->send_json(['ok' => 'ok']);
                    }
                } else {
                    $this->send_json(['msg' => 'Erro: A ordem se serviço não existe ou foi fechada!']);
                }
            } else {

                if ($this->Servico_model->get_lst($ORDEM_ID)) {
                    if ($this->form_validation->error_array()) {
                        $this->send_json($this->form_validation->error_array());
                        exit;
                    }

                    $data['Ordem'] = $ordem;

                    $this->load->model('Forma_pg_model');
                    $data['form_pgs'] = $this->Forma_pg_model->get_all(null, null, null, TRUE);
                    $data['total'] = $this->Ordens_model->total($ORDEM_ID);

                    $data['form_pg_pcs'] = $this->Forma_pg_model->get($ordem->FPG_ID);

                    $this->load->model('Financeiro_model');
                    $data['financeiro'] = $this->Financeiro_model->getByOrdemID($ORDEM_ID)->row();

                    $this->load->view("ordem/fatura", $data);
                } else {
                    $this->send_json(['msg' => 'Não existe itens na ordem de serviço!']);
                }
            }
        } else {
            $this->send_json(['msg' => 'A ordem de serviço já foi entregue!']);
        }
    }

    function reabrir($ORDEM_ID) {

        $ordem = $this->Ordens_model->get($ORDEM_ID);

        if ($ordem->ORDEM_ESTATUS === 'RE' or $ordem->ORDEM_ESTATUS === 'CC' ) {

            $this->db->trans_start();

            $this->load->model('Financeiro_model');
            $this->Financeiro_model->delByOrdemID($ORDEM_ID);
            $this->Servico_model->abrir($ORDEM_ID);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $this->send_json(['msg' => 'Erro: problema com o fechamento da ordem ser serviço! (Falha no DB)']);
            } else {
                $this->db->trans_commit();
                $this->send_json(['ok' => 'ok']);
            }
        } else {
            $this->send_json(['msg' => ' A ordem se serviço não esta faturada!']);
        }
    }

    ///////////////////////////////////////////////////////////////
    ///////   Funçoes para manipular itens do OS //////////
    ///////////////////////////////////////////////////////////////
    
    function lstitem($ORDEM_ID) {
        if ($ORDEM_ID) {
            $json_out['total'] = $this->Ordens_model->total($ORDEM_ID);
            $json_out['produtos'] = $this->Servico_model->get_lst($ORDEM_ID);
            $this->send_json($json_out);
        }
    }
    
    function additem() {
        $post = $this->input->post();
        $ORDEM_ID = $post['ORDEM_ID'];
        $PRO_ID = $post['PRO_ID'];

        $item = $this->Servico_model->get_item($ORDEM_ID, $PRO_ID);
        // verifica se o item existe
        if (!$item) {

            $this->load->model('Produto_model');
            $item = $this->Produto_model->get($PRO_ID);

            $params = [
                'ORDEM_ID' => $ORDEM_ID,
                'PRO_ID' => $item->PRO_ID,
                'LIST_PED_QNT' => 1,
                'LIST_PED_VALOR' => $item->ESTOQ_VENDA,
            ];

            $list_ordem = $this->Servico_model->add_lst($params);
            if ($list_ordem) {
                $json_out['total'] = $this->Ordens_model->total($ORDEM_ID);
                $json_out['produtos'] = $this->Servico_model->get_lst($ORDEM_ID);
                $this->send_json($json_out);
            } else {
                $this->send_json(['msg' => 'Erro ao adicionar item!']);
            }
        }
    }

    function delitem() {
        $post = $this->input->post();
        $ORDEM_ID = $post['ORDEM_ID'];
        $PRO_ID = $post['PRO_ID'];
        
        $list_ordem = $this->Servico_model->del_lst($ORDEM_ID, $PRO_ID);
        if ($list_ordem) {
            $json_out['total'] = $this->Ordens_model->total($ORDEM_ID);
            $json_out['produtos'] = $this->Servico_model->get_lst($ORDEM_ID);
            $this->send_json($json_out);
        } else {
            $this->send_json(['msg' => 'Erro ao deletar o item!']);
        }
    }

    function upditem() {
        $post = $this->input->post();
        $ORDEM_ID = $post['ORDEM_ID'];
        $PRO_ID = $post['PRO_ID'];
        $QNT = $post['QNT'];
        
        if ($QNT >= 1) {
            $list_ordem = $this->Servico_model->upd_lst($ORDEM_ID, $PRO_ID, $QNT);
            if ($list_ordem) {
                $json_out['total'] = $this->Ordens_model->total($ORDEM_ID);
                $json_out['produtos'] = $this->Servico_model->get_lst($ORDEM_ID);
                $this->send_json($json_out);
            } else {
                $this->send_json(['msg' => 'Erro ao adicionar item!']);
            }
        } else {
            $this->delitem($ORDEM_ID, $PRO_ID);
        }
    }
    
}
