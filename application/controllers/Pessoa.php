<?php

class Pessoa extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
        $this->load->model('Pessoa_model');
    }

    function index() {
        $this->page_construct("pessoa/index");
    }

    function lista() {
        $mostra = $this->input->get('mostrar');
        $busca = $this->input->get('buscar') ? $this->input->get('buscar') : null;

        $regpload = $this->getconf("reg_por_load");
        $output = $this->Pessoa_model->get_all($busca, $regpload, $mostra)->result();

        $this->send_json($output);
    }

    function busca() {
        $busca = $this->input->get('term');
        $retorno = $this->Pessoa_model->get_all($busca, 5)->result();
        $this->send_json($retorno);
    }

    function add() {

        $this->load->library('form_validation');

        $this->form_validation->set_rules('PES_NOME', 'NOME', 'required');
        $this->form_validation->set_rules('PES_CPF_CNPJ', 'CPF/CNPJ', 'required|is_unique[PESSOAS.PES_CPF_CNPJ]');
        $this->form_validation->set_rules('PES_DATA_NASC', 'DATA NASCIMENTO', 'required');
        $this->form_validation->set_rules('PES_CEL1', 'CELULAR', 'required');
        $this->form_validation->set_rules('PES_END_CEP', 'CEP', 'required');
        $this->form_validation->set_rules('RUA_ID', 'RUA', 'required');

        if ($this->form_validation->run()) {

            $post = $this->input->post();
            $params = array(
                'PES_NOME' => $post['PES_NOME'],
                'PES_CPF_CNPJ' => $post['PES_CPF_CNPJ'],
                'PES_NOME_PAI' => $post['PES_NOME_PAI'],
                'PES_NOME_MAE' => $post['PES_NOME_MAE'],
                'PES_DATA_NASC' => $this->mastersis->DataToDB($post['PES_DATA_NASC']),
                'PES_FONE' => $post['PES_FONE'],
                'PES_CEL1' => $post['PES_CEL1'],
                'PES_CEL2' => $post['PES_CEL2'],
                'PES_END_NUMERO' => $post['PES_END_NUMERO'],
                'PES_END_CEP' => $post['PES_END_CEP'],
                'PES_END_REFERENCIA' => $post['PES_END_REFERENCIA'],
                'RUA_ID' => $post['RUA_ID'],
                'PES_TIPO' => $post['PES_TIPO'],
                'PES_DATA' => date('Y-m-d H:i:s'),
                'PES_EMAIL' => $post['PES_EMAIL'],
                'PES_ESTATUS' => 'a',
                'PES_ESTOQUE' => isset($post['PES_ESTOQUE'])
            );

            $this->Pessoa_model->add($params);

            if ($this->db->trans_status() === TRUE) {
                $this->send_json(['ok' => 'ok']);
            } else {
                $this->send_json(['msg' => 'Erro ao cadastrar a pessoa!']);
            }
        } else {

            if ($this->form_validation->error_array()) {
                $this->send_json($this->form_validation->error_array());
                exit;
            }

            $this->load->model('Endereco_model');
            $data['all_ruas'] = $this->Endereco_model->get_all();

            $this->load->view('pessoa/form', $data);
        }
    }

    function edit($PES_ID) {
        $pessoa = $this->Pessoa_model->get($PES_ID);

        if (isset($pessoa->PES_ID)) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('PES_NOME', 'NOME', "required|edit_unique[PESSOAS.PES_NOME.PES_ID.$pessoa->PES_ID]");
            $this->form_validation->set_rules('PES_CPF_CNPJ', 'CPF/CNPJ', "required|edit_unique[PESSOAS.PES_CPF_CNPJ.PES_ID.$pessoa->PES_ID]");
            $this->form_validation->set_rules('PES_DATA_NASC', 'DATA DE NASCIMENTO', 'required');
            $this->form_validation->set_rules('PES_CEL1', 'CELULAR', 'required');
            $this->form_validation->set_rules('PES_END_CEP', 'CEP', 'required');
            $this->form_validation->set_rules('RUA_ID', 'RUA', 'required');

            if ($this->form_validation->run()) {
                
                $post = $this->input->post();
                
                $params = array(
                    'PES_NOME' => $post['PES_NOME'],
                    'PES_CPF_CNPJ' =>  $post['PES_CPF_CNPJ'],
                    'PES_NOME_PAI' =>  $post['PES_NOME_PAI'],
                    'PES_NOME_MAE' =>  $post['PES_NOME_MAE'],
                    'PES_DATA_NASC' => $this->mastersis->DataToDB( $post['PES_DATA_NASC']),
                    'PES_FONE' =>  $post['PES_FONE'],
                    'PES_CEL1' =>  $post['PES_CEL1'],
                    'PES_CEL2' =>  $post['PES_CEL2'],
                    'PES_END_NUMERO' =>  $post['PES_END_NUMERO'],
                    'PES_END_CEP' =>  $post['PES_END_CEP'],
                    'PES_END_REFERENCIA' =>  $post['PES_END_REFERENCIA'],
                    'RUA_ID' =>  $post['RUA_ID'],
                    'PES_TIPO' =>  $post['PES_TIPO'],
                    'PES_EMAIL' =>  $post['PES_EMAIL'],
                    'PES_ESTATUS' =>  $post['PES_ESTATUS'],
                );

                $pessoa = $this->Pessoa_model->update($PES_ID, $params);
                if ($pessoa) {
                    $this->send_json(['ok' => 'ok']);
                } else {
                    $this->send_json(['msg' => 'Erro ao alterar o cadastro']);
                }
            } else {

                if ($this->form_validation->error_array()) {
                    $this->send_json($this->form_validation->error_array());
                    exit;
                }

                $data['pessoa'] = $pessoa;

                $this->load->model('Endereco_model');
                $data['rua'] = $this->Endereco_model->get_rua($data['pessoa']->RUA_ID);

                $this->load->view('pessoa/form', $data);
            }
        } else {
            $this->send_json(['msg' => 'A pessoa que você tentou editar não existe no DB!']);
        }
    }

    function remove($PES_ID) {
        $pessoa = $this->Pessoa_model->get($PES_ID);

        if (isset($pessoa->PES_ID)) {
            if ($this->Pessoa_model->delete($PES_ID)) {
                $this->send_json(['ok' => 'ok']);
            } else {
                $this->send_json(['msg' => 'Erro: O Itens não pode ser deletado!']);
            }
        } else {
            $this->send_json(['msg' => 'Erro: A pessoa que você tentou deletar já foi deletado ou não existe.']);
        }
    }

}
