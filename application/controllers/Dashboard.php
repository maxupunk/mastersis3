<?php

class Dashboard extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
    }

    function index() {
        $this->page_construct('dashboard/index');
    }

    function manutencao() {
        $this->output->set_status_header('503');
        $this->load->view('dashboard/manutencao');
    }

}
