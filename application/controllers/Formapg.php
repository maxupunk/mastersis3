<?php

class Formapg extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
        $this->load->model('Forma_pg_model');
    }

    function index() {
        $this->page_construct("formapg/index");
    }

    function lista() {
        $mostra = $this->input->get('mostrar');
        $busca = $this->input->get('buscar') ? $this->input->get('buscar') : null;

        $regpload = $this->getconf("reg_por_load");
        $output = $this->Forma_pg_model->get_all($busca, $regpload, $mostra);

        $this->send_json($output);
    }

    function add() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('FPG_DESCR', 'FPG DESCR', 'required|is_unique[FORMA_PG.FPG_DESCR]');
        $this->form_validation->set_rules('FPG_PARCE', 'FPG PARCE', 'integer|greater_than_equal_to[1]');
        $this->form_validation->set_rules('FPG_AJUSTE', 'FPG AJUSTE', 'numeric');

        if ($this->form_validation->run()) {
            $params = array(
                'FPG_DESCR' => $this->input->post('FPG_DESCR'),
                'FPG_PARCE' => $this->input->post('FPG_PARCE'),
                'FPG_CUSTO' => $this->input->post('FPG_CUSTO'),
                'FPG_AJUSTE' => $this->input->post('FPG_AJUSTE'),
                'FPG_ESTATUS' => $this->input->post('FPG_ESTATUS'),
            );

            $forma_pg_id = $this->Forma_pg_model->add($params);
            if ($forma_pg_id) {
                $this->send_json(['ok' => 'ok']);
            } else {
                $this->send_json(['msg' => 'Erro ao cadastrar a pessoa!']);
            }
        } else {

            if ($this->form_validation->error_array()) {
                $this->send_json($this->form_validation->error_array());
                exit;
            }

            $this->load->view('formapg/form');
        }
    }

    function edit($FPG_ID) {

        $forma_pg = $this->Forma_pg_model->get($FPG_ID);

        if (isset($forma_pg->FPG_ID)) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('FPG_DESCR', 'DESCRIÇÃO', "required|edit_unique[FORMA_PG.FPG_DESCR.FPG_ID.$forma_pg->FPG_ID]");
            $this->form_validation->set_rules('FPG_PARCE', 'N DE PARCELA', 'integer|greater_than_equal_to[1]');
            $this->form_validation->set_rules('FPG_AJUSTE', 'AJUSTE', 'numeric');

            if ($this->form_validation->run()) {
                $params = array(
                    'FPG_DESCR' => $this->input->post('FPG_DESCR'),
                    'FPG_PARCE' => $this->input->post('FPG_PARCE'),
                    'FPG_CUSTO' => $this->input->post('FPG_CUSTO'),
                    'FPG_AJUSTE' => $this->input->post('FPG_AJUSTE'),
                    'FPG_ESTATUS' => $this->input->post('FPG_ESTATUS'),
                );

                $forma_pg = $this->Forma_pg_model->update($FPG_ID, $params);

                if ($forma_pg) {
                    $this->send_json(['ok' => 'ok']);
                } else {
                    $this->send_json(['msg' => 'Erro ao alterar o cadastro']);
                }
            } else {

                if ($this->form_validation->error_array()) {
                    $this->send_json($this->form_validation->error_array());
                    exit;
                }

                $data['forma_pg'] = $forma_pg;

                $this->load->view('formapg/form', $data);
            }
        } else {
            $this->send_json(['msg' => 'A forma de pagamento que você tentou editar não existe!']);
        }
    }

    function remove($FPG_ID) {
        $forma_pg = $this->Forma_pg_model->get($FPG_ID);

        if (isset($forma_pg->FPG_ID)) {
            if ($this->Forma_pg_model->delete($FPG_ID)) {
                $this->send_json(['ok' => 'ok']);
            } else {
                $this->send_json(['msg' => 'Erro: O Itens não pode ser deletado!']);
            }
        } else {
            $this->send_json(['msg' => 'Erro: A forma de pagamento que você tentou deletar já foi deletado ou não existe.']);
        }
    }

    function pega($FPG_ID) {
        $forma_pg = $this->Forma_pg_model->get($FPG_ID);

        if (isset($forma_pg->FPG_ID)) {
            $this->send_json($forma_pg);
        } else {
            $this->send_json(['msg' => 'Erro: A forma de pagamento que você selecionou foi deletado ou não existe.']);
        }
    }

}
