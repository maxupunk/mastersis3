<?php

class Avaria extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
        $this->load->model('Avaria_model');
    }

    function index() {
        $this->page_construct("avaria/index");
    }

    function lista() {
        $mostra = $this->input->get('mostrar');
        $busca = $this->input->get('buscar') ? $this->input->get('buscar') : null;

        $regpload = $this->getconf("reg_por_load");
        $output = $this->Avaria_model->get_all($this->session->ESTOQUE_ID, $regpload, $busca, $mostra);

        $this->send_json($output);
    }

    function add() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('PRO_ID', 'PRODUTO', 'required');
        $this->form_validation->set_rules('AVA_QNT', 'QNT', 'required');
        $this->form_validation->set_rules('AVA_MOTIVO', 'MOTIVO', 'required');

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $params = [
                'PES_ID_EST' => $this->session->ESTOQUE_ID,
                'USUARIO_ID' => $this->session->USUARIO_ID,
                'PRO_ID' => $post['PRO_ID'],
                'AVA_QNT' => $post['AVA_QNT'],
                'AVA_MOTIVO' => $post['AVA_MOTIVO'],
                'AVA_DATA' => date("Y-m-d")
            ];

            $this->db->trans_start();
            
            $AVA_ID = $this->Avaria_model->add($params);
            $this->Avaria_model->fechar($AVA_ID, $post['PRO_ID']);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $this->send_json(['msg' => 'Erro: ao avariar o produto! (Falha no DB)']);
            } else {
                $this->db->trans_commit();
                $this->send_json(['ok' => 'ok']);
            }
        } else {

            if ($this->form_validation->error_array()) {
                $this->send_json($this->form_validation->error_array());
                exit;
            }

            $this->load->view('avaria/add');
        }
    }

    function remove($AVA_ID) {
        $avaria = $this->Avaria_model->get($AVA_ID);

        if (isset($avaria->AVA_ID)) {
            $this->db->trans_start();
            
            $this->Avaria_model->restourar($AVA_ID, $avaria->PRO_ID);
            $this->Avaria_model->delete($AVA_ID);
            
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $this->send_json(['msg' => 'Erro: problemo ao fazer a remoção do item! (Falha no DB)']);
            } else {
                $this->db->trans_commit();
                $this->send_json(['ok' => 'ok']);
            }
            
        } else {
            $this->send_json(['msg' => 'Erro: A avaria que você tentou deletar já foi deletado ou não existe.']);
        }
    }

}
