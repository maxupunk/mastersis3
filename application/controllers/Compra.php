<?php

class Compra extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
    }

    function index() {
        $this->page_construct("compra/index");
    }

    function cart($cmd = NULL) {

        if (isset($cmd)) {

            $this->load->library('MastersisCart');
            $this->mastersiscart->id("cmp");

            $post = $this->input->post();

            switch ($cmd) {

                case 'add':
                    // adiciona produtoes
                    $this->load->model('Produto_model');
                    $produto = $this->Produto_model->get($post['id']);

                    if (isset($produto->PRO_ID)) {
                        if ($produto->PRO_TIPO === "p") {
                            $data = array(
                                'PRO_ID' => $produto->PRO_ID,
                                'PRO_CODBARRA' => $produto->PRO_CODBARRA,
                                'PRO_DESCRICAO' => $produto->PRO_DESCRICAO,
                                'LIST_PED_QNT' => 1,
                                'MEDI_SIGLA' => $produto->MEDI_SIGLA,
                                'LIST_PED_VALOR' => $produto->ESTOQ_CUSTO,
                            );

                            $this->mastersiscart->insert($data);

                            $json_out['total'] = $this->mastersiscart->total();
                            $json_out['produtos'] = $this->mastersiscart->contents();
                            $json_out['Ordens'] = $this->mastersiscart->pedido();
                            $this->send_json($json_out);
                        } else {
                            $this->send_json(['msg' => 'Erro: isso não é um produto!']);
                        }
                    } else {
                        $this->send_json(['msg' => 'Erro ao adicionar o produto, considere o seguinte:<br>- Não existe<br>- foi removido']);
                    }
                    // adiciona produtoes
                    break;

                case 'rm':
                    // remove produto
                    $this->mastersiscart->remove($post['id']);
                    // remove produto
                    break;

                case 'qnt':
                    // altera a quantidade 
                    $data['PRO_ID'] = $post['id'];
                    $data['LIST_PED_QNT'] = $post['qnt'];
                    $this->mastersiscart->update($data);
                    // altera a quantidade 
                    break;

                case 'vlr':
                    // altera o valor
                    $data['PRO_ID'] = $post['id'];
                    $data['LIST_PED_VALOR'] = $post['valor'];
                    $this->mastersiscart->update($data);
                    // altera o valor
                    break;

                case 'lmp':
                    // limpa o carrinho
                    $this->mastersiscart->destroy();
                    // limpa o carrinho
                    break;
            }

            $json_out['total'] = $this->mastersiscart->total();
            $json_out['produtos'] = $this->mastersiscart->contents();
            $json_out['Ordens'] = $this->mastersiscart->pedido();
            $this->send_json($json_out);
            
        } else {
            $this->page_construct("compra/cart");
        }
    }

}
