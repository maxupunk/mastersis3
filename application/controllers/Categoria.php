<?php

class Categoria extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
        $this->load->model('Categoria_model');
    }

    function index() {
        $this->page_construct("categoria/index");
    }

    function lista() {

        $mostra = $this->input->get('mostrar');
        $busca = $this->input->get('buscar') ? $this->input->get('buscar') : null;

        //$regpload = $this->getconf("reg_por_load");
        $output = $this->Categoria_model->get_all($busca, null, $mostra);

        $this->send_json($output);
    }

    function add() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('CATE_NOME', 'NOME', 'required|is_unique[CATEGORIAS.CATE_NOME]');

        if ($this->form_validation->run()) {

            $params = array(
                'CATE_NOME' => $this->input->post('CATE_NOME'),
                'CATE_DESCRIC' => $this->input->post('CATE_DESCRIC'),
                'CATE_IMG' => $this->input->post('CATE_IMG'),
                'CATE_ESTATUS' => $this->input->post('CATE_ESTATUS'),
            );

            $categoria_id = $this->Categoria_model->add($params);
            if ($categoria_id) {
                $this->send_json(['ok' => 'ok']);
            } else {
                $this->send_json(['msg' => 'Erro ao cadastrar a categoria! DB.']);
            }
        } else {

            if ($this->form_validation->error_array()) {
                $this->send_json($this->form_validation->error_array());
                exit;
            }

            $this->load->view('categoria/form');
        }
    }

    function edit($CATE_ID) {

        $categoria = $this->Categoria_model->get($CATE_ID);

        if (isset($categoria->CATE_ID)) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('CATE_NOME', 'NOME', "required|edit_unique[CATEGORIAS.CATE_NOME.CATE_ID.$categoria->CATE_ID]");

            if ($this->form_validation->run()) {
                $params = array(
                    'CATE_NOME' => $this->input->post('CATE_NOME'),
                    'CATE_DESCRIC' => $this->input->post('CATE_DESCRIC'),
                    'CATE_IMG' => $this->input->post('CATE_IMG'),
                    'CATE_ESTATUS' => $this->input->post('CATE_ESTATUS'),
                );

                $categoria = $this->Categoria_model->update($CATE_ID, $params);

                if ($categoria) {
                    $this->send_json(['ok' => 'ok']);
                } else {
                    $this->send_json(['msg' => 'Erro no DB!']);
                }
            } else {

                if ($this->form_validation->error_array()) {
                    $this->send_json($this->form_validation->error_array());
                    exit;
                }

                $data['categoria'] = $categoria;

                $this->load->view('categoria/form', $data);
            }
        } else {
            $this->send_json(['msg' => 'Erro: A categoria que você tentou edita não existe ou foi deletada.']);
        }
    }

    function remove($CATE_ID) {

        $categoria = $this->Categoria_model->get($CATE_ID);

        if (isset($categoria->CATE_ID)) {
            if ($this->Categoria_model->delete($CATE_ID)) {
                $this->send_json(['ok' => 'ok']);
            } else {
                $this->send_json(['msg' => 'Erro: O Itens não pode ser deletado!']);
            }
        } else {
            $this->send_json(['msg' => 'Erro: A categoria que você tentou deletar já foi deletado ou não existe.']);
        }
    }

}
