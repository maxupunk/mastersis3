<?php

class Estoque extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
        $this->load->model('Estoque_model');
    }

    function index() {
        $this->page_construct("estoque/index");
    }

    function lista() {
        $this->load->model('Produto_model');

        $mostra = $this->input->get('mostrar');
        $busca = $this->input->get('buscar') ? $this->input->get('buscar') : null;

        $regpload = $this->getconf("reg_por_load");
        $output = $this->Produto_model->get_all($busca, $regpload, $mostra)->result();

        $this->send_json($output);
    }

    // função para atualizar o preço de venda
    function set() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('PRO_ID', 'id', "required");
        if ($this->form_validation->run()) {
            $post = $this->input->post();
            if (isset($post['ESTOQ_MIN']) or isset($post['ESTOQ_ATUAL'])) {
                if (isset($post['ESTOQ_MIN'])) {
                    $estoque['ESTOQ_MIN'] = $post['ESTOQ_MIN'];
                } else {
                    $estoque['ESTOQ_ATUAL'] = $post['ESTOQ_ATUAL'];
                }
                $query = $this->Estoque_model->set($post['PRO_ID'], $estoque);
                if (!$query) {
                    $this->send_json(['msg' => 'Erro ao seta o preço']);
                } else {
                    $this->send_json(['ok' => 'ok']);
                }
            }
        }
    }

    function edit($CARG_ID) {

        $cargo = $this->Cargo_model->get($CARG_ID);

        if (isset($cargo->CARG_ID)) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('CARG_NOME', 'CARG NOME', "required|edit_unique[CARGOS.CARG_NOME.CARG_ID.$CARG_ID]");

            if ($this->form_validation->run()) {
                $params = array(
                    'CARG_NOME' => $this->input->post('CARG_NOME'),
                );

                $cargo = $this->Cargo_model->update($CARG_ID, $params);
                if ($cargo) {
                    $this->send_json(['ok' => 'ok']);
                } else {
                    $this->send_json(['msg' => 'Erro: ao cadastrar o cargo! (Falha no DB)']);
                }
            } else {

                if ($this->form_validation->error_array()) {
                    $this->send_json($this->form_validation->error_array());
                    exit;
                }

                $data['cargo'] = $cargo;

                $this->load->view('cargo/edit', $data);
            }
        } else {
            $this->send_json(['msg' => 'O cargo que você tentou editar não existe!']);
        }
    }

    function remove($CARG_ID) {
        $cargo = $this->Cargo_model->get($CARG_ID);

        if (isset($cargo->CARG_ID)) {

            if ($this->Cargo_model->delete($CARG_ID)) {
                $this->send_json(['ok' => 'ok']);
            } else {
                $this->send_json(['msg' => 'Erro: O Itens não pode ser deletado!']);
            }
        } else {
            $this->send_json(['msg' => 'Erro: O cargo que você tentou deletar já foi deletado ou não existe.']);
        }
    }

}
