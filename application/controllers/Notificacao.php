<?php

class Notificacao extends MY_Controller
{

    protected $notificacao = array();

    function __construct()
    {
        parent::__construct();

        if (!isset($this->session->notificacao)) {

            // verififca a situação do estoque
            $this->stqbaixo();

            $this->session->set_userdata(['notificacao' => $this->notificacao]);
            $this->session->mark_as_temp('notificacao', 3);
        }
    }

    public function alerta()
    {
        $this->auth->check_logged($this->router->class, $this->router->method);
        if ($this->session->notificacao) {
            $this->send_json($this->session->notificacao);
        }
    }

    private function stqbaixo()
    {
        $this->load->model('Estoque_model');
        $stqbaixo = $this->Estoque_model->estatus(true)->num_rows();
        if ($stqbaixo) {
            $this->notificacao[] = [
                "url" => site_url('relatorio/estoque/baixo'),
                "msg" => "Estoque baixo",
                "qnt" => $stqbaixo
            ];
        }
    }

}
