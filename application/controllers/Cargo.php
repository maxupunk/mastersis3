<?php

class Cargo extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->auth->check_logged($this->router->class, $this->router->method);
        $this->load->model('Cargo_model');
    }

    function index() {
        $this->page_construct("cargo/index");
    }

    function lista() {

        $mostra = $this->input->get('mostrar');
        $busca = $this->input->get('buscar') ? $this->input->get('buscar') : null;

        //$regpload = $this->getconf("reg_por_load");
        $output = $this->Cargo_model->get_all($busca, null, $mostra);

        $this->send_json($output);
    }

    function add() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('CARG_NOME', 'CARGO NOME', 'required|is_unique[CARGOS.CARG_NOME]');

        if ($this->form_validation->run()) {
            $params = array(
                'CARG_NOME' => $this->input->post('CARG_NOME'),
            );

            $cargo_id = $this->Cargo_model->add($params);

            if ($cargo_id) {
                $this->send_json(['ok' => 'ok']);
            } else {
                $this->send_json(['msg' => 'Erro: ao cadastrar o produto! (Falha no DB)']);
            }
        } else {

            if ($this->form_validation->error_array()) {
                $this->send_json($this->form_validation->error_array());
                exit;
            }

            $this->load->view('cargo/form');
        }
    }

    function edit($CARG_ID) {

        $cargo = $this->Cargo_model->get($CARG_ID);

        if (isset($cargo->CARG_ID)) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('CARG_NOME', 'CARG NOME', "required|edit_unique[CARGOS.CARG_NOME.CARG_ID.$CARG_ID]");

            if ($this->form_validation->run()) {
                $params = array(
                    'CARG_NOME' => $this->input->post('CARG_NOME'),
                );

                $cargo = $this->Cargo_model->update($CARG_ID, $params);
                if ($cargo) {
                    $this->send_json(['ok' => 'ok']);
                } else {
                    $this->send_json(['msg' => 'Erro: ao cadastrar o cargo! (Falha no DB)']);
                }
            } else {

                if ($this->form_validation->error_array()) {
                    $this->send_json($this->form_validation->error_array());
                    exit;
                }

                $data['cargo'] = $cargo;

                $this->load->view('cargo/form', $data);
            }
        } else {
            $this->send_json(['msg' => 'O cargo que você tentou editar não existe!']);
        }
    }

    function remove($CARG_ID) {
        $cargo = $this->Cargo_model->get($CARG_ID);

        if (isset($cargo->CARG_ID)) {

            if ($this->Cargo_model->delete($CARG_ID)) {
                $this->send_json(['ok' => 'ok']);
            } else {
                $this->send_json(['msg' => 'Erro: O Itens não pode ser deletado!']);
            }
        } else {
            $this->send_json(['msg' => 'Erro: O cargo que você tentou deletar já foi deletado ou não existe.']);
        }
    }

}
