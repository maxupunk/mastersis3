<?php

class Ordens_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_all($bsc = null, $limit = null, $i = 0, $tp = 'v', $dti = null, $dtf = null, $user_id = null, $orde = 'desc')
    {

        $this->db->select('ORDENS.ORDEM_ID, ORDEM_DATA, PES_NOME, ORDEM_ESTATUS, USUARIO_APELIDO,'
            . 'DESREC_ID, FINANC_ESTATUS, FINANC_PORCONTA, ORDEM_EQUIPAMENT, '
            . 'SUM(LIST_PED_QNT * (LIST_PED_VALOR - LIST_PED_DESC))'
            . '+ ORDENS.ORDEM_IMPOSTO + ORDENS.ORDEM_FRETE + ORDENS.ORDEM_ACRECIMO'
            . '- ORDENS.ORDEM_DESCO as TOTAL');
        $this->db->from('ORDENS');
        $this->db->join('PESSOAS', 'ORDENS.PES_ID = PESSOAS.PES_ID');
        $this->db->join('USUARIOS', 'ORDENS.USUARIO_ID = USUARIOS.USUARIO_ID');
        $this->db->join('FINANCEIRO', 'ORDENS.ORDEM_ID = FINANCEIRO.ORDEM_ID', 'left outer');
        $this->db->join('LISTA_ITENS', 'ORDENS.ORDEM_ID = LISTA_ITENS.ORDEM_ID', 'left outer');
        $this->db->group_by('ORDENS.ORDEM_ID');
        $this->db->order_by('ORDEM_ID', $orde);
        $this->db->where('ORDEM_TIPO', $tp);

        if ($bsc) {
            $this->db->like('PESSOAS.PES_NOME', $bsc);
        }

        if ($dti) {
            $this->db->where('ORDEM_DATA>=', $dti);
        }

        if ($dtf) {
            $this->db->where('ORDEM_DATA<=', $dtf);
        }

        if ($user_id) {
            $this->db->where('USUARIO_ID', $user_id);
        }

        if ($limit) {
            $this->db->limit($limit, $i);
        }

        return $this->db->get();
    }

    function get($ordem_id = null)
    {
        $this->db->select('ORDENS.*, PES_NOME, PES_CEL1, PES_CEL2, USUARIO_APELIDO,'
            . ' FPG_DESCR, FPG_AJUSTE, '
            . 'SUM(LIST_PED_QNT * (LIST_PED_VALOR - LIST_PED_DESC))'
            . '+ ORDENS.ORDEM_IMPOSTO + ORDENS.ORDEM_FRETE + ORDENS.ORDEM_ACRECIMO'
            . '- ORDENS.ORDEM_DESCO as TOTAL,'
            . 'SUM(LIST_PED_QNT * LIST_PED_VALOR) as SUBTOTAL');
        $this->db->from('ORDENS');
        $this->db->join('PESSOAS', 'ORDENS.PES_ID = PESSOAS.PES_ID');
        $this->db->join('FORMA_PG', 'ORDENS.FPG_ID = FORMA_PG.FPG_ID', 'left outer');
        $this->db->join('USUARIOS', 'ORDENS.USUARIO_ID = USUARIOS.USUARIO_ID');
        $this->db->join('LISTA_ITENS', 'ORDENS.ORDEM_ID = LISTA_ITENS.ORDEM_ID', 'left outer');

        if ($ordem_id) {
            $this->db->where('ORDENS.ORDEM_ID', $ordem_id);
        }

        return $this->db->get()->row();
    }

    function add($params)
    {
        $this->db->insert('ORDENS', $params);
        return $this->db->insert_id();
    }

    // update com o ID do ordem
    function update($ORDEM_ID, $params)
    {
        $this->db->where('ORDEM_ID', $ORDEM_ID);
        $response = $this->db->update('ORDENS', $params);
        return $response;
    }

    function delete($ORDEM_ID)
    {
        if ($this->del_lst_produto($ORDEM_ID)) {
            $response = $this->db->delete('ORDENS', array('ORDEM_ID' => $ORDEM_ID));
            return $response;
        } else {
            return false;
        }
    }

    public function fecha($id_pedido, $pes_id, $estatus = "RE")
    {

        $this->db->query("UPDATE PRODUTOS, ESTOQUES, LISTA_ITENS, ORDENS
            SET ESTOQUES.ESTOQ_ATUAL = ESTOQUES.ESTOQ_ATUAL - LISTA_ITENS.LIST_PED_QNT,
            ORDENS.ORDEM_ESTATUS = $estatus, ORDENS.ORDEM_DATA = NOW(),
            ORDENS.PES_ID=$pes_id
            WHERE ORDENS.ORDEM_ID=$id_pedido AND LISTA_ITENS.ORDEM_ID=$id_pedido
            AND ORDENS.ORDEM_ESTATUS<=4
            AND ESTOQUES.PRO_ID = LISTA_ITENS.PRO_ID
            AND PRODUTOS.PRO_TIPO='p'");

        return $this->db->affected_rows();
    }

    // Ativa e desativa servico estatos do produto na lista de compra
    // para quando for entra no estoque somente servico marcados iram entrar
    function set_produto_estatus($ORDEM_ID, $PRO_ID, $estatus)
    {
        $this->db->where('ORDEM_ID', $ORDEM_ID);
        $this->db->where('PRO_ID', $PRO_ID);
        $this->db->where('LIST_PED_ESTATUS<', 2);
        $response = $this->db->update('LISTA_ITENS', ['LIST_PED_ESTATUS' => $estatus]);
        return $response;
    }

    // Verifica se quantos produtos falta receber
    function get_rest_lst_pro($id)
    {
        if ($id) {
            $this->db->where('LIST_PED_ESTATUS<', 2);
            $this->db->where('ORDEM_ID', $id);
            return $this->db->get('LISTA_ITENS')->num_rows();
        }
    }

    function get_lst_produto($id)
    {
        if ($id) {
            $this->db->select('LIST_PED_QNT, LIST_PED_VALOR, LIST_PED_DESC, PRO_CODBARRA, PRODUTOS.PRO_ID, '
                . ' PRO_DESCRICAO, MEDI_SIGLA, LIST_PED_ESTATUS, ESTOQ_CUSTO, ESTOQ_VENDA');
            $this->db->from('LISTA_ITENS');
            $this->db->join('PRODUTOS', 'LISTA_ITENS.PRO_ID = PRODUTOS.PRO_ID');
            $this->db->join('MEDIDAS', 'PRODUTOS.MEDI_ID = MEDIDAS.MEDI_ID');
            $this->db->join('CATEGORIAS', 'PRODUTOS.CATE_ID = CATEGORIAS.CATE_ID');
            $this->db->join('ESTOQUES', 'PRODUTOS.PRO_ID = ESTOQUES.PRO_ID');

            $this->db->where('LISTA_ITENS.ORDEM_ID', $id);

            return $this->db->get();
        }
    }

    function add_lst_produto($params)
    {
        $this->db->insert_batch('LISTA_ITENS', $params);
        return $this->db->insert_id();
    }

    function upd_lst_produto($ID, $params)
    {
        $response = $this->db->delete('LISTA_ITENS', array("ORDEM_ID" => $ID));
        if ($response) {
            $this->db->insert_batch('LISTA_ITENS', $params);
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function del_lst_produto($ORDEM_ID)
    {
        $response = $this->db->delete('LISTA_ITENS', array('ORDEM_ID' => $ORDEM_ID));
        return $response;
    }

    function total($id)
    {
        $this->db->select('SUM(LIST_PED_QNT * (LIST_PED_VALOR - LIST_PED_DESC))'
            . '+ ORDENS.ORDEM_IMPOSTO + ORDENS.ORDEM_FRETE + ORDENS.ORDEM_ACRECIMO'
            . '- ORDENS.ORDEM_DESCO as TOTAL');
        $this->db->from('LISTA_ITENS');
        $this->db->join('ORDENS', 'LISTA_ITENS.ORDEM_ID = ORDENS.ORDEM_ID');
        $this->db->where('ORDENS.ORDEM_ID', $id);
        $resultado = $this->db->get()->row();

        return (float)$resultado->TOTAL;
    }

}
