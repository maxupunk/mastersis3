<?php

class Estoque_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all() {
        $this->db->select('PES_ID, PES_NOME');
        $this->db->from('PESSOAS');
        $this->db->where('PESSOAS.PES_ESTOQUE', TRUE);
        $this->db->where('PESSOAS.PES_ESTATUS', 'a');
        return $this->db->get()->result();
    }

    function get($stq = null, $ativo = FALSE) {
        $this->db->select('PES_ID, PES_NOME');
        $this->db->from('PESSOAS');
        $this->db->where('PESSOAS.PES_ESTOQUE', TRUE);
        if ($ativo) {
            $this->db->where('PESSOAS.PES_ESTATUS', 'a');
        }
        if ($stq) {
            $this->db->where('PESSOAS.PES_ID', $stq);
        }
        return $this->db->get()->row();
    }

    function set($PRO_ID, $params) {
        $this->db->where('PRO_ID', $PRO_ID);
        $response = $this->db->update('ESTOQUES', $params);
        return $response;
    }

    function add($params) {
        $this->db->insert('ESTOQUES', $params);
        return $this->db->insert_id();
    }

    function estatus($stt = NULL) {
        $this->db->select('*');
        $this->db->from('ESTOQUES');
        $this->db->join('PRODUTOS', 'ESTOQUES.PRO_ID = PRODUTOS.PRO_ID');
        $this->db->join('MEDIDAS', 'PRODUTOS.MEDI_ID = MEDIDAS.MEDI_ID');
        $this->db->join('CATEGORIAS', 'PRODUTOS.CATE_ID = CATEGORIAS.CATE_ID');
        $this->db->where('PRODUTOS.PRO_ESTATUS', 'a');
        if ($stt) {
            $this->db->where('ESTOQUES.ESTOQ_ATUAL', 0);
        } else {
            $this->db->where('ESTOQUES.ESTOQ_ATUAL <= ESTOQUES.ESTOQ_MIN');
        }
        return $this->db->get();
    }

    function zerado() {
        $this->db->select('*');
        $this->db->from('ESTOQUES');
        $this->db->join('PRODUTOS', 'ESTOQUES.PRO_ID = PRODUTOS.PRO_ID');
        $this->db->join('MEDIDAS', 'PRODUTOS.MEDI_ID = MEDIDAS.MEDI_ID');
        $this->db->join('CATEGORIAS', 'PRODUTOS.CATE_ID = CATEGORIAS.CATE_ID');
        $this->db->where('PRODUTOS.PRO_ESTATUS', 'a');
        $this->db->where('ESTOQUES.ESTOQ_ATUAL', 0);
        return $this->db->get();
    }

}
