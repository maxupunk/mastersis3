<?php
 
class Pessoa_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function get($PES_ID)
    {
        return $this->db->get_where('PESSOAS',array('PES_ID'=>$PES_ID))->row();
    }
    
    function get_all($buscar = null, $limit = null, $i = 0, $sort = 'PESSOAS.PES_ID', $order = 'desc')
    {
        if ($buscar) {
            $this->db->or_like('PESSOAS.PES_NOME', $buscar);
            $this->db->or_like('PESSOAS.PES_CPF_CNPJ', $buscar);
            $this->db->or_like('PESSOAS.PES_NOME_PAI', $buscar);
            $this->db->or_like('PESSOAS.PES_NOME_MAE', $buscar);
            $this->db->or_like('PESSOAS.PES_CEL1', $buscar);
            $this->db->or_like('PESSOAS.PES_EMAIL', $buscar);
        }

        $this->db->order_by($sort, $order);

        if ($limit) {
            $this->db->limit($limit, $i);
        }
        
        return $this->db->get('PESSOAS');
    }
    
    function add($params)
    {
        $this->db->insert('PESSOAS',$params);
        return $this->db->insert_id();
    }
    
    function update($PES_ID,$params)
    {
        $this->db->where('PES_ID',$PES_ID);
        $response = $this->db->update('PESSOAS',$params);
        return $response;
    }
    
    function delete($PES_ID)
    {
        $response = $this->db->delete('PESSOAS',array('PES_ID'=>$PES_ID));
        return $response;
    }
}
