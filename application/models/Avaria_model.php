<?php

class Avaria_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get($CARG_ID) {
        return $this->db->get_where('AVARIAS', array('AVA_ID' => $CARG_ID))->row();
    }

    function get_all($id_estoq = null, $buscar = null, $limit = null, $i = 0) {

        $this->db->from('AVARIAS');
        $this->db->join('PRODUTOS', 'AVARIAS.PRO_ID = PRODUTOS.PRO_ID');
        $this->db->join('MEDIDAS', 'PRODUTOS.MEDI_ID = MEDIDAS.MEDI_ID');
        $this->db->join('CATEGORIAS', 'PRODUTOS.CATE_ID = CATEGORIAS.CATE_ID');
        $this->db->join('PESSOAS', 'AVARIAS.PES_ID_EST = PESSOAS.PES_ID');

        if ($id_estoq) {
            $this->db->where('AVARIAS.PES_ID_EST', $id_estoq);
        }

        if ($buscar) {
            $this->db->where('PRODUTOS.PRO_CODBARRA', $buscar);
            $this->db->or_like('PRODUTOS.PRO_DESCRICAO', $buscar);
            $this->db->or_like('AVARIAS.AVA_MOTIVO', $buscar);
        }

        if ($limit) {
            $this->db->limit($limit, $i);
        }

        return $this->db->get()->result();
    }

    function add($params) {
        $this->db->insert('AVARIAS', $params);
        return $this->db->insert_id();
    }

    function fechar($AVA_ID, $PRO_ID) {
        $this->db->query("UPDATE ESTOQUES, AVARIAS
            SET ESTOQUES.ESTOQ_ATUAL = ESTOQUES.ESTOQ_ATUAL - AVARIAS.AVA_QNT, AVARIAS.AVA_ESTATUS=1
            WHERE AVARIAS.AVA_ID=$AVA_ID AND ESTOQUES.PRO_ID=$PRO_ID AND AVARIAS.AVA_ESTATUS=0");

        return $this->db->affected_rows();
    }

    function restourar($AVA_ID, $PRO_ID) {
        $this->db->query("UPDATE ESTOQUES, AVARIAS
            SET ESTOQUES.ESTOQ_ATUAL = ESTOQUES.ESTOQ_ATUAL + AVARIAS.AVA_QNT, AVARIAS.AVA_ESTATUS=0
            WHERE AVARIAS.AVA_ID=$AVA_ID AND ESTOQUES.PRO_ID=$PRO_ID AND AVARIAS.AVA_ESTATUS=1");

        return $this->db->affected_rows();
    }

    function delete($AVA_ID) {
        $response = $this->db->delete('AVARIAS', array('AVA_ID' => $AVA_ID,'AVA_ESTATUS' => 0));
        return $response;
    }

}
