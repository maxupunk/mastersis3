<?php

class Financeiro_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get($DESREC_ID) {
        $this->db->select('FINANCEIRO.* , PES_NOME, USUARIO_APELIDO ');
        $this->db->from('FINANCEIRO');
        $this->db->join('PESSOAS', 'FINANCEIRO.PES_ID = PESSOAS.PES_ID', 'left');
        $this->db->join('USUARIOS', 'USUARIOS.USUARIO_ID = FINANCEIRO.USUARIO_ID', 'left');

        $this->db->where('DESREC_ID', $DESREC_ID);

        return $this->db->get()->row();
    }

    function get_all($buscar = null, $limit = null, $i = 0, $dti = null, $dtf = null, $natu = null, $estatus = null, $user_id = null) {

        $this->db->select('*');
        $this->db->from('FINANCEIRO');
        $this->db->join('PESSOAS', 'FINANCEIRO.PES_ID = PESSOAS.PES_ID', 'left');
        $this->db->join('USUARIOS', 'USUARIOS.USUARIO_ID = FINANCEIRO.USUARIO_ID', 'left');
        $this->db->join('ORDENS', 'ORDENS.ORDEM_ID = FINANCEIRO.ORDEM_ID', 'left');
        $this->db->order_by('FINANC_DATA', 'desc');

        if ($dti) {
            $this->db->where('FINANC_DATA>=', $dti);
        }

        if ($dtf) {
            $this->db->where('FINANC_DATA<=', $dtf);
        }

        if ($user_id) {
            $this->db->where('USUARIO_ID', $user_id);
        }

        if ($natu) {
            $this->db->where('FINANC_NATUREZA', $natu);
        }

        if ($estatus) {
            $this->db->where('FINANC_ESTATUS', $estatus);
        }

        if ($buscar) {
            $this->db->like('PESSOAS.PES_NOME', $buscar);
            $this->db->or_like('PESSOAS.PES_CPF_CNPJ', $buscar);
        }

        if ($limit) {
            $this->db->limit($limit, $i);
        }

        return $this->db->get();
    }

    function add($params) {
        $this->db->insert('FINANCEIRO', $params);
        return $this->db->insert_id();
    }

    function update($DESREC_ID, $params) {
        $this->db->where('DESREC_ID', $DESREC_ID);
        $response = $this->db->update('FINANCEIRO', $params);
        return $response;
    }

    function del($DESREC_ID) {
        $this->db->where('DESREC_ID', $DESREC_ID);
        $response = $this->db->delete('FINANCEIRO');
        return $response;
    }

    ///////////////////////////////////////
    ////////  Funçoes de ordens  //////////
    ///////////////////////////////////////
    function getByOrdemID($ORDEM_ID) {
        return $this->db->get_where('FINANCEIRO', array('ORDEM_ID' => $ORDEM_ID));
    }

    function updadeByOrdemID($ORDEM_ID, $params) {
        $this->db->where('ORDEM_ID', $ORDEM_ID);
        $response = $this->db->update('FINANCEIRO', $params);
        return $response;
    }

    function delByOrdemID($ORDEM_ID) {
        $this->db->where('ORDEM_ID', $ORDEM_ID);
        $response = $this->db->delete('FINANCEIRO');
        return $response;
    }


}
