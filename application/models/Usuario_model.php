<?php

class Usuario_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get($USUARIO_ID) {
        $this->db->select('*');
        $this->db->from('USUARIOS', 'CARGOS', 'PESSOAS');
        $this->db->join('CARGOS', 'USUARIOS.CARG_ID = CARGOS.CARG_ID', 'left');
        $this->db->join('PESSOAS', 'PESSOAS.PES_ID = USUARIOS.PES_ID', 'left');
        $this->db->where('USUARIO_ID', $USUARIO_ID);
        return $this->db->get()->row();
    }

    function get_all($buscar = null, $limit = null, $i = 0) {
        $this->db->select('*');
        $this->db->from('USUARIOS');
        $this->db->join('CARGOS', 'USUARIOS.CARG_ID = CARGOS.CARG_ID', 'left');
        $this->db->join('PESSOAS', 'PESSOAS.PES_ID = USUARIOS.PES_ID', 'left');

        if ($buscar) {
            $this->db->or_like('USUARIOS.USUARIO_APELIDO', $buscar);
            $this->db->or_like('USUARIOS.USUARIO_LOGIN', $buscar);
            $this->db->or_like('PESSOAS.PES_NOME', $buscar);
            $this->db->or_like('PESSOAS.PES_CPF_CNPJ', $buscar);
        }

        if ($limit) {
            $this->db->limit($limit, $i);
        }

        return $this->db->get()->result();
    }

    function add($params) {
        $this->db->insert('USUARIOS', $params);
        return $this->db->insert_id();
    }

    function update($USUARIO_ID, $params) {
        $this->db->where('USUARIO_ID', $USUARIO_ID);
        $response = $this->db->update('USUARIOS', $params);
        return $response;
    }

    function delete($USUARIO_ID) {
        $response = $this->db->delete('USUARIOS', array('USUARIO_ID' => $USUARIO_ID));
        return $response;
    }

    function getlogin($LOGIN) {
        $login = [
            'USUARIO_LOGIN' => $LOGIN,
            'USUARIO_ESTATUS' => 'a'
        ];

        return $this->db->get_where('USUARIOS', $login)->row();
    }

    function permissoes($USUARIO_ID) {
        $this->db->select('METOD_ID');
        $this->db->from('PERMISSOES');
        $this->db->where('USUARIO_ID', $USUARIO_ID);
        return $this->db->get()->result();
    }

    function metodos() {
        $this->db->select('*');
        $this->db->from('METODOS');
        $this->db->where('METOD_PRIVADO', 1);
        $this->db->order_by('METOD_CLASS', 'ASC');
        return $this->db->get()->result();
    }

    function setpermissao($params) {
        $this->db->insert('PERMISSOES', $params);
        return $this->db->insert_id();
    }

    function getpermissao($params) {
        return $this->db->get_where('PERMISSOES', $params)->row();
    }

    function delpermissao($params) {
        return $this->db->delete('PERMISSOES', $params);
    }

}
