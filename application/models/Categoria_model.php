<?php

class Categoria_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get($CATE_ID) {
        return $this->db->get_where('CATEGORIAS', array('CATE_ID' => $CATE_ID))->row();
    }

    function get_all($buscar = null, $limit = null, $i = 0, $ativo = null) {
        if ($buscar) {
            $this->db->or_like('CATE_NOME', $buscar);
            $this->db->or_like('CATE_DESCRIC', $buscar);
        }

        if ($ativo) {
            $this->db->where('CATE_ESTATUS', 'a');
        }

        if ($limit) {
            $this->db->limit($limit, $i);
        }

        return $this->db->get('CATEGORIAS')->result();
    }

    function add($params) {
        $this->db->insert('CATEGORIAS', $params);
        return $this->db->insert_id();
    }

    function update($CATE_ID, $params) {
        $this->db->where('CATE_ID', $CATE_ID);
        $response = $this->db->update('CATEGORIAS', $params);
        return $response;
    }

    function delete($CATE_ID) {
        $response = $this->db->delete('CATEGORIAS', array('CATE_ID' => $CATE_ID));
        return $response;
    }

}
