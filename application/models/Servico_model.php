<?php

class Servico_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function del_lst_item($OS_ID) {
        $response = $this->db->delete('LISTA_ITENS', array('OS_ID' => $OS_ID));
        return $response;
    }

    public function abrir($ORDEM_ID) {

        $this->db->query("UPDATE PRODUTOS, ESTOQUES, LISTA_ITENS, ORDENS
            SET ESTOQUES.ESTOQ_ATUAL = ESTOQUES.ESTOQ_ATUAL + LISTA_ITENS.LIST_PED_QNT,
            ORDENS.ORDEM_ESTATUS='EA'
            WHERE ORDENS.ORDEM_ID=$ORDEM_ID AND LISTA_ITENS.ORDEM_ID=$ORDEM_ID
            AND ESTOQUES.PRO_ID = LISTA_ITENS.PRO_ID
            AND PRODUTOS.PRO_TIPO='p'");

        return $this->db->affected_rows();
    }

    ////////////////////////////////////////////////
    ///// gerenciamento dos itens dentro do OS /////
    ////////////////////////////////////////////////
    function add_lst($params) {
        $this->db->insert('LISTA_ITENS', $params);
        return $this->db->trans_status();
    }

    function get_item($ORDEM_ID, $PRO_ID) {
        $this->db->select('LIST_PED_QNT, LIST_PED_VALOR');
        $this->db->from('LISTA_ITENS');
        $this->db->where('ORDEM_ID', $ORDEM_ID);
        $this->db->where('PRO_ID', $PRO_ID);
        return $this->db->get()->row();
    }

    function get_lst($id) {
        if ($id) {
            $this->db->select('LIST_PED_QNT, LIST_PED_VALOR, LIST_PED_DESC, PRO_CODBARRA, PRODUTOS.PRO_ID, PRO_DESCRICAO, MEDI_SIGLA');
            $this->db->from('LISTA_ITENS');
            $this->db->join('PRODUTOS', 'LISTA_ITENS.PRO_ID = PRODUTOS.PRO_ID');
            $this->db->join('MEDIDAS', 'PRODUTOS.MEDI_ID = MEDIDAS.MEDI_ID');
            $this->db->join('CATEGORIAS', 'PRODUTOS.CATE_ID = CATEGORIAS.CATE_ID');

            $this->db->where('LISTA_ITENS.ORDEM_ID', $id);

            return $this->db->get()->result();
        }
    }

    function del_lst($ORDEM_ID, $PRO_ID) {
        $response = $this->db->delete('LISTA_ITENS', array('ORDEM_ID' => $ORDEM_ID, 'PRO_ID' => $PRO_ID));
        return $response;
    }

    function upd_lst($ORDEM_ID, $PRO_ID, $QNT) {
        $this->db->where('ORDEM_ID', $ORDEM_ID);
        $this->db->where('PRO_ID', $PRO_ID);
        $response = $this->db->update('LISTA_ITENS', ['LIST_PED_QNT' => $QNT]);
        return $response;
    }

}
