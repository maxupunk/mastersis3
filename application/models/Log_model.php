<?php

class Log_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // em construção
    function get_all($buscar = null, $limit = null, $i = 0, $dti = null, $dtf = null, $orde = 'desc') {
        $this->db->select('*');
        $this->db->from('LOG_ACESSOS');
        $this->db->join('USUARIOS', 'LOG_ACESSOS.USUARIO_ID = USUARIOS.USUARIO_ID', 'left');
        $this->db->join('METODOS', 'LOG_ACESSOS.METOD_ID = METODOS.METOD_ID', 'left');
        

        if ($buscar) {
            $this->db->or_like('USUARIOS.USUARIO_APELIDO', $buscar);
            $this->db->or_like('USUARIOS.USUARIO_LOGIN', $buscar);
            $this->db->or_like('LOG_ACESSOS.LOG_ACESS_IP', $buscar);
            $this->db->or_like('LOG_ACESSOS.LOG_ACESS_MSN', $buscar);
        }

        if ($dti) {
            $this->db->where('LOG_ACESSOS.LOG_ACESS_DATA>=', $dti);
        }

        if ($dtf) {
            $this->db->where('.LOG_ACESSOS.LOG_ACESS_DATA<=', $dtf);
        }

        if ($limit) {
            $this->db->limit($limit, $i);
        }
        
        $this->db->order_by('LOG_ACESS_DATA', $orde);

        return $this->db->get()->result();
    }

    function add($params) {
        $this->db->insert('LOG_ACESSOS', $params);
        return $this->db->insert_id();
    }

}
