<?php

class Venda_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function fecha($id_pedido, $pes_id, $estatus = 'RE') {

        $this->db->query("UPDATE PRODUTOS, ESTOQUES, LISTA_ITENS, ORDENS
            SET ESTOQUES.ESTOQ_ATUAL = ESTOQUES.ESTOQ_ATUAL - LISTA_ITENS.LIST_PED_QNT,
            ORDENS.ORDEM_ESTATUS = '$estatus', ORDENS.ORDEM_DATA = NOW(),
            ORDENS.PES_ID=$pes_id
            WHERE
            ORDENS.ORDEM_ID=$id_pedido
            AND LISTA_ITENS.ORDEM_ID=$id_pedido
            AND ESTOQUES.PRO_ID = LISTA_ITENS.PRO_ID
            AND PRODUTOS.PRO_TIPO='p'
            AND ORDENS.ORDEM_ESTATUS!='RE'");

        return $this->db->affected_rows();
    }

    public function abrir($id_pedido) {

        $this->db->query("UPDATE PRODUTOS, ESTOQUES, LISTA_ITENS, ORDENS
            SET ESTOQUES.ESTOQ_ATUAL = ESTOQUES.ESTOQ_ATUAL + LISTA_ITENS.LIST_PED_QNT,
            ORDENS.ORDEM_ESTATUS=1
            WHERE ORDENS.ORDEM_ID=$id_pedido AND LISTA_ITENS.ORDEM_ID=$id_pedido
            AND ESTOQUES.PRO_ID = LISTA_ITENS.PRO_ID
            AND PRODUTOS.PRO_TIPO='p'");

        return $this->db->affected_rows();
    }

}
