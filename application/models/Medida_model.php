<?php

class Medida_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get($MEDI_ID) {
        return $this->db->get_where('MEDIDAS', array('MEDI_ID' => $MEDI_ID))->row();
    }

    function get_all($buscar = null, $limit = null, $i = 0) {
        if ($buscar) {
            $this->db->or_like('MEDI_NOME', $buscar);
            $this->db->or_like('MEDI_SIGLA', $buscar);
        }

        if ($limit) {
            $this->db->limit($limit, $i);
        }

        return $this->db->get('MEDIDAS')->result();
    }

    function add($params) {
        $this->db->insert('MEDIDAS', $params);
        return $this->db->insert_id();
    }

    function update($MEDI_ID, $params) {
        $this->db->where('MEDI_ID', $MEDI_ID);
        $response = $this->db->update('MEDIDAS', $params);
        return $response;
    }

    function delete($MEDI_ID) {
        $response = $this->db->delete('MEDIDAS', array('MEDI_ID' => $MEDI_ID));
        return $response;
    }

}
