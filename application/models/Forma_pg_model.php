<?php
 
class Forma_pg_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function get($FPG_ID)
    {
        return $this->db->get_where('FORMA_PG',array('FPG_ID'=>$FPG_ID))->row();
    }
    
    function get_all($buscar = null, $limit = null, $i = 0, $ativo = FALSE)
    {
        if ($buscar) {
            $this->db->like('FPG_DESCR', $buscar);
        }
        
        if ($ativo){
            $this->db->like('FPG_ESTATUS', 'a');
        }

        if ($limit) {
            $this->db->limit($limit, $i);
        }
        
        return $this->db->get('FORMA_PG')->result();
    }
    
    function add($params)
    {
        $this->db->insert('FORMA_PG',$params);
        return $this->db->insert_id();
    }
    
    function update($FPG_ID,$params)
    {
        $this->db->where('FPG_ID',$FPG_ID);
        $response = $this->db->update('FORMA_PG',$params);
        return $response;
    }
    
    function delete($FPG_ID)
    {
        $response = $this->db->delete('FORMA_PG',array('FPG_ID'=>$FPG_ID));
        return $response;
    }
}
