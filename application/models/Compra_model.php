<?php

class Compra_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function fecha($id_ordem) {

        $this->db->query("UPDATE PRODUTOS, ESTOQUES, LISTA_ITENS, ORDENS
            SET ESTOQUES.ESTOQ_ATUAL = ESTOQUES.ESTOQ_ATUAL + LISTA_ITENS.LIST_PED_QNT,
            ORDENS.ORDEM_DATA = NOW(),
            ESTOQUES.ESTOQ_CUSTO = LISTA_ITENS.LIST_PED_VALOR,
            LISTA_ITENS.LIST_PED_ESTATUS=2
            WHERE ORDENS.ORDEM_ID=$id_ordem AND LISTA_ITENS.ORDEM_ID=$id_ordem
            AND ORDENS.ORDEM_ESTATUS != 'RE'
            AND ESTOQUES.PRO_ID = LISTA_ITENS.PRO_ID
            AND LISTA_ITENS.LIST_PED_ESTATUS=1");

        return $this->db->affected_rows();
    }

    public function abrir($id_ordem) {

        $this->db->query("UPDATE PRODUTOS, ESTOQUES, LISTA_ITENS, ORDENS
            SET ESTOQUES.ESTOQ_ATUAL = ESTOQUES.ESTOQ_ATUAL - LISTA_ITENS.LIST_PED_QNT,
            ORDENS.ORDEM_ESTATUS=1
            WHERE ORDENS.ORDEM_ID=$id_ordem AND LISTA_ITENS.ORDEM_ID=$id_ordem
            AND ESTOQUES.PRO_ID = LISTA_ITENS.PRO_ID
            AND LISTA_ITENS.LIST_PED_ESTATUS=<2");

        return $this->db->affected_rows();
    }

}
