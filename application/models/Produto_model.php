<?php

class Produto_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_produto($buscar = null, $limit = null, $sem_estoq = null) {

        $this->db->select('PRODUTOS.PRO_ID, PRO_DESCRICAO, ESTOQ_VENDA, ESTOQ_ATUAL');
        $this->db->from('PRODUTOS');
        $this->db->join('ESTOQUES', 'PRODUTOS.PRO_ID = ESTOQUES.PRO_ID');

        if ($buscar) {
            $this->db->like('PRODUTOS.PRO_CODBARRA', $buscar);
            $this->db->or_like('PRODUTOS.PRO_DESCRICAO', $buscar);
        }

        if ($sem_estoq) {
            $this->db->where('ESTOQUES.ESTOQ_ATUAL >', 0);
        }

        $this->db->where('PRODUTOS.PRO_TIPO', 'p');

        if ($limit) {
            $this->db->limit($limit);
        }

        return $this->db->get();
    }

    function get_servico($buscar = null, $limit = null) {

        $this->db->select('PRODUTOS.PRO_ID, PRO_DESCRICAO, ESTOQ_VENDA');
        $this->db->from('PRODUTOS');
        $this->db->join('ESTOQUES', 'PRODUTOS.PRO_ID = ESTOQUES.PRO_ID');

        if ($buscar) {
            $this->db->like('PRODUTOS.PRO_CODBARRA', $buscar);
            $this->db->or_like('PRODUTOS.PRO_DESCRICAO', $buscar);
        }

        $this->db->where('PRODUTOS.PRO_TIPO', 's');

        if ($limit) {
            $this->db->limit($limit);
        }

        return $this->db->get();
    }

    function get($PRO_ID = NULL) {

        $this->db->select('*');
        $this->db->from('PRODUTOS');
        $this->db->join('MEDIDAS', 'PRODUTOS.MEDI_ID = MEDIDAS.MEDI_ID');
        $this->db->join('CATEGORIAS', 'PRODUTOS.CATE_ID = CATEGORIAS.CATE_ID');
        $this->db->join('ESTOQUES', 'PRODUTOS.PRO_ID = ESTOQUES.PRO_ID');

        if ($PRO_ID) {
            $this->db->where('PRODUTOS.PRO_ID', $PRO_ID);
        }

        return $this->db->get()->row();
    }

    function get_all($buscar = null, $limit = null, $i = 0, $tipo = null) {

        $this->db->from('PRODUTOS');
        $this->db->join('MEDIDAS', 'PRODUTOS.MEDI_ID = MEDIDAS.MEDI_ID');
        $this->db->join('CATEGORIAS', 'PRODUTOS.CATE_ID = CATEGORIAS.CATE_ID');
        $this->db->join('ESTOQUES', 'PRODUTOS.PRO_ID = ESTOQUES.PRO_ID');

        if ($buscar) {
            $this->db->like('PRODUTOS.PRO_CODBARRA', $buscar);
            $this->db->or_like('PRODUTOS.PRO_DESCRICAO', $buscar);
        }

        if ($limit) {
            $this->db->limit($limit, $i);
        }
        
        if ($tipo) {
            $this->db->where('PRODUTOS.PRO_TIPO', $tipo);
        }

        return $this->db->get();
    }

    function add($params) {
        $this->db->insert('PRODUTOS', $params);
        return $this->db->insert_id();
    }

    function update($PRO_ID, $params = NULL, $estoque = NULL) {

        if ($params or $estoque) {
            if ($estoque) {
                $this->db->set($estoque);
                $this->db->where('ESTOQUES.PRO_ID', $PRO_ID);
                $this->db->update('ESTOQUES');
            }

            if ($params) {
                $this->db->set($params);
                $this->db->where('PRODUTOS.PRO_ID', $PRO_ID);
                $response = $this->db->update('PRODUTOS');
                return $response;
            }
        } else {
            return false;
        }
    }

    function delete($PRO_ID) {
        $response = $this->db->delete('PRODUTOS', array('PRO_ID' => $PRO_ID));
        return $response;
    }

    function total() {
        return $this->db->get('PRODUTOS')->num_rows();
    }

}
