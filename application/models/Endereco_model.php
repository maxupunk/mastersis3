<?php

class Endereco_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_estado($ESTADO) {
        $this->db->where('ESTA_ID', $ESTADO);
        $this->db->or_where('ESTA_NOME', $ESTADO);
        return $this->db->get()->row();
    }

    function get_cidade($CIDA_ID) {
        $this->db->where('CIDA_ID', $CIDA_ID);
        return $this->db->get()->row();
    }

    function get_bairro($BAIRRO_ID) {
        $this->db->where('BAIRRO_ID', $BAIRRO_ID);
        return $this->db->get()->row();
    }

    function get_rua($RUA_ID) {
        $this->db->select('*');
        $this->db->from('RUAS', 'BAIRROS', 'CIDADES', 'ESTADOS');
        $this->db->join('BAIRROS', 'RUAS.BAIRRO_ID = BAIRROS.BAIRRO_ID');
        $this->db->join('CIDADES', 'BAIRROS.CIDA_ID = CIDADES.CIDA_ID');
        $this->db->join('ESTADOS', 'CIDADES.ESTA_ID = ESTADOS.ESTA_ID');

        $this->db->where('RUA_ID', $RUA_ID);

        return $this->db->get()->row();
    }

    function get_all($buscar = null, $limit = null, $i = 0) {
        $this->db->select('*');
        $this->db->from('RUAS');
        $this->db->join('BAIRROS', 'RUAS.BAIRRO_ID = BAIRROS.BAIRRO_ID');
        $this->db->join('CIDADES', 'BAIRROS.CIDA_ID = CIDADES.CIDA_ID');
        $this->db->join('ESTADOS', 'CIDADES.ESTA_ID = ESTADOS.ESTA_ID');

        if ($buscar) {
            $this->db->or_like('RUAS.RUA_NOME', $buscar);
            $this->db->or_like('BAIRROS.BAIRRO_NOME', $buscar);
            $this->db->or_like('CIDADES.CIDA_NOME', $buscar);
            $this->db->or_like('ESTADOS.ESTA_NOME', $buscar);
            $this->db->or_like('ESTADOS.ESTA_UF', $buscar);
        }

        if ($limit) {
            $this->db->limit($limit, $i);
        }

        return $this->db->get()->result();
    }

    function busca_estados($buscar = null, $limit = 5) {
        $this->db->select('*');

        if ($buscar) {
            $this->db->like('ESTA_NOME', $buscar);
            $this->db->or_like('ESTA_UF', $buscar);
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        return $this->db->get('ESTADOS')->result();
    }

    function busca_cidades($buscar = null, $id = null, $limit = 5) {

        $this->db->select('*');
        $this->db->where('ESTA_ID', $id);

        if ($buscar) {
            $this->db->like('CIDA_NOME', $buscar);
        }

        $this->db->limit($limit);

        return $this->db->get('CIDADES')->result();
    }

    function busca_bairros($buscar = null, $id = null, $limit = 5) {

        $this->db->select('*');
        $this->db->where('CIDA_ID', $id);

        if ($buscar) {
            $this->db->like('BAIRRO_NOME', $buscar);
        }

        $this->db->limit($limit);

        return $this->db->get('BAIRROS')->result();
    }

    function busca_ruas($buscar = null, $id = null, $limit = 5) {

        $this->db->select('*');
        $this->db->where('BAIRRO_ID', $id);

        if ($buscar) {
            $this->db->like('RUA_NOME', $buscar);
        }

        $this->db->limit($limit);

        return $this->db->get('RUAS')->result();
    }

    function add_estado($params) {

        $this->db->where('ESTA_NOME', $params['ESTA_NOME']);
        $estado = $this->db->get('ESTADOS')->row();

        if ($estado) {
            return $estado->ESTA_ID;
        } else {
            $this->db->insert('ESTADOS', $params);
            return $this->db->insert_id();
        }
    }

    function add_cidade($params) {

        $this->db->where('CIDA_NOME', $params['CIDA_NOME']);
        $this->db->where('ESTA_ID', $params['ESTA_ID']);

        $cidade = $this->db->get('CIDADES')->row();

        if ($cidade) {
            return $cidade->CIDA_ID;
        } else {
            $this->db->insert('CIDADES', $params);
            return $this->db->insert_id();
        }
    }

    function add_bairro($params) {

        $this->db->where('BAIRRO_NOME', $params['BAIRRO_NOME']);
        $this->db->where('CIDA_ID', $params['CIDA_ID']);
        $bairro = $this->db->get('BAIRROS')->row();

        if ($bairro) {
            return $bairro->BAIRRO_ID;
        } else {
            $this->db->insert('BAIRROS', $params);
            return $this->db->insert_id();
        }
    }

    function add_rua($params) {

        $this->db->where('RUA_NOME', $params['RUA_NOME']);
        $this->db->where('BAIRRO_ID', $params['BAIRRO_ID']);
        $rua = $this->db->get('RUAS')->row();

        if ($rua) {
            return $rua->RUA_ID;
        } else {
            $this->db->insert('RUAS', $params);
            return $this->db->insert_id();
        }
    }

    function update_estado($RUA_ID, $params) {
        $this->db->where('ESTA_ID', $RUA_ID);
        $response = $this->db->update('ESTADOS', $params);
        return $response;
    }

    function update_cidade($RUA_ID, $params) {
        $this->db->where('CIDA_ID', $RUA_ID);
        $response = $this->db->update('CIDADES', $params);
        return $response;
    }

    function update_bairro($RUA_ID, $params) {
        $this->db->where('BAIRRO_ID', $RUA_ID);
        $response = $this->db->update('BAIRROS', $params);
        return $response;
    }

    function update_rua($RUA_ID, $params) {
        $this->db->where('RUA_ID', $RUA_ID);
        $response = $this->db->update('RUAS', $params);
        return $response;
    }

    function delete_rua($RUA_ID) {
        $response = $this->db->delete('RUAS', array('RUA_ID' => $RUA_ID));
        return $response;
    }

}
