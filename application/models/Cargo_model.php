<?php
 
class Cargo_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function get($CARG_ID)
    {
        return $this->db->get_where('CARGOS',array('CARG_ID'=>$CARG_ID))->row();
    }
    
    function get_all($buscar = null, $limit = null, $i = 0)
    {
        if ($buscar) {
            $this->db->or_like('CATE_NOME', $buscar);
            $this->db->or_like('CATE_DESCRIC', $buscar);
        }

        if ($limit) {
            $this->db->limit($limit, $i);
        }
        
        return $this->db->get('CARGOS')->result();
    }
    
    function add($params)
    {
        $this->db->insert('CARGOS',$params);
        return $this->db->insert_id();
    }
    
    function update($CARG_ID,$params)
    {
        $this->db->where('CARG_ID',$CARG_ID);
        $response = $this->db->update('CARGOS',$params);
        return $response;
    }
    
    function delete($CARG_ID)
    {
        $response = $this->db->delete('CARGOS',array('CARG_ID'=>$CARG_ID));
        return $response;
    }
}
