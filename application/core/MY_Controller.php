<?php

class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function page_construct($page, $data = array(), $meta = array()) {

        $meta['usuario'] = $this->session->USUARIO_APELIDO;
        $meta['ip_address'] = $this->input->ip_address();

        $this->load->view('header', $meta);
        $this->load->view($page, $data);
        $this->load->view('footer');
    }

    function send_json($data) {
        header('Content-Type: application/json');
        die(json_encode($data));
    }

    function getconf($opc) {
        $this->db->select('VALOR');
        $this->db->from('CONFIG');
        $this->db->where('OPCAO', $opc);
        $retorno = $this->db->get()->row();
        return isset($retorno) ? $retorno->VALOR : FALSE;
    }

}
